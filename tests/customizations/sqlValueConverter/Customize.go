package sqlValueConverter

import "bitbucket.org/graph-ql-schema/sbuilder/v2"

// Кастомизатор из процессора
type customize struct {}

// Конструктор кастомизатора
func NewCustomize() sbuilder.CustomizationInterface {
	return &customize{}
}

// Основной метод кастомизации процессора
func (c customize) Customize() interface{} {
	return newSqlValueConverter()
}

// Получение типа кастомизации
func (c customize) GetType() int64 {
	return sbuilder.CustomizationTypeValueConverter
}