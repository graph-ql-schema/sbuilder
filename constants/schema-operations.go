package constants

const (
	WhereSchemaKey   string = "where"
	HavingSchemaKey  string = "having"
	OrderBySchemaKey string = "order"
	GroupBySchemaKey string = "groupBy"
	ObjectsSchemaKey string = "objects"
	SetSchemaKey     string = "set"
	LimitSchemaKey   string = "limit"
	OffsetSchemaKey  string = "offset"

	SumOperationSchemaKey      string = "sum"
	AvgOperationSchemaKey      string = "avg"
	MaxOperationSchemaKey      string = "max"
	MinOperationSchemaKey      string = "min"
	CountOperationSchemaKey    string = "count"
	VariantsOperationSchemaKey string = "variants"

	AndSchemaKey string = "_and"
	OrSchemaKey  string = "_or"
	NotSchemaKey string = "_not"

	BetweenSchemaKey      string = "_between"
	EqualsSchemaKey       string = "_equals"
	InSchemaKey           string = "_in"
	MoreSchemaKey         string = "_more"
	LessSchemaKey         string = "_less"
	MoreOrEqualsSchemaKey string = "_more_or_equals"
	LessOrEqualsSchemaKey string = "_less_or_equals"
	LikeSchemaKey         string = "_like"
)
