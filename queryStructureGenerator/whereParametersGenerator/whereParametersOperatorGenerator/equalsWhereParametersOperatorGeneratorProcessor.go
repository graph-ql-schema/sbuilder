package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/nullable"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
)

// Процессор генерации оператора "_equals"
type equalsWhereParametersOperatorGeneratorProcessor struct {
	maxLevel        uint8
	logger          *logrus.Entry
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Проверка доступности процессора
func (e equalsWhereParametersOperatorGeneratorProcessor) IsAvailable(ctx context.Context, field *graphql.FieldDefinition, level uint8) bool {
	return level <= e.maxLevel && field.Resolve == nil
}

// Код оператора, для вставки в схему GraphQL
func (e equalsWhereParametersOperatorGeneratorProcessor) OperatorCode() string {
	return constants.EqualsSchemaKey
}

// Генерация параметров
func (e equalsWhereParametersOperatorGeneratorProcessor) Generate(
	ctx context.Context,
	field *graphql.FieldDefinition,
	level uint8,
	namePrefix string,
) *graphql.InputObjectFieldConfig {
	rType := e.fieldTypeGetter.GetRootType(field.Type)
	if scalar, ok := rType.(*graphql.Scalar); ok && e.fieldTypeGetter.IsNullable(field.Type) {
		rType = nullable.NewNullable(scalar)
	}

	e.logger.WithFields(logrus.Fields{
		"code":       100,
		"field":      field.Name,
		"level":      level,
		"namePrefix": namePrefix,
	}).Debug(`Generated _EQUALS operator for field`)

	return &graphql.InputObjectFieldConfig{
		Type:        rType,
		Description: "Операция строго сравнения значения поля с переданным",
	}
}

// Фабрика процессора
func newEqualsWhereParametersOperatorGeneratorProcessor(
	maxLevel uint8,
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface,
) whereParametersOperatorGeneratorProcessorInterface {
	return &equalsWhereParametersOperatorGeneratorProcessor{
		maxLevel:        maxLevel,
		logger:          helpers.NewLogger(`equalsWhereParametersOperatorGeneratorProcessor`),
		fieldTypeGetter: fieldTypeGetter,
	}
}
