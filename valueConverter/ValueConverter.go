package valueConverter

import (
	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"github.com/graphql-go/graphql"
)

// Расширяемый конвертер
type valueConverter struct {
	converters []gql_sql_converter.GraphQlSqlConverterInterface
}

// Конструктор
func NewValueConverter() gql_sql_converter.GraphQlSqlConverterInterface {
	return &valueConverter{
		converters: []gql_sql_converter.GraphQlSqlConverterInterface{gql_sql_converter.NewGraphQlSqlConverter()},
	}
}

// Конструктор с кастомизаторами
func NewValueConverterWithCustomizations(customizations []customizationService.CustomizationInterface) gql_sql_converter.GraphQlSqlConverterInterface {
	converters := []gql_sql_converter.GraphQlSqlConverterInterface{gql_sql_converter.NewGraphQlSqlConverter()}

	for _, customConverter := range customizations {
		if customConverter.GetType() == constants.CustomizationTypeValueConverter {
			converters = append(converters, customConverter.Customize().(gql_sql_converter.GraphQlSqlConverterInterface))
		}
	}

	return &valueConverter{
		converters: converters,
	}
}

// Конвертация в базовый тип, например в строку или число
// Если конвертация дефолтным конвертером не удалась, тогда пытаемся конвертировать кастомными (если они есть)
func (c valueConverter) ToBaseType(object *graphql.Object, field string, value interface{}) (result interface{}, err error) {
	for _, customConverter := range c.converters {
		result, err = customConverter.ToBaseType(object, field, value)
		if nil == err {
			return result, err
		}
	}

	return result, err
}

// Конвертация в SQL like значение
func (c valueConverter) ToSQLValue(object *graphql.Object, field string, value interface{}) (result string, err error) {
	for _, customConverter := range c.converters {
		result, err = customConverter.ToSQLValue(object, field, value)
		if nil == err {
			return result, err
		}
	}

	return result, err
}