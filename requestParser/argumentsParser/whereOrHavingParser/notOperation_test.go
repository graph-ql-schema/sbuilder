package whereOrHavingParser

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
)

// Тестирование получения типа
func Test_notOperation_Type(t *testing.T) {
	type fields struct {
		value Operation
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "Тестирование получения типа",
			fields: fields{},
			want:   constants.NotSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notOperation{
				value: tt.fields.value,
			}
			if got := n.Type(); got != tt.want {
				t.Errorf("Type() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения значения
func Test_notOperation_Value(t *testing.T) {
	type fields struct {
		value Operation
	}
	tests := []struct {
		name   string
		fields fields
		want   interface{}
	}{
		{
			name:   "Тестирование получения значения",
			fields: fields{},
			want:   nil,
		},
		{
			name: "Тестирование получения значения",
			fields: fields{
				value: &OperationStub{},
			},
			want: &OperationStub{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notOperation{
				value: tt.fields.value,
			}
			if got := n.Value(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения названия поля
func Test_notOperation_Field(t *testing.T) {
	type fields struct {
		value Operation
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "Тестирование получения названия поля",
			fields: fields{},
			want:   constants.NotSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notOperation{
				value: tt.fields.value,
			}
			if got := n.Field(); got != tt.want {
				t.Errorf("Field() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации SQL
func Test_notOperation_ToSQL(t *testing.T) {
	type fields struct {
		value Operation
	}
	type args struct {
		object    *graphql.Object
		fieldsMap map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование генерации SQL",
			fields: fields{
				value: &OperationStub{
					sql:    "test",
					sqlErr: false,
				},
			},
			args:    args{},
			want:    "not (test)",
			wantErr: false,
		},
		{
			name: "Тестирование генерации SQL (Ошибка подоперации)",
			fields: fields{
				value: &OperationStub{
					sql:    "",
					sqlErr: true,
				},
			},
			args:    args{},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notOperation{
				value: tt.fields.value,
			}

			got, err := n.ToSQL(tt.args.object, tt.args.fieldsMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if got != tt.want {
				t.Errorf("ToSQL() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование фабрики
func TestNewNotOperation(t *testing.T) {
	type args struct {
		value Operation
	}
	tests := []struct {
		name string
		args args
		want Operation
	}{
		{
			name: "Тестирование фабрики",
			args: args{
				value: &OperationStub{},
			},
			want: &notOperation{
				value: &OperationStub{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewNotOperation(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewNotOperation() = %v, want %v", got, tt.want)
			}
		})
	}
}
