package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aggregateFieldsGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации
func Test_aggregateQueryGenerator_Generate(t *testing.T) {
	objectConfig := graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	objectConfigResult := graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role_aggregate",
			Fields: graphql.Fields{
				"field_1_aggregate": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1_aggregate",
				},
			},
			Description: "Role aggregate entity",
		},
	)

	type fields struct {
		logger                   *logrus.Entry
		processors               []queryGeneratorProcessorInterface
		aggregateEntityGenerator aggregateFieldsGenerator.AggregateFieldsGeneratorInterface
		aliasGenerator           aliasGenerationService.AliasGenerationServiceInterface
	}
	type args struct {
		object   *graphql.Object
		resolver TResolver
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Field
	}{
		{
			name: "Тестирование генерации #1",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				processors: []queryGeneratorProcessorInterface{
					&queryGeneratorProcessorMock{
						name:         "test",
						isCalled:     false,
						returnResult: nil,
					},
				},
				aggregateEntityGenerator: &for_tests.AggregateFieldsGenerator{
					Result:   objectConfigResult,
					IsCalled: false,
				},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
			},
			args: args{
				object:   objectConfig,
				resolver: nil,
			},
			want: &graphql.Field{
				Type:        graphql.NewList(objectConfigResult),
				Name:        `Аггрегирующий запрос сущностей 'role'`,
				Description: `Аггрегирующий запрос сущностей 'role'`,
				Args:        graphql.FieldConfigArgument{},
				Resolve:     nil,
			},
		},
		{
			name: "Тестирование генерации #2",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				processors: []queryGeneratorProcessorInterface{
					&queryGeneratorProcessorMock{
						name:         "test",
						isCalled:     false,
						returnResult: &graphql.ArgumentConfig{},
					},
				},
				aggregateEntityGenerator: &for_tests.AggregateFieldsGenerator{
					Result:   objectConfigResult,
					IsCalled: false,
				},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
			},
			args: args{
				object:   objectConfig,
				resolver: nil,
			},
			want: &graphql.Field{
				Type:        graphql.NewList(objectConfigResult),
				Name:        `Аггрегирующий запрос сущностей 'role'`,
				Description: `Аггрегирующий запрос сущностей 'role'`,
				Args: graphql.FieldConfigArgument{
					"test": &graphql.ArgumentConfig{},
				},
				Resolve: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := aggregateQueryGenerator{
				logger:                   tt.fields.logger,
				processors:               tt.fields.processors,
				aggregateEntityGenerator: tt.fields.aggregateEntityGenerator,
				aliasGenerator:           tt.fields.aliasGenerator,
			}
			if got := a.Generate(context.Background(), tt.args.object, tt.args.resolver); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения суффикса
func Test_aggregateQueryGenerator_Suffix(t *testing.T) {
	type fields struct {
		logger                   *logrus.Entry
		processors               []queryGeneratorProcessorInterface
		aggregateEntityGenerator aggregateFieldsGenerator.AggregateFieldsGeneratorInterface
		aliasGenerator           aliasGenerationService.AliasGenerationServiceInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   constants.QueryType
	}{
		{
			name:   "Тестирование получения суффикса",
			fields: fields{},
			want:   "_aggregate",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := aggregateQueryGenerator{
				logger:                   tt.fields.logger,
				processors:               tt.fields.processors,
				aggregateEntityGenerator: tt.fields.aggregateEntityGenerator,
				aliasGenerator:           tt.fields.aliasGenerator,
			}
			if got := a.Suffix(); got != tt.want {
				t.Errorf("Suffix() = %v, want %v", got, tt.want)
			}
		})
	}
}
