package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

type inDataType = map[string]interface{}

// Процессор операции IN
type inProcessor struct {
	logger  *logrus.Entry
	factory TNewInOperation
}

// Установка парсера в процессор
func (i inProcessor) SetParser(WhereOrHavingParserInterface) {}

// Проверка доступности процессора
func (i inProcessor) IsAvailable(ctx context.Context, arguments map[string]interface{}) bool {
	for _, data := range arguments {
		if values, ok := data.(inDataType); ok {
			for _, value := range values {
				if reflect.TypeOf(value).Kind() == reflect.Slice {
					return true
				}
			}
		}
	}

	return false
}

// Парсинг параметров фильтрации процессором
func (i inProcessor) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (operation Operation, err error) {
	i.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started filter in parameters parsing")

	var operationCode string
	var field string

	var operationValue []interface{}
	for code, argument := range arguments {
		if parsedArgument, ok := argument.(inDataType); ok {
			for parsedCode, value := range parsedArgument {
				if reflect.TypeOf(value).Kind() == reflect.Slice {
					field = code
					operationCode = parsedCode

					s := reflect.ValueOf(value)
					operationValue = make([]interface{}, s.Len())

					for i := 0; i < s.Len(); i++ {
						operationValue[i] = s.Index(i).Interface()
					}

					break
				}
			}
		}
	}

	if operationCode != constants.InSchemaKey {
		err = fmt.Errorf(`operation code is incorrect for IN operation`)
		i.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	val := i.factory(
		operationCode,
		operationValue,
		field,
	)

	i.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
		"value":     val,
	}).Debug("Parsed filter in value")

	return val, nil
}

// Фабрика процессора
func newInOperationProcessor() whereOrHavingParserProcessorInterface {
	return &inProcessor{
		logger:  helpers.NewLogger(`inProcessor`),
		factory: NewInOperation,
	}
}
