package objectsParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Парсер объектов для вставки
type objectsParser struct {
	logger *logrus.Entry
}

// Парсинг значений для вставки
func (o objectsParser) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (Objects, error) {
	o.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started insert objects parsing")

	insertObjects, ok := arguments[constants.ObjectsSchemaKey]
	if !ok {
		err := fmt.Errorf(`insert objects is not passed, or schema key is incorrect. Valid schema key: "%v"`, constants.ObjectsSchemaKey)
		o.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	var result Objects
	if reflect.TypeOf(insertObjects).Kind() == reflect.Slice {
		s := reflect.ValueOf(insertObjects)
		result = make(Objects, s.Len())

		for i := 0; i < s.Len(); i++ {
			item := s.Index(i)

			var parsedItem Object
			var correct = item.CanInterface()

			if correct {
				parsedItem, correct = item.Interface().(Object)
			}

			if false == correct {
				err := fmt.Errorf(`one of passed insert object items is not object`)
				o.logger.WithFields(logrus.Fields{
					"code":      400,
					"arguments": arguments,
				}).Warning(err.Error())

				return nil, err
			}

			result[i] = parsedItem
		}
	} else {
		err := fmt.Errorf(`insert object items isn't an array`)
		o.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	o.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Insert objects parsed")

	return result, nil
}

// Фабрика парсера объектов для вставки
func NewObjectsParser() ObjectsParserInterface {
	return &objectsParser{
		logger: helpers.NewLogger(`objectsParser`),
	}
}
