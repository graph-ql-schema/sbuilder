package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"
)

// Парсер реальных операций AND и OR
type andOrProcessor struct {
	logger        *logrus.Entry
	parser        WhereOrHavingParserInterface
	operationKey  string
	operationType string
}

// Установка парсера в процессор
func (a *andOrProcessor) SetParser(parser WhereOrHavingParserInterface) {
	a.parser = parser
}

// Проверка доступности процессора
func (a andOrProcessor) IsAvailable(ctx context.Context, arguments map[string]interface{}) bool {
	data, ok := arguments[a.operationKey]
	if !ok {
		return false
	}

	return ok && reflect.TypeOf(data).Kind() == reflect.Slice
}

// Парсинг параметров фильтрации процессором
func (a andOrProcessor) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (operation Operation, err error) {
	a.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug(fmt.Sprintf("Started filter '%v' parameters parsing", a.operationKey))

	var typedData []interface{}
	data, _ := arguments[a.operationKey]
	if reflect.TypeOf(data).Kind() == reflect.Slice {
		s := reflect.ValueOf(data)
		typedData = make([]interface{}, s.Len())
		for i := 0; i < s.Len(); i++ {
			typedData[i] = s.Index(i).Interface()
		}
	}

	if 0 == len(typedData) {
		err = fmt.Errorf(`'%v' operation fiter data is not passed`, a.operationKey)
		a.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	subOperations := make([]Operation, len(typedData))
	subOperationsIndex := 0

	for _, operationData := range typedData {
		res, err := a.parser.Parse(ctx, map[string]interface{}{
			constants.AndSchemaKey: operationData,
		})

		if nil != err {
			return nil, err
		}

		if nil == res {
			continue
		}

		subOperations[subOperationsIndex] = res
		subOperationsIndex++
	}

	operations := make([]Operation, subOperationsIndex)
	for i := 0; i < subOperationsIndex; i++ {
		operations[i] = subOperations[i]
	}

	a.logger.WithFields(logrus.Fields{
		"code":       100,
		"arguments":  arguments,
		"operations": operations,
	}).Debug(fmt.Sprintf("Parsed filter '%v' parameters", a.operationKey))

	if 0 == len(operations) {
		return nil, nil
	}

	return NewAndOrOperation(a.operationType, operations, a.operationKey), nil
}

// Фабрика AND процессора
func newAndProcessor() whereOrHavingParserProcessorInterface {
	return &andOrProcessor{
		logger:        helpers.NewLogger(`andOrProcessor(root: _and)`),
		operationKey:  constants.AndSchemaKey,
		operationType: constants.AndSchemaKey,
	}
}

// Фабрика OR процессора
func newOrProcessor() whereOrHavingParserProcessorInterface {
	return &andOrProcessor{
		logger:        helpers.NewLogger(`andOrProcessor(root: _or)`),
		operationKey:  constants.OrSchemaKey,
		operationType: constants.OrSchemaKey,
	}
}
