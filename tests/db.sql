create schema test_data;
create table test_data.test
(
    id serial not null
        constraint test_pk
            primary key,
    active_from timestamptz default now() not null,
    name text,
    stat int not null,
    is_active bool default true not null
);

create index test_active_from_index
    on test_data.test (active_from desc);

create index test_is_active_index
    on test_data.test (is_active desc);

create index test_name_index
    on test_data.test (name);

create index test_stat_index
    on test_data.test (stat);

insert into test_data.test(name, stat, is_active) values
('test', 1, true),
('asd', 12, false),
('dst', 14, true),
('ddd', 120, false),
('aaa', 11, false),
('1a2', 34, true),
('2df4', 55, false),
('mg5', 49, true),
('ggndk', 88, true),
('jj', 55, false);