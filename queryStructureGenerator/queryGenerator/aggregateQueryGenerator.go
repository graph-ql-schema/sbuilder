package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aggregateFieldsGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Генератор запросов аггрегирующих сущностей для модели
type aggregateQueryGenerator struct {
	logger                   *logrus.Entry
	processors               []queryGeneratorProcessorInterface
	aggregateEntityGenerator aggregateFieldsGenerator.AggregateFieldsGeneratorInterface
	aliasGenerator           aliasGenerationService.AliasGenerationServiceInterface
}

// Получение суффикса для названия мутации
func (a aggregateQueryGenerator) Suffix() constants.QueryType {
	return constants.AggregateQuery
}

// Генерация запроса на получение сущности
func (a aggregateQueryGenerator) Generate(
	ctx context.Context,
	object *graphql.Object,
	resolver TResolver,
) *graphql.Field {
	a.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(`Generation aggregation query for entity`)

	aggregateObject := a.aggregateEntityGenerator.Generate(ctx, object)
	arguments := graphql.FieldConfigArgument{}
	for _, processor := range a.processors {
		config := processor.Generate(ctx, object, a.aliasGenerator.Generate(aggregateObject.Name()))
		if config != nil {
			arguments[processor.Name()] = config
		}
	}

	return &graphql.Field{
		Type:        graphql.NewList(aggregateObject),
		Name:        fmt.Sprintf(`Аггрегирующий запрос сущностей '%v'`, object.Name()),
		Description: fmt.Sprintf(`Аггрегирующий запрос сущностей '%v'`, object.Name()),
		Args:        arguments,
		Resolve:     resolver,
	}
}

// Фабрика генератора запроса аггрегации сущностей
func AggregateQueryGeneratorFactory() QueryGeneratorInterface {
	return &aggregateQueryGenerator{
		logger: helpers.NewLogger(`aggregateQueryGenerator`),
		processors: []queryGeneratorProcessorInterface{
			newGroupByGeneratorProcessor(),
			newWhereOrHavingQueryGeneratorProcessor(constants.HavingSchemaKey),
			newWhereOrHavingQueryGeneratorProcessor(constants.WhereSchemaKey),
			newLimitQueryGeneratorProcessor(),
			newOffsetQueryGeneratorProcessor(),
		},
		aggregateEntityGenerator: aggregateFieldsGenerator.AggregateFieldsGenerator(),
		aliasGenerator:           aliasGenerationService.AliasGenerationService(),
	}
}
