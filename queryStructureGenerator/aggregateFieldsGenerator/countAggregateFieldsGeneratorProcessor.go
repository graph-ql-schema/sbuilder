package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
)

// Процессор для генерации аггрегирующей сущности. Генерирует поле 'count'
type countAggregateFieldsGeneratorProcessor struct {
	logger          *logrus.Entry
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Генерация поля для сущности
func (c countAggregateFieldsGeneratorProcessor) GenerateField(ctx context.Context, object *graphql.Object) (*graphql.Field, string) {
	if false == c.isSimpleFieldAvailable(object) {
		return nil, ""
	}

	c.logger.WithFields(logrus.Fields{
		"code":   100,
		"field":  constants.CountOperationSchemaKey,
		"object": object.Name(),
	}).Debug(fmt.Sprintf(`Generation aggregation field '%v'`, constants.CountOperationSchemaKey))

	return &graphql.Field{
		Type:        graphql.Int,
		Name:        fmt.Sprintf(`%v_aggregate_count_field`, strings.ReplaceAll(strings.ToLower(object.Name()), ` `, `_`)),
		Description: fmt.Sprintf(`Получение количества элементов сущности во всей таблице или в каждой группе отдельно, при наличии GroupBy для сущности: '%v'`, object.Name()),
	}, constants.CountOperationSchemaKey
}

// Проверяет доступность простого поля в объекте. По сути метод получения количества должен быть доступен
// только для объектов с простыми полями, количество значений которых возможно вычислить.
func (c countAggregateFieldsGeneratorProcessor) isSimpleFieldAvailable(object *graphql.Object) bool {
	for _, field := range object.Fields() {
		if nil != field.Resolve {
			continue
		}

		fType := c.fieldTypeGetter.GetRootType(field.Type)
		if _, ok := fType.(*graphql.Object); ok {
			continue
		}

		return true
	}

	return false
}

// Фабрика процессора генератора
func newCountAggregateFieldsGeneratorProcessor() aggregateFieldsGeneratorProcessorInterface {
	return &countAggregateFieldsGeneratorProcessor{
		logger:          helpers.NewLogger(`countAggregateFieldsGeneratorProcessor`),
		fieldTypeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
