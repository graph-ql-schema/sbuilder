package wsResolveGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"github.com/graphql-go/graphql"
)

// Валидный результат работы резолвера подписчика
type ResolverResponse struct {
	EventType  types.TEventType `json:"eventType"`
	EntityId   string           `json:"entityId"`
	EntityData interface{}      `json:"data"`
}

// Параметры посредника для обработчика подписчика
type SubscriptionMiddlewareParams struct {
	User       interface{}
	Object     *graphql.Object
	EventType  types.TEventType
	EntityId   string
	EntityData interface{}
}

// Посредник обработчика подписчика
type TSubscriptionMiddleware = func(params SubscriptionMiddlewareParams) (entityData interface{}, err error)

// Резолвер обработчика подписчика
type TResolver = func(p graphql.ResolveParams) (i interface{}, e error)

// Интерфейс генератора резолвера сущностей для подписчиков
type WsResolveGeneratorInterface interface {
	// Создание резолвера
	MakeResolver(
		object *graphql.Object,
		middleware TSubscriptionMiddleware,
	) TResolver
}
