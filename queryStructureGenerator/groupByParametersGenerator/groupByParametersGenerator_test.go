package groupByParametersGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	for_tests "bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование генератора группировки
func Test_groupByParametersGenerator_Generate(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		logger     *logrus.Entry
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.ArgumentConfig
	}{
		{
			name: "Тестирование генератора группировки",
			fields: fields{
				logger:     helpers.NewLogger(`test`),
				typeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "role",
			},
			want: &graphql.ArgumentConfig{
				Type: graphql.NewList(graphql.NewEnum(graphql.EnumConfig{
					Name: `role_group_by_parameters_group_by`,
					Values: graphql.EnumValueConfigMap{
						"field_1": &graphql.EnumValueConfig{
							Value:       "field_1",
							Description: "Значение field_1",
						},
					},
					Description: "Варианты выбора полей для группировки",
				})),
				Description: "Группировка по переданным параметрам",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupByParametersGenerator{
				logger:     tt.fields.logger,
				typeGetter: tt.fields.typeGetter,
			}
			if got := g.Generate(context.Background(), tt.args.object, tt.args.namePrefix); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
