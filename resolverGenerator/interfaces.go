package resolverGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"context"
	"github.com/graphql-go/graphql"
)

// Резолвер сущности
type TResolver = func(graphql.ResolveParams) (interface{}, error)

// Генератор резолверов
type ResolverGeneratorInterface interface {
	// Генерация резолвера
	Generate(
		ctx context.Context,
		object *graphql.Object,
		resolver types.RequestResolver,
	) TResolver
}
