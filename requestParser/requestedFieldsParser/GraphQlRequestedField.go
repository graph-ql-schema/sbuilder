package requestedFieldsParser

// Сущность запрошенного поля
type graphQlRequestedField struct {
	fieldName string
	subFields GraphQlRequestedFields
}

// Получение названия поля
func (g graphQlRequestedField) GetFieldName() string {
	return g.fieldName
}

// Есть ли у поля дочерние элементы
func (g graphQlRequestedField) HasSubFields() bool {
	return 0 != len(g.subFields)
}

// Получение дочерних элементов поля
func (g graphQlRequestedField) GetSubFields() GraphQlRequestedFields {
	if nil == g.subFields {
		return GraphQlRequestedFields{}
	}

	return g.subFields
}

// Фабрика сущности поля
func NewGraphQlRequestedField(fieldName string, subFields GraphQlRequestedFields) GraphQlRequestedFieldInterface {
	fields := subFields
	if nil == subFields {
		fields = GraphQlRequestedFields{}
	}

	return &graphQlRequestedField{
		fieldName: fieldName,
		subFields: fields,
	}
}
