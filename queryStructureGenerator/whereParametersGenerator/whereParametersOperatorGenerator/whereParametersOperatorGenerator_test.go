package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

var objectConfig = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "role",
		Fields: graphql.Fields{
			"field_1": &graphql.Field{
				Type: graphql.Int,
				Name: "field_1",
			},
			"field_2": &graphql.Field{
				Type: graphql.Int,
				Name: "field_2",
			},
		},
		Description: "Role entity",
	},
)

// Тестирование генератора операций
func Test_whereParametersOperatorGenerator_GenerateOperations(t *testing.T) {
	type fields struct {
		processors      []whereParametersOperatorGeneratorProcessorInterface
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
		level      uint8
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		isGenerationCalled     bool
		isConfigGenerated      bool
		fieldsGenerationStatus map[string]bool
	}{
		{
			name: "Тестирование генерации с передачей процессора, который не подходит к полям.",
			fields: fields{
				processors: []whereParametersOperatorGeneratorProcessorInterface{
					&whereParametersOperatorGeneratorProcessorMock{
						operation:          "test",
						isAvailable:        false,
						isGenerationCalled: false,
						generationResult:   nil,
					},
				},
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "",
				level:      0,
			},
			isGenerationCalled: false,
			isConfigGenerated:  false,
			fieldsGenerationStatus: map[string]bool{
				"field_1": false,
				"field_2": false,
			},
		},
		{
			name: "Тестирование генерации с передачей процессора, который подходит к полям, но не генерирует результат.",
			fields: fields{
				processors: []whereParametersOperatorGeneratorProcessorInterface{
					&whereParametersOperatorGeneratorProcessorMock{
						operation:          "test",
						isAvailable:        true,
						isGenerationCalled: false,
						generationResult:   nil,
					},
				},
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "",
				level:      0,
			},
			isGenerationCalled: true,
			isConfigGenerated:  false,
			fieldsGenerationStatus: map[string]bool{
				"field_1": false,
				"field_2": false,
			},
		},
		{
			name: "Тестирование генерации с передачей процессора, который подходит к полям и генерирует результат.",
			fields: fields{
				processors: []whereParametersOperatorGeneratorProcessorInterface{
					&whereParametersOperatorGeneratorProcessorMock{
						operation:          "test",
						isAvailable:        true,
						isGenerationCalled: false,
						generationResult:   &graphql.InputObjectFieldConfig{},
					},
				},
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "",
				level:      0,
			},
			isGenerationCalled: true,
			isConfigGenerated:  true,
			fieldsGenerationStatus: map[string]bool{
				"field_1": true,
				"field_2": true,
			},
		},
		{
			name: "Тестирование генерации с передачей нескольких валидных процессоров.",
			fields: fields{
				processors: []whereParametersOperatorGeneratorProcessorInterface{
					&whereParametersOperatorGeneratorProcessorMock{
						operation:          "test",
						isAvailable:        true,
						isGenerationCalled: false,
						generationResult:   &graphql.InputObjectFieldConfig{},
					},
					&whereParametersOperatorGeneratorProcessorMock{
						operation:          "test_2",
						isAvailable:        true,
						isGenerationCalled: false,
						generationResult:   &graphql.InputObjectFieldConfig{},
					},
				},
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "",
				level:      0,
			},
			isGenerationCalled: true,
			isConfigGenerated:  true,
			fieldsGenerationStatus: map[string]bool{
				"field_1": true,
				"field_2": true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := whereParametersOperatorGenerator{
				processors:      tt.fields.processors,
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}

			got := w.GenerateOperations(context.Background(), tt.args.object, tt.args.namePrefix, tt.args.level)
			for i, proc := range tt.fields.processors {
				processor := proc.(*whereParametersOperatorGeneratorProcessorMock)
				if processor.isGenerationCalled != tt.isGenerationCalled {
					t.Errorf("GenerateOperations() Processor #%v call status %v, want %v", i, processor.isGenerationCalled, tt.isGenerationCalled)
				}
			}

			if tt.isConfigGenerated && nil == got {
				t.Errorf("GenerateOperations() config is generated, but should not.")

				return
			}

			if false == tt.isConfigGenerated {
				return
			}

			for name := range *got {
				status := tt.fieldsGenerationStatus[name]
				if status == false {
					t.Errorf("GenerateOperations() field %v should not be generated", name)
				}
			}
		})
	}
}
