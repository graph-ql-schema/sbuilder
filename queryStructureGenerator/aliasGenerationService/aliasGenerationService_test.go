package aliasGenerationService

import "testing"

// Тестирвоание генерации алиса
func Test_aliasGenerationService_Generate(t *testing.T) {
	type fields struct {
		replacements symbolReplaceMap
	}
	type args struct {
		source string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование на пустой строке",
			fields: fields{
				replacements: symbolReplaceMap{
					"а": "i",
					"б": "b",
					"-": "_",
					" ": "_",
				},
			},
			args: args{
				source: "",
			},
			want: "",
		},
		{
			name: "Тестирование на строке из 3 валидных символов",
			fields: fields{
				replacements: symbolReplaceMap{
					"а": "i",
					"б": "b",
					"-": "_",
					" ": "_",
				},
			},
			args: args{
				source: "ааа",
			},
			want: "iii",
		},
		{
			name: "Тестирование на строке из смешанных символов",
			fields: fields{
				replacements: symbolReplaceMap{
					"а": "i",
					"б": "b",
					"-": "_",
					" ": "_",
				},
			},
			args: args{
				source: "абВ_а",
			},
			want: "ibi",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := aliasGenerationService{
				replacements: tt.fields.replacements,
			}
			if got := a.Generate(tt.args.source); got != tt.want {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
