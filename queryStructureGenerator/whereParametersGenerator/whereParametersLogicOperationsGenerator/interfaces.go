package whereParametersLogicOperationsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"context"
	"github.com/graphql-go/graphql"
)

// Генератор логических операций для Where
type WhereParametersLogicOperationsGeneratorInterface interface {
	// Установка генератора параметров для процессора
	SetWhereParametersOperatorGenerator(interfaces.WhereParametersGeneratorInterface)

	// Генерация параметров
	GenerateParametersForObject(context.Context, *graphql.Object, uint8, string) *graphql.InputObjectConfigFieldMap
}

// Процессор генератора логических операций для Where
type whereParametersLogicOperationsGeneratorProcessorInterface interface {
	// Установка генератора параметров для процессора
	SetWhereParametersOperatorGenerator(interfaces.WhereParametersGeneratorInterface)

	// Код операции, для вставки в схему GraphQL
	OperationCode() string

	// Генерация параметров
	Generate(context.Context, *graphql.Object, uint8, string) *graphql.InputObjectFieldConfig
}
