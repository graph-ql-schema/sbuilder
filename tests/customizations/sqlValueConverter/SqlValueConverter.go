package sqlValueConverter

import (
    gql_root_type_getter "bitbucket.org/graph-ql-schema/gql-root-type-getter"
    gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
    "fmt"
    "github.com/graphql-go/graphql"
)

// Конвертер значений для парсера параметров
type sqlValueConverter struct {
    typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Конструктор конвертера
func newSqlValueConverter() gql_sql_converter.GraphQlSqlConverterInterface {
    return &sqlValueConverter{
        typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
    }
}

// Конвертация в базовый тип, например в строку или число
func (c sqlValueConverter) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
    if nil == object {
        return nil, fmt.Errorf(`passed nil object to value converter`)
    }

    fieldData, ok := object.Fields()[field]
    if !ok {
        return nil, fmt.Errorf(`field '%v' is not found in passed structure`, field)
    }

    fieldType := c.typeGetter.GetRootType(fieldData.Type)
    if fieldType.String() != `Ltree` {
        return nil, fmt.Errorf(`no available processor for field '%v'`, field)
    }

    result := fmt.Sprintf(`%v`, value)
    if nil == value || result == "nil"{
        return nil, nil
    }

    return result, nil
}

// Конвертация в SQL like значение
func (c sqlValueConverter) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
    if nil == object {
        return "", fmt.Errorf(`passed nil object to value converter`)
    }

    fieldData, ok := object.Fields()[field]
    if !ok {
        return "", fmt.Errorf(`field '%v' is not found in passed structure`, field)
    }

    fieldType := c.typeGetter.GetRootType(fieldData.Type)
    if fieldType.String() != `Ltree` {
        return "", fmt.Errorf(`no available processor for field '%v'`, field)
    }

    val, err := c.ToBaseType(object, field, value)
    if nil != err {
        return "", err
    }

    if nil == val {
        return "null", nil
    }

    return fmt.Sprintf(`%v`, val), nil
}