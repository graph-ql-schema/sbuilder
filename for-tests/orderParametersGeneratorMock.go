package for_tests

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type OrderParametersGeneratorMock struct {
	IsCalled bool
	Result   *graphql.ArgumentConfig
}

// Генерация параметров сортировки
func (o *OrderParametersGeneratorMock) Generate(
	context.Context,
	*graphql.Object,
	string,
) *graphql.ArgumentConfig {
	o.IsCalled = true

	return o.Result
}
