package aggregateFieldsGenerator

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Резолвер сущности
type TResolver = func(graphql.ResolveParams) (interface{}, error)

// Интерфейс генератора аггрегирующей сущности для модели
type AggregateFieldsGeneratorInterface interface {
	// Генерация сущности
	Generate(ctx context.Context, object *graphql.Object) *graphql.Object
}

// Процессор для генерации аггрегирующей сущности
type aggregateFieldsGeneratorProcessorInterface interface {
	// Генерация поля для сущности
	GenerateField(ctx context.Context, object *graphql.Object) (*graphql.Field, string)
}
