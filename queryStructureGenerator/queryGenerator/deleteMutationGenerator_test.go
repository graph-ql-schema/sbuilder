package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации запроса
func Test_deleteMutationGenerator_Generate(t *testing.T) {
	object := graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		logger         *logrus.Entry
		typeGetter     gql_root_type_getter.GraphQlRootTypeGetterInterface
		processors     []queryGeneratorProcessorInterface
		aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
	}
	type args struct {
		object   *graphql.Object
		resolver TResolver
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Field
	}{
		{
			name: "Тестирование генерации мутации без валидных процессоров.",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				typeGetter:     &for_tests.GraphQlRootTypeGetterMock{},
				processors:     []queryGeneratorProcessorInterface{},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
			},
			args: args{
				object:   object,
				resolver: nil,
			},
			want: nil,
		},
		{
			name: "Тестирование генерации мутации без валидных процессоров.",
			fields: fields{
				logger:     helpers.NewLogger(`test`),
				typeGetter: &for_tests.GraphQlRootTypeGetterMock{},
				processors: []queryGeneratorProcessorInterface{
					&queryGeneratorProcessorMock{
						name:         "test",
						isCalled:     false,
						returnResult: &graphql.ArgumentConfig{},
					},
				},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
			},
			args: args{
				object:   object,
				resolver: nil,
			},
			want: &graphql.Field{
				Type: graphql.NewObject(
					graphql.ObjectConfig{
						Name: `Role_delete_result_type`,
						Fields: graphql.Fields{
							"affected_rows": &graphql.Field{
								Type:        graphql.NewNonNull(graphql.Int),
								Name:        `Role_delete_affected`,
								Description: "Количество записей, удаленных запросом",
							},
						},
						Description: `Результат удаления значений для сущности 'Role'`,
					},
				),
				Name:        `Role_delete_mutation`,
				Description: "Мутация удаления значений объекта 'Role'",
				Args: graphql.FieldConfigArgument{
					"test": &graphql.ArgumentConfig{},
				},
				Resolve: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := deleteMutationGenerator{
				logger:         tt.fields.logger,
				typeGetter:     tt.fields.typeGetter,
				processors:     tt.fields.processors,
				aliasGenerator: tt.fields.aliasGenerator,
			}
			if got := d.Generate(context.Background(), tt.args.object, tt.args.resolver); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения суффикса
func Test_deleteMutationGenerator_Suffix(t *testing.T) {
	type fields struct {
		logger         *logrus.Entry
		typeGetter     gql_root_type_getter.GraphQlRootTypeGetterInterface
		processors     []queryGeneratorProcessorInterface
		aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   constants.QueryType
	}{
		{
			name:   "Тестирование получения суффикса",
			fields: fields{},
			want:   "_delete",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := deleteMutationGenerator{
				logger:         tt.fields.logger,
				typeGetter:     tt.fields.typeGetter,
				processors:     tt.fields.processors,
				aliasGenerator: tt.fields.aliasGenerator,
			}
			if got := d.Suffix(); got != tt.want {
				t.Errorf("Suffix() = %v, want %v", got, tt.want)
			}
		})
	}
}
