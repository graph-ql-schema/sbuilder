package contextParser

import (
	"context"

	"github.com/valyala/fasthttp"
)

// Парсер запроса для формирования контекста
type contextParser struct{}

// Парсинг запроса для выделения контекста
func (c contextParser) Parse(ctx *fasthttp.RequestCtx) context.Context {
	return context.WithValue(context.Background(), "fastHttpCtx", ctx)
}

// Фабрика парсера
func NewContextParser() ContextParserInterface {
	return &contextParser{}
}
