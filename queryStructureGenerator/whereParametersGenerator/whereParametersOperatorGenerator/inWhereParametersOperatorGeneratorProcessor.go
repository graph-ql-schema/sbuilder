package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/nullable"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
)

// Процессор генерации оператора "_in"
type inWhereParametersOperatorGeneratorProcessor struct {
	availableTypes  []graphql.Type
	maxLevel        uint8
	logger          *logrus.Entry
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Проверка доступности процессора
func (i inWhereParametersOperatorGeneratorProcessor) IsAvailable(ctx context.Context, field *graphql.FieldDefinition, level uint8) bool {
	if level > i.maxLevel {
		return false
	}

	if field.Resolve != nil {
		return false
	}

	fieldRootType := i.fieldTypeGetter.GetRootType(field.Type)
	for _, fType := range i.availableTypes {
		if fType == fieldRootType {
			return true
		}
	}

	return false
}

// Код оператора, для вставки в схему GraphQL
func (i inWhereParametersOperatorGeneratorProcessor) OperatorCode() string {
	return constants.InSchemaKey
}

// Генерация параметров
func (i inWhereParametersOperatorGeneratorProcessor) Generate(
	ctx context.Context,
	field *graphql.FieldDefinition,
	level uint8,
	namePrefix string,
) *graphql.InputObjectFieldConfig {
	rType := i.fieldTypeGetter.GetRootType(field.Type)
	if scalar, ok := rType.(*graphql.Scalar); ok && i.fieldTypeGetter.IsNullable(field.Type) {
		rType = nullable.NewNullable(scalar)
	}

	i.logger.WithFields(logrus.Fields{
		"code":       100,
		"field":      field.Name,
		"level":      level,
		"namePrefix": namePrefix,
	}).Debug(`Generated _IN operator for field`)

	return &graphql.InputObjectFieldConfig{
		Type:        graphql.NewList(rType),
		Description: "Операция множественного сравнения значения поля с переданным",
	}
}

// Фабрика процессора
func newInWhereParametersOperatorGeneratorProcessor(
	availableTypes []graphql.Type,
	maxLevel uint8,
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface,
) whereParametersOperatorGeneratorProcessorInterface {
	return &inWhereParametersOperatorGeneratorProcessor{
		availableTypes:  availableTypes,
		maxLevel:        maxLevel,
		logger:          helpers.NewLogger(`inWhereParametersOperatorGeneratorProcessor`),
		fieldTypeGetter: fieldTypeGetter,
	}
}
