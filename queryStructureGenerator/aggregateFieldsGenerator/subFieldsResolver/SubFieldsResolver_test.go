package subFieldsResolver

import (
	"reflect"
	"testing"
	"time"

	"github.com/graphql-go/graphql"
)

// Тестирвоание резолвинга полей сущности для API GraphQL
func Test_subFieldsResolver_Resolve(t *testing.T) {
	tVal := time.Now()
	pTimeVal, _ := time.Parse(time.RFC3339, "2020-01-01T12:05:05.000Z")

	type fields struct {
		fieldCode string
		fieldType graphql.Type
	}
	type args struct {
		p graphql.ResolveParams
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Не передан листинг сущностей",
			fields: fields{
				fieldCode: "test",
				fieldType: graphql.Float,
			},
			args: args{
				p: graphql.ResolveParams{
					Source: "test",
				},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "Пустой листинг сущностей",
			fields: fields{
				fieldCode: "test",
				fieldType: graphql.Float,
			},
			args: args{
				p: graphql.ResolveParams{
					Source: []interface{}{},
				},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "Листинг сущностей с одним значением",
			fields: fields{
				fieldCode: "test",
				fieldType: graphql.String,
			},
			args: args{
				p: graphql.ResolveParams{
					Source: map[string]interface{}{
						"test": "test",
					},
				},
			},
			want:    "test",
			wantErr: false,
		},
		{
			name: "Листинг сущностей с двумя не уникальными значениями",
			fields: fields{
				fieldCode: "test",
				fieldType: graphql.String,
			},
			args: args{
				p: graphql.ResolveParams{
					Source: map[string]interface{}{
						"test": []interface{}{
							"test",
							"test",
						},
					},
				},
			},
			want: []interface{}{
				"test",
			},
			wantErr: false,
		},
		{
			name: "Листинг сущностей с двумя не уникальными значениями дат",
			fields: fields{
				fieldCode: "test",
				fieldType: graphql.DateTime,
			},
			args: args{
				p: graphql.ResolveParams{
					Source: map[string]interface{}{
						"test": []interface{}{
							tVal,
							tVal,
						},
					},
				},
			},
			want: []interface{}{
				tVal,
			},
			wantErr: false,
		},
		{
			name: "Листинг сущностей с двумя не уникальными значениями дат в строковом формате",
			fields: fields{
				fieldCode: "test",
				fieldType: graphql.DateTime,
			},
			args: args{
				p: graphql.ResolveParams{
					Source: map[string]interface{}{
						"test": []interface{}{
							"2020-01-01T12:05:05.000Z",
							"2020-01-01T12:05:05.000Z",
						},
					},
				},
			},
			want: []interface{}{
				pTimeVal,
			},
			wantErr: false,
		},
		{
			name: "Листинг сущностей с двумя значениями дат в строковом не корректном формате",
			fields: fields{
				fieldCode: "test",
				fieldType: graphql.DateTime,
			},
			args: args{
				p: graphql.ResolveParams{
					Source: map[string]interface{}{
						"test": []interface{}{
							"2020-01-01T12:05:05.Ф000Z",
							"2020-01-01T12:05:05.Ф000Z",
						},
					},
				},
			},
			want:    []interface{}{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := subFieldsResolver{
				fieldCode: tt.fields.fieldCode,
				fieldType: tt.fields.fieldType,
			}
			got, err := s.Resolve(tt.args.p)
			if (err != nil) != tt.wantErr {
				t.Errorf("Resolve() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Resolve() got = %v, want %v", got, tt.want)
			}
		})
	}
}
