package whereOrHavingParser

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
)

// Значение операции between
type BetweenValue struct {
	From interface{}
	To   interface{}
}

// Операция between
type betweenOperation struct {
	value          BetweenValue
	field          string
	valueConverter gql_sql_converter.GraphQlSqlConverterInterface
}

// Получение типа текущей операции
func (b betweenOperation) Type() string {
	return constants.BetweenSchemaKey
}

// Получение значения для текущей операции
func (b betweenOperation) Value() interface{} {
	return b.value
}

// Получение кода поля для операции
func (b betweenOperation) Field() string {
	return b.field
}

// Конвертация в SQL
func (b betweenOperation) ToSQL(object *graphql.Object, fieldsMap map[string]string) (string, error) {
	fieldName := b.field
	if mappedField, ok := fieldsMap[fieldName]; ok {
		fieldName = mappedField
	}

	from, fromErr := b.valueConverter.ToSQLValue(object, b.field, b.value.From)
	if nil != fromErr {
		return "", fmt.Errorf(`'%v' field, 'from' value: %v`, b.field, fromErr.Error())
	}

	to, toErr := b.valueConverter.ToSQLValue(object, b.field, b.value.To)
	if nil != toErr {
		return "", fmt.Errorf(`'%v' field, 'to' value: %v`, b.field, toErr.Error())
	}

	return fmt.Sprintf(`%v between %v and %v`, fieldName, from, to), nil
}

// Фабрика операций IN
type TNewBetweenOperation = func(BetweenValue, string) Operation

func NewBetweenOperation(value BetweenValue, field string) Operation {
	return &betweenOperation{
		value:          value,
		field:          field,
		valueConverter: gql_sql_converter.NewGraphQlSqlConverter(),
	}
}
