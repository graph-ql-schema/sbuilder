package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"github.com/graphql-go/graphql"
)

// Процессор генерации параметров Where
type whereOrHavingQueryGeneratorProcessor struct {
	logger         *logrus.Entry
	whereGenerator interfaces.WhereParametersGeneratorInterface
	name           string
}

// Получение названия генерируемого поля
func (w whereOrHavingQueryGeneratorProcessor) Name() string {
	return w.name
}

// Генерация
func (w whereOrHavingQueryGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
) *graphql.ArgumentConfig {
	w.logger.WithFields(logrus.Fields{
		"code":       100,
		"object":     object.Name(),
		"namePrefix": namePrefix,
	}).Debug(fmt.Sprintf(`Generation parameters for '%v' parameter`, w.name))

	namePrefixImproved := fmt.Sprintf(
		`%v_query_%v`,
		namePrefix,
		w.name,
	)

	fieldsConfig := w.whereGenerator.BuildParameters(ctx, object, namePrefixImproved, 1)
	if nil == fieldsConfig {
		return nil
	}

	return &graphql.ArgumentConfig{
		Type: graphql.NewInputObject(graphql.InputObjectConfig{
			Name:        fmt.Sprintf(`%v_object`, namePrefixImproved),
			Fields:      *fieldsConfig,
			Description: fmt.Sprintf(`%v: Фильтр '%v' для листинга сущностей.`, object.Name(), w.name),
		}),
		Description: fmt.Sprintf(`%v: Фильтр '%v' для листинга сущностей.`, object.Name(), w.name),
	}
}

// Фабрика процессора генерации параметров Where
func newWhereOrHavingQueryGeneratorProcessor(name string) queryGeneratorProcessorInterface {
	return &whereOrHavingQueryGeneratorProcessor{
		logger:         helpers.NewLogger(fmt.Sprintf(`whereOrHavingQueryGeneratorProcessor(Operator: %v)`, name)),
		whereGenerator: whereParametersGenerator.WhereParametersGeneratorFactory(),
		name:           name,
	}
}

// Фабрика процессора генерации параметров Where с кастомизацией
func newWhereOrHavingQueryGeneratorProcessorWithCustomizations(
	name string,
	customizations []customizationService.CustomizationInterface,
) queryGeneratorProcessorInterface {
	return &whereOrHavingQueryGeneratorProcessor{
		logger:         helpers.NewLogger(fmt.Sprintf(`whereOrHavingQueryGeneratorProcessor(Operator: %v)`, name)),
		whereGenerator: whereParametersGenerator.WhereParametersGeneratorFactoryWithCustomizations(customizations),
		name:           name,
	}
}
