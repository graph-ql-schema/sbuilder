package main

import (
	"bitbucket.org/graph-ql-schema/nullable"
	"github.com/graphql-go/graphql"
)

var sqlTestObj = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "test",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.ID,
				Name: "id",
			},
			"name": &graphql.Field{
				Type: graphql.String,
				Name: "name",
			},
			"active_date": &graphql.Field{
				Type: graphql.DateTime,
				Name: "active_date",
			},
			"stat": &graphql.Field{
				Type: nullable.NewNullable(graphql.Int),
				Name: "stat",
			},
			"is_active": &graphql.Field{
				Type: graphql.NewNonNull(graphql.Boolean),
				Name: "is_active",
			},
		},
		Description: "test entity",
	},
)
