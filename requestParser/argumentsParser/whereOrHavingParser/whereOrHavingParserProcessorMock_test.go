package whereOrHavingParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования
type whereOrHavingParserProcessorMock struct {
	parser       WhereOrHavingParserInterface
	isAvailable  bool
	IsParseError bool
	ParseResult  Operation
}

// Установка парсера в процессор
func (w *whereOrHavingParserProcessorMock) SetParser(parser WhereOrHavingParserInterface) {
	w.parser = parser
}

// Проверка доступности процессора
func (w whereOrHavingParserProcessorMock) IsAvailable(context.Context, map[string]interface{}) bool {
	return w.isAvailable
}

// Парсинг параметров фильтрации процессором
func (w whereOrHavingParserProcessorMock) Parse(
	context.Context,
	map[string]interface{},
) (operation Operation, err error) {
	if w.IsParseError {
		return nil, fmt.Errorf(`test`)
	}

	return w.ParseResult, nil
}
