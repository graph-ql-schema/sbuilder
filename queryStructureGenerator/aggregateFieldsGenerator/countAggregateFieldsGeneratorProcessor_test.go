package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	for_tests "bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации поля
func Test_countAggregateFieldsGeneratorProcessor_GenerateField(t *testing.T) {
	var object = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"id": &graphql.Field{
					Type: graphql.ID,
					Name: "Role ID",
				},
				"name": &graphql.Field{
					Type: graphql.NewNonNull(graphql.String),
					Name: "Role name",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object *graphql.Object
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Field
		want1  string
	}{
		{
			name: "Тестирование генерации на объекте без подходящих полей.",
			fields: fields{
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name:        "test",
						Fields:      graphql.Fields{},
						Description: "Test entity",
					},
				),
			},
			want:  nil,
			want1: "",
		},
		{
			name: "Тестирование генерации на объекте c подходящими полями.",
			fields: fields{
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object: object,
			},
			want: &graphql.Field{
				Type:        graphql.Int,
				Name:        `role_aggregate_count_field`,
				Description: `Получение количества элементов сущности во всей таблице или в каждой группе отдельно, при наличии GroupBy для сущности: 'role'`,
			},
			want1: "count",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := countAggregateFieldsGeneratorProcessor{
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}
			got, got1 := c.GenerateField(context.Background(), tt.args.object)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateField() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GenerateField() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
