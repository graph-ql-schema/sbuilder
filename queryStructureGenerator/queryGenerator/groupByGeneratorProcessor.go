package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/groupByParametersGenerator"
	"context"
	"github.com/graphql-go/graphql"
)

// Процессор для генерации параметров группировки
type groupByGeneratorProcessor struct {
	groupByGenerator groupByParametersGenerator.GroupByParametersGeneratorInterface
}

// Получение названия генерируемого поля
func (g groupByGeneratorProcessor) Name() string {
	return constants.GroupBySchemaKey
}

// Генерация
func (g groupByGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
) *graphql.ArgumentConfig {
	return g.groupByGenerator.Generate(ctx, object, namePrefix)
}

// Фабрика процессора генерации
func newGroupByGeneratorProcessor() queryGeneratorProcessorInterface {
	return &groupByGeneratorProcessor{
		groupByGenerator: groupByParametersGenerator.GroupByParametersGenerator(),
	}
}
