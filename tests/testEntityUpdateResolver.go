package main

import (
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"context"
	"database/sql"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strings"
	"time"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/tests/pgsql"
)

// Резолвер обновления сущности
func testEntityUpdateResolver(params sbuilder.Parameters) (i interface{}, err error) {
	valueConverter := gql_sql_converter.NewGraphQlSqlConverter()

	if 0 == len(params.Arguments.Set) {
		return nil, fmt.Errorf(`you should pass any field to store in 'set'`)
	}

	transaction, err := pgsql.Connection.Begin()
	if nil != err {
		return nil, err
	}

	if nil == transaction {
		return nil, fmt.Errorf(`can't start transaction`)
	}

	mappedFields := map[string]string{
		"active_date": "active_from",
	}

	fields := []string{}
	arguments := []interface{}{}
	for code, val := range params.Arguments.Set {
		fieldCode := code
		if mappedField, ok := mappedFields[fieldCode]; ok {
			fieldCode = mappedField
		}

		convertedVal, err := valueConverter.ToSQLValue(params.GraphQlObject, code, val)
		if nil != err {
			_ = transaction.Rollback()
			return nil, err
		}

		arguments = append(arguments, convertedVal)

		fields = append(
			fields,
			fmt.Sprintf(
				`%v = $%v`,
				fieldCode,
				len(arguments),
			),
		)
	}

	if 0 == len(fields) {
		_ = transaction.Rollback()
		return nil, fmt.Errorf(`you should pass any field to store in 'set'`)
	}

	query := `update test_data.test set ` + strings.Join(fields, `, `)
	where := ""
	if nil != params.Arguments.Where {
		where, err = params.Arguments.Where.ToSQL(params.GraphQlObject, mappedFields)

		if nil != err {
			_ = transaction.Rollback()
			return nil, err
		}
	}

	if 0 != len(where) {
		query += ` where ` + where
	}

	var result []TestData
	query += ` returning id, active_from, name, stat, is_active`

	log.Println(query)
	rows, err := transaction.Query(query, arguments...)

	if nil != err {
		_ = transaction.Rollback()
		return nil, err
	}

	for rows.Next() {
		var id int64
		var activeDate time.Time
		var name string
		var stat sql.NullInt32
		var isActive bool

		err := rows.Scan(&id, &activeDate, &name, &stat, &isActive)
		if nil != err {
			_ = transaction.Rollback()
			return nil, err
		}

		var statVal *int = nil
		if stat.Valid {
			intVal := int(stat.Int32)
			statVal = &intVal
		}

		data := TestData{
			Id:         id,
			ActiveDate: activeDate,
			Name:       name,
			Stat:       statVal,
			IsActive:   isActive,
		}

		_ = wsDispatcher.DispatchEvent(context.Background(), sqlTestObj, sbuilder.EventTypeUpdated, fmt.Sprintf(`%v`, id), data)

		result = append(result, data)
	}

	_ = transaction.Commit()

	return map[string]interface{}{
		"affected_rows": len(result),
		"returning":     result,
	}, nil
}
