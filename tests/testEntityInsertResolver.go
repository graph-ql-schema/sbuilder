package main

import (
	"context"
	"database/sql"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strings"
	"time"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/tests/pgsql"
)

// Резолвер вставки сущностей
func testEntityInsertResolver(params sbuilder.Parameters) (i interface{}, err error) {
	if 0 == len(params.Arguments.Objects) {
		return nil, fmt.Errorf(`you should pass any entity to store in 'objects'`)
	}

	transaction, err := pgsql.Connection.Begin()
	if nil != err {
		return nil, err
	}

	if nil == transaction {
		return nil, fmt.Errorf(`can't start transaction`)
	}

	ids := []string{}
	for _, object := range params.Arguments.Objects {
		id, err := insertNewEntity(object, transaction, map[string]string{
			"active_date": "active_from",
		})

		if nil != err {
			_ = transaction.Rollback()
			return nil, err
		}

		if nil == id {
			_ = transaction.Rollback()
			return nil, fmt.Errorf(`failed to create entity. returned empty ID`)
		}

		ids = append(ids, fmt.Sprintf(`%v`, *id))
	}

	if 0 == len(ids) {
		_ = transaction.Rollback()
		return map[string]interface{}{
			"affected_rows": 0,
			"returning":     []TestData{},
		}, nil
	}

	var result []TestData
	q := fmt.Sprintf(
		`select id, active_from, name, stat, is_active from test_data.test where id in (%v)`,
		strings.Join(ids, `,`),
	)

	log.Println(q)
	rows, err := transaction.Query(q)

	if nil != err {
		_ = transaction.Rollback()
		return nil, err
	}

	for rows.Next() {
		var id int64
		var activeDate time.Time
		var name string
		var stat sql.NullInt32
		var isActive bool

		err := rows.Scan(&id, &activeDate, &name, &stat, &isActive)
		if nil != err {
			_ = transaction.Rollback()
			return nil, err
		}

		var statVal *int = nil
		if stat.Valid {
			intVal := int(stat.Int32)
			statVal = &intVal
		}

		data := TestData{
			Id:         id,
			ActiveDate: activeDate,
			Name:       name,
			Stat:       statVal,
			IsActive:   isActive,
		}

		_ = wsDispatcher.DispatchEvent(context.Background(), sqlTestObj, sbuilder.EventTypeCreated, fmt.Sprintf(`%v`, id), data)

		result = append(result, data)
	}

	_ = transaction.Commit()

	return map[string]interface{}{
		"affected_rows": len(result),
		"returning":     result,
	}, nil
}

func insertNewEntity(
	object map[string]interface{},
	transaction *sql.Tx,
	fieldsMap map[string]string,
) (*int, error) {
	fields := []string{}
	values := []interface{}{}

	if 0 == len(object) {
		return nil, fmt.Errorf(`passed empty object to insert`)
	}

	for fieldCode, value := range object {
		if mappedField, ok := fieldsMap[fieldCode]; ok {
			fieldCode = mappedField
		}

		fields = append(fields, fieldCode)
		values = append(values, value)
	}

	parsedValues := []string{}
	for _, val := range values {
		switch v := val.(type) {
		case string:
			parsedValues = append(parsedValues, fmt.Sprintf(`'%v'`, v))
			break
		case time.Time:
			parsedValues = append(parsedValues, fmt.Sprintf(`'%v'`, v.Format(time.RFC3339)))
			break
		default:
			parsedValues = append(parsedValues, fmt.Sprintf(`%v`, v))
		}
	}

	var lastInsertId int
	query := fmt.Sprintf(
		`insert into test_data.test(%v) values (%v) returning id`,
		strings.Join(fields, `,`),
		strings.Join(parsedValues, `,`),
	)

	log.Println(query)
	err := transaction.QueryRow(query).Scan(&lastInsertId)
	if nil != err {
		return nil, err
	}

	return &lastInsertId, nil
}
