package whereParametersLogicOperationsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/whereParametersOperatorGenerator"
	"github.com/graphql-go/graphql"
)

// Процессор генератора оператора "not"
type notWhereParametersLogicOperationsGeneratorProcessor struct {
	generator whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface
	logger    *logrus.Entry
}

// Установка генератора параметров для процессора
func (n *notWhereParametersLogicOperationsGeneratorProcessor) SetWhereParametersOperatorGenerator(interfaces.WhereParametersGeneratorInterface) {
}

// Код операции, для вставки в схему GraphQL
func (n notWhereParametersLogicOperationsGeneratorProcessor) OperationCode() string {
	return constants.NotSchemaKey
}

// Генерация параметров
func (n notWhereParametersLogicOperationsGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	level uint8,
	namePrefix string,
) *graphql.InputObjectFieldConfig {
	if 0 == len(object.Fields()) {
		return nil
	}

	name := `not`
	if 0 != len(namePrefix) {
		name = fmt.Sprintf("%v%v", namePrefix, "_not")
	}

	n.logger.WithFields(logrus.Fields{
		"code":              200,
		"object":            object.Name(),
		"level":             level,
		"input-object-name": name,
	}).Debug(`Generated NOT operator for object.`)

	// Уровень сложности оператора - 0, поэтому нет смысла инкременировать текущий уровень
	fields := n.generator.GenerateOperations(
		ctx,
		object,
		name,
		level,
	)

	if nil == fields {
		return nil
	}

	return &graphql.InputObjectFieldConfig{
		Description: `Оператор NOT для объединения условий`,
		Type: graphql.NewInputObject(graphql.InputObjectConfig{
			Name:        name,
			Fields:      *fields,
			Description: `Объект листинга для оператора NOT для объединения условий`,
		}),
	}
}

// Фабрика процессора
func newNotWhereParametersLogicOperationsGeneratorProcessor(
	generator whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface,
) whereParametersLogicOperationsGeneratorProcessorInterface {
	return &notWhereParametersLogicOperationsGeneratorProcessor{
		generator: generator,
		logger:    helpers.NewLogger(`notWhereParametersLogicOperationsGeneratorProcessor`),
	}
}
