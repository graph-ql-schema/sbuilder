package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Процессор оператора NOT
type notProcessor struct {
	logger *logrus.Entry
	parser WhereOrHavingParserInterface
}

// Установка парсера в процессор
func (n *notProcessor) SetParser(parser WhereOrHavingParserInterface) {
	n.parser = parser
}

// Проверка доступности процессора
func (n *notProcessor) IsAvailable(ctx context.Context, arguments map[string]interface{}) bool {
	_, ok := arguments[constants.NotSchemaKey]

	return ok
}

// Парсинг параметров фильтрации процессором
func (n *notProcessor) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (operation Operation, err error) {
	n.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started filter 'not' parameters parsing")

	data, _ := arguments[constants.NotSchemaKey]
	typedData, ok := data.(map[string]interface{})
	if !ok {
		err = fmt.Errorf(`'NOT' operation fiter data is not in valid format`)
		n.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	parseResult, parseErr := n.parser.Parse(ctx, map[string]interface{}{
		constants.AndSchemaKey: typedData,
	})

	if nil != parseErr {
		return nil, parseErr
	}

	n.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
		"value":     parseResult,
	}).Debug("Parsed filter 'NOT' parameters")

	if nil == parseResult {
		return nil, nil
	}

	return NewNotOperation(parseResult), nil
}

// Фабрика NOT процессора
func newNotProcessor() whereOrHavingParserProcessorInterface {
	return &notProcessor{
		logger: helpers.NewLogger(`notProcessor`),
	}
}
