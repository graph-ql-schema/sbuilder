package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Процессор генерации операторов с ограниченным набором доступных типов
type configurableWhereParametersOperatorGeneratorProcessor struct {
	availableTypes  []graphql.Type
	operatorCode    string
	maxLevel        uint8
	logger          *logrus.Entry
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Проверка доступности процессора
func (c configurableWhereParametersOperatorGeneratorProcessor) IsAvailable(ctx context.Context, field *graphql.FieldDefinition, level uint8) bool {
	if level > c.maxLevel {
		return false
	}

	if field.Resolve != nil {
		return false
	}

	fieldRootType := c.fieldTypeGetter.GetRootType(field.Type)
	for _, fType := range c.availableTypes {
		if fType == fieldRootType {
			return true
		}
	}

	return false
}

// Код оператора, для вставки в схему GraphQL
func (c configurableWhereParametersOperatorGeneratorProcessor) OperatorCode() string {
	return c.operatorCode
}

// Генерация параметров
func (c configurableWhereParametersOperatorGeneratorProcessor) Generate(
	ctx context.Context,
	field *graphql.FieldDefinition,
	level uint8,
	namePrefix string,
) *graphql.InputObjectFieldConfig {
	rType := c.fieldTypeGetter.GetRootType(field.Type)
	c.logger.WithFields(logrus.Fields{
		"code":       100,
		"field":      field.Name,
		"level":      level,
		"namePrefix": namePrefix,
	}).Debug(fmt.Sprintf(`Generated %v operator for field`, c.operatorCode))

	return &graphql.InputObjectFieldConfig{
		Type:        rType,
		Description: fmt.Sprintf(`Операция %v со значением поля`, c.operatorCode),
	}
}

// Фабрика процессора
func newConfigurableWhereParametersOperatorGeneratorProcessor(
	availableTypes []graphql.Type,
	operatorCode string,
	maxLevel uint8,
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface,
) whereParametersOperatorGeneratorProcessorInterface {
	return &configurableWhereParametersOperatorGeneratorProcessor{
		availableTypes:  availableTypes,
		operatorCode:    operatorCode,
		maxLevel:        maxLevel,
		logger:          helpers.NewLogger(fmt.Sprintf(`configurableWhereParametersOperatorGeneratorProcessor(Operation: %v)`, operatorCode)),
		fieldTypeGetter: fieldTypeGetter,
	}
}
