package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aggregateFieldsGenerator/subFieldsResolver"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации поля
func Test_configurableAggregateFieldsGeneratorProcessor_GenerateField(t *testing.T) {
	factory := func(
		fieldCode string,
		fieldType graphql.Type,
	) func(p graphql.ResolveParams) (interface{}, error) {
		return nil
	}

	type fields struct {
		logger                    *logrus.Entry
		fieldTypeGetter           gql_root_type_getter.GraphQlRootTypeGetterInterface
		operatorType              string
		operatorDescription       tDescriptionGenerator
		valueDescriptionGenerator tOperatorValueDescriptionGenerator
		availableTypes            []graphql.Type
		fieldsResolverFactory     subFieldsResolver.TSubFieldsResolverFactory
	}
	type args struct {
		object *graphql.Object
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Field
		want1  string
	}{
		{
			name: "Тестирование на не валидном наборе полей объекта.",
			fields: fields{
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
				operatorType:    "test",
				operatorDescription: func(object *graphql.Object) string {
					return "test desc"
				},
				valueDescriptionGenerator: func(valueCode string, object *graphql.Object) string {
					return "test field desc"
				},
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				fieldsResolverFactory: factory,
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "test",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "Role ID",
							},
						},
						Description: "Test entity",
					},
				),
			},
			want:  nil,
			want1: "",
		},
		{
			name: "Тестирование на валидном наборе полей объекта. Одно из полей валидно. Поле Int. Должно преобразоваться во Float.",
			fields: fields{
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
				operatorType:    "test",
				operatorDescription: func(object *graphql.Object) string {
					return "test desc"
				},
				valueDescriptionGenerator: func(valueCode string, object *graphql.Object) string {
					return "test field desc"
				},
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				fieldsResolverFactory: factory,
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.Int,
								Name: "Role ID",
							},
							"name": &graphql.Field{
								Type: graphql.String,
								Name: "Role name",
							},
						},
						Description: "Test entity",
					},
				),
			},
			want: &graphql.Field{
				Type: graphql.NewNonNull(graphql.NewObject(
					graphql.ObjectConfig{
						Name: `role_aggregate_test_field_response_type`,
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type:        graphql.Float,
								Name:        `role_aggregate_test_field_value_id`,
								Description: `test field desc`,
								Resolve:     nil,
							},
						},
						Description: `test desc`,
					},
				)),
				Name:        `role_aggregate_test_field_type`,
				Description: `test desc`,
			},
			want1: "test",
		},
		{
			name: "Тестирование на валидном наборе полей объекта. Одно из полей валидно. Поле DateTime.",
			fields: fields{
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
				operatorType:    "test",
				operatorDescription: func(object *graphql.Object) string {
					return "test desc"
				},
				valueDescriptionGenerator: func(valueCode string, object *graphql.Object) string {
					return "test field desc"
				},
				availableTypes: []graphql.Type{
					graphql.DateTime,
				},
				fieldsResolverFactory: factory,
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
			},
			want: &graphql.Field{
				Type: graphql.NewNonNull(graphql.NewObject(
					graphql.ObjectConfig{
						Name: `role_aggregate_test_field_response_type`,
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type:        graphql.DateTime,
								Name:        `role_aggregate_test_field_value_date`,
								Description: `test field desc`,
								Resolve:     nil,
							},
						},
						Description: `test desc`,
					},
				)),
				Name:        `role_aggregate_test_field_type`,
				Description: `test desc`,
			},
			want1: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := configurableAggregateFieldsGeneratorProcessor{
				logger:                    tt.fields.logger,
				fieldTypeGetter:           tt.fields.fieldTypeGetter,
				operatorType:              tt.fields.operatorType,
				operatorDescription:       tt.fields.operatorDescription,
				valueDescriptionGenerator: tt.fields.valueDescriptionGenerator,
				availableTypes:            tt.fields.availableTypes,
				fieldsResolverFactory:     tt.fields.fieldsResolverFactory,
			}
			got, got1 := c.GenerateField(context.Background(), tt.args.object)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateField() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GenerateField() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
