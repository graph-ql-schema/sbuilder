package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

var betweenOperationMockFactory TNewBetweenOperation = func(value BetweenValue, field string) Operation {
	return &OperationStub{
		operationType: constants.BetweenSchemaKey,
		value:         value,
		field:         field,
		sql:           fmt.Sprintf(`%v between %v and %v`, field, value.From, value.To),
		sqlErr:        false,
	}
}

// Проверка доступности процессора
func Test_betweenProcessor_isAvailable(t *testing.T) {
	type fields struct {
		logger  *logrus.Entry
		factory TNewBetweenOperation
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: nil,
			},
			want: false,
		},
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": 1,
				},
			},
			want: false,
		},
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						"aaa": map[string]interface{}{
							"_from": 1,
							"_to":   2,
						},
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование на корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.BetweenSchemaKey: map[string]interface{}{
							"_from": 1,
							"_to":   2,
						},
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenProcessor{
				logger:  tt.fields.logger,
				factory: tt.fields.factory,
			}
			if got := b.IsAvailable(context.Background(), tt.args.arguments); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга
func Test_betweenProcessor_parse(t *testing.T) {
	type fields struct {
		logger  *logrus.Entry
		factory TNewBetweenOperation
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantOperation Operation
		wantErr       bool
	}{
		{
			name: "Тестирование на корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.BetweenSchemaKey: map[string]interface{}{
							"_from": 1,
							"_to":   2,
						},
					},
				},
			},
			wantOperation: &OperationStub{
				operationType: constants.BetweenSchemaKey,
				value: BetweenValue{
					From: 1,
					To:   2,
				},
				field:  "test",
				sql:    `test between 1 and 2`,
				sqlErr: false,
			},
			wantErr: false,
		},
		{
			name: "Тестирование с не известным оператором",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						"_test": map[string]interface{}{
							"_from": 1,
							"_to":   2,
						},
					},
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "Тестирование с не корректной структурой внутри оператора",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.BetweenSchemaKey: map[string]interface{}{
							"_from": 1,
							"_test": 2,
						},
					},
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "Тестирование с пустой структурой внутри оператора",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.BetweenSchemaKey: map[string]interface{}{},
					},
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: betweenOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			wantOperation: nil,
			wantErr:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenProcessor{
				logger:  tt.fields.logger,
				factory: tt.fields.factory,
			}
			gotOperation, err := b.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOperation, tt.wantOperation) {
				t.Errorf("parse() gotOperation = %v, want %v", gotOperation, tt.wantOperation)
			}
		})
	}
}
