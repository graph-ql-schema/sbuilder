package paginationParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"
)

// Тестирование парсинга
func Test_paginationParser_Parse(t *testing.T) {
	type fields struct {
		logger *logrus.Entry
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name           string
		fields         fields
		args           args
		wantPagination *Pagination
		wantErr        bool
	}{
		{
			name: "Тестирование с передачей строкового не валидного значения limit",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"limit":  "test",
					"offset": 0,
				},
			},
			wantPagination: nil,
			wantErr:        true,
		},
		{
			name: "Тестирование с передачей числового не валидного значения limit",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"limit":  -100,
					"offset": 0,
				},
			},
			wantPagination: nil,
			wantErr:        true,
		},
		{
			name: "Тестирование с передачей строкового не валидного значения offset",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"limit":  0,
					"offset": "test",
				},
			},
			wantPagination: nil,
			wantErr:        true,
		},
		{
			name: "Тестирование с передачей числового не валидного значения offset",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"limit":  0,
					"offset": -100,
				},
			},
			wantPagination: nil,
			wantErr:        true,
		},
		{
			name: "Тестирование с передачей числового валидных значений",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"limit":  10,
					"offset": 100,
				},
			},
			wantPagination: &Pagination{
				Limit:  10,
				Offset: 100,
			},
			wantErr: false,
		},
		{
			name: "Тестирование без передачи значений",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			wantPagination: nil,
			wantErr:        false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := paginationParser{
				logger: tt.fields.logger,
			}
			gotPagination, err := p.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotPagination, tt.wantPagination) {
				t.Errorf("Parse() gotPagination = %v, want %v", gotPagination, tt.wantPagination)
			}
		})
	}
}
