package for_tests

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования генерации параметров группировки
type GroupByParametersGeneratorMock struct {
	IsCalled bool
	Result   *graphql.ArgumentConfig
}

func (g *GroupByParametersGeneratorMock) Generate(
	context.Context,
	*graphql.Object,
	string,
) *graphql.ArgumentConfig {
	g.IsCalled = true

	return g.Result
}
