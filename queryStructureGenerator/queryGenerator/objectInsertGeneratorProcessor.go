package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	gql_root_type_getter "bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Генерация параметра для вставки значения
type objectInsertGeneratorProcessor struct {
	logger         *logrus.Entry
	typeGetter     gql_root_type_getter.GraphQlRootTypeGetterInterface
	aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
}

// Получение названия генерируемого поля
func (o objectInsertGeneratorProcessor) Name() string {
	return constants.ObjectsSchemaKey
}

// Генерация
func (o objectInsertGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
) *graphql.ArgumentConfig {
	name := fmt.Sprintf(`%v_object`, namePrefix)
	insertObject := o.createInsertObject(object, name)
	if nil == insertObject {
		return nil
	}

	o.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(`Generation object parameter for mutation`)

	return &graphql.ArgumentConfig{
		Type:        graphql.NewList(insertObject),
		Description: "Массив сущностей для вставки",
	}
}

// Генерация объекта для вставки
func (o objectInsertGeneratorProcessor) createInsertObject(object *graphql.Object, namePrefix string) *graphql.InputObject {
	fieldsConfig := o.getFieldsConfigMapByObject(object, namePrefix)
	if 0 == len(fieldsConfig) {
		return nil
	}

	return graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name:        fmt.Sprintf(`%v_type`, namePrefix),
			Fields:      fieldsConfig,
			Description: fmt.Sprintf(`Тип объекта для вставки значения для сущности: '%v'`, object.Name()),
		},
	)
}

// Генерация коллекции значений для вставки
func (o objectInsertGeneratorProcessor) getFieldsConfigMapByObject(object *graphql.Object, namePrefix string) graphql.InputObjectConfigFieldMap {
	configMap := graphql.InputObjectConfigFieldMap{}
	for code, field := range object.Fields() {
		if nil != field.Resolve {
			continue
		}

		fieldRootType := o.typeGetter.GetRootType(field.Type)
		if _, ok := fieldRootType.(*graphql.Object); ok {
			continue
		}

		configMap[code] = &graphql.InputObjectFieldConfig{
			Type: field.Type,
			Description: fmt.Sprintf(
				`Значение для вставки для сущности '%v' поля '%v'`,
				object.Name(),
				o.aliasGenerator.Generate(field.Name),
			),
		}
	}

	return configMap
}

// Фабрика процессора
func newObjectInsertGeneratorProcessor() queryGeneratorProcessorInterface {
	return &objectInsertGeneratorProcessor{
		logger:         helpers.NewLogger(`objectInsertGeneratorProcessor`),
		typeGetter:     gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
	}
}
