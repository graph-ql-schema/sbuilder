package orderParametersGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	for_tests "bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование генератора сортировки
func Test_orderParametersGenerator_Generate(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		logger     *logrus.Entry
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.ArgumentConfig
	}{
		{
			name: "Тестирование генератора сортировки",
			fields: fields{
				logger:     helpers.NewLogger(`test`),
				typeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "role",
			},
			want: &graphql.ArgumentConfig{
				Type: graphql.NewList(graphql.NewInputObject(graphql.InputObjectConfig{
					Name: `role_order_parameters_object`,
					Fields: graphql.InputObjectConfigFieldMap{
						"by": &graphql.InputObjectFieldConfig{
							Type: graphql.NewEnum(graphql.EnumConfig{
								Name: `role_order_parameters_object_by`,
								Values: graphql.EnumValueConfigMap{
									"field_1": &graphql.EnumValueConfig{
										Value:       "field_1",
										Description: "Значение field_1",
									},
								},
								Description: "Варианты выбора полей для сортировки",
							}),
							Description: "Варианты выбора полей для сортировки",
						},
						"direction": &graphql.InputObjectFieldConfig{
							Type: graphql.NewEnum(graphql.EnumConfig{
								Name: `role_order_parameters_object_order`,
								Values: graphql.EnumValueConfigMap{
									"asc": &graphql.EnumValueConfig{
										Value:       "asc",
										Description: "Сортировка по возрастанию",
									},
									"desc": &graphql.EnumValueConfig{
										Value:       "desc",
										Description: "Сортировка по убыванию",
									},
								},
								Description: "Варианты сортировки",
							}),
							DefaultValue: "asc",
							Description:  "Варианты сортировки",
						},
						"priority": &graphql.InputObjectFieldConfig{
							Type:         graphql.Int,
							DefaultValue: 500,
							Description:  "Приоритет применения сортировки. Должен быть больше 0.",
						},
					},
					Description: "Сортировка по переданным параметрам",
				})),
				Description: "Сортировка по переданным параметрам",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := orderParametersGenerator{
				logger:     tt.fields.logger,
				typeGetter: tt.fields.typeGetter,
			}
			if got := o.Generate(context.Background(), tt.args.object, tt.args.namePrefix); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
