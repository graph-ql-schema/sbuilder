package whereParametersGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/whereParametersLogicOperationsGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/whereParametersOperatorGenerator"
	"github.com/graphql-go/graphql"
)

var objectConfig = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "role",
		Fields: graphql.Fields{
			"field_1": &graphql.Field{
				Type: graphql.Int,
				Name: "field_1",
			},
		},
		Description: "Role entity",
	},
)

// Тестирвоание генерации параметров
func Test_whereParametersGenerator_BuildParameters(t *testing.T) {
	type fields struct {
		logicOperationsGenerator whereParametersLogicOperationsGenerator.WhereParametersLogicOperationsGeneratorInterface
		operatorGenerator        whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface
		logger                   *logrus.Entry
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
		level      uint8
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.InputObjectConfigFieldMap
	}{
		{
			name: "Тестирование с параметрами, когда оба подсервиса возвращают значение.",
			fields: fields{
				logicOperationsGenerator: &for_tests.WhereParametersLogicOperationsGeneratorMock{
					IsCalled: false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{
						"test_1": {
							Type:        graphql.Int,
							Description: "Test 1",
						},
					},
				},
				operatorGenerator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled: false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{
						"test_2": {
							Type:        graphql.Int,
							Description: "Test 2",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object:     objectConfig,
				namePrefix: "test",
				level:      1,
			},
			want: &graphql.InputObjectConfigFieldMap{
				"test_1": {
					Type:        graphql.Int,
					Description: "Test 1",
				},
				"test_2": {
					Type:        graphql.Int,
					Description: "Test 2",
				},
			},
		},
		{
			name: "Тестирование с параметрами, когда один подсервис возвращают значение. Вариант #1",
			fields: fields{
				logicOperationsGenerator: &for_tests.WhereParametersLogicOperationsGeneratorMock{
					IsCalled:     false,
					ReturnResult: nil,
				},
				operatorGenerator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled: false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{
						"test_2": {
							Type:        graphql.Int,
							Description: "Test 2",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object:     objectConfig,
				namePrefix: "test",
				level:      1,
			},
			want: &graphql.InputObjectConfigFieldMap{
				"test_2": {
					Type:        graphql.Int,
					Description: "Test 2",
				},
			},
		},
		{
			name: "Тестирование с параметрами, когда один подсервис возвращают значение. Вариант #2",
			fields: fields{
				logicOperationsGenerator: &for_tests.WhereParametersLogicOperationsGeneratorMock{
					IsCalled: false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{
						"test_1": {
							Type:        graphql.Int,
							Description: "Test 1",
						},
					},
				},
				operatorGenerator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled:     false,
					ReturnResult: nil,
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object:     objectConfig,
				namePrefix: "test",
				level:      1,
			},
			want: &graphql.InputObjectConfigFieldMap{
				"test_1": {
					Type:        graphql.Int,
					Description: "Test 1",
				},
			},
		},
		{
			name: "Тестирование с параметрами, когда ни один подсервис не возвращают значение.",
			fields: fields{
				logicOperationsGenerator: &for_tests.WhereParametersLogicOperationsGeneratorMock{
					IsCalled:     false,
					ReturnResult: nil,
				},
				operatorGenerator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled:     false,
					ReturnResult: nil,
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object:     objectConfig,
				namePrefix: "test",
				level:      1,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := whereParametersGenerator{
				logicOperationsGenerator: tt.fields.logicOperationsGenerator,
				operatorGenerator:        tt.fields.operatorGenerator,
				logger:                   tt.fields.logger,
			}
			if got := w.BuildParameters(context.Background(), tt.args.object, tt.args.namePrefix, tt.args.level); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BuildParameters() = %v, want %v", got, tt.want)
			}
		})
	}
}
