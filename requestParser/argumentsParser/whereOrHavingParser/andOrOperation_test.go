package whereOrHavingParser

import (
	"reflect"
	"testing"

	"github.com/graphql-go/graphql"
)

// Тестирование получения типа операции
func Test_andOrOperation_Type(t *testing.T) {
	type fields struct {
		operationType string
		operations    []Operation
		field         string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения типа операции",
			fields: fields{
				operationType: "test",
				operations:    nil,
				field:         "",
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := andOrOperation{
				operationType: tt.fields.operationType,
				operations:    tt.fields.operations,
				field:         tt.fields.field,
			}
			if got := a.Type(); got != tt.want {
				t.Errorf("Type() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения значения операции
func Test_andOrOperation_Value(t *testing.T) {
	type fields struct {
		operationType string
		operations    []Operation
		field         string
	}
	tests := []struct {
		name   string
		fields fields
		want   interface{}
	}{
		{
			name: "Тестирование получения пустого значения",
			fields: fields{
				operationType: "test",
				operations:    []Operation{},
				field:         "test",
			},
			want: []Operation{},
		},
		{
			name: "Тестирование получения не пустого значения",
			fields: fields{
				operationType: "test",
				operations:    []Operation{&andOrOperation{}},
				field:         "test",
			},
			want: []Operation{&andOrOperation{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := andOrOperation{
				operationType: tt.fields.operationType,
				operations:    tt.fields.operations,
				field:         tt.fields.field,
			}
			if got := a.Value(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения кода поля
func Test_andOrOperation_Field(t *testing.T) {
	type fields struct {
		operationType string
		operations    []Operation
		field         string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения кода поля",
			fields: fields{
				field: "test",
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := andOrOperation{
				operationType: tt.fields.operationType,
				operations:    tt.fields.operations,
				field:         tt.fields.field,
			}
			if got := a.Field(); got != tt.want {
				t.Errorf("Field() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации в SQL
func Test_andOrOperation_ToSQL(t *testing.T) {
	type fields struct {
		operationType string
		operations    []Operation
		field         string
	}
	type args struct {
		object    *graphql.Object
		fieldsMap map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование без подопераций",
			fields: fields{
				operationType: "and",
				operations:    []Operation{},
				field:         "where",
			},
			args:    args{},
			want:    "",
			wantErr: false,
		},
		{
			name: "Тестирование c подоперацией",
			fields: fields{
				operationType: "and",
				operations: []Operation{
					&OperationStub{
						operationType: "test",
						value:         nil,
						field:         "test",
						sql:           "test = 1",
						sqlErr:        false,
					},
				},
				field: "where",
			},
			args:    args{},
			want:    "test = 1",
			wantErr: false,
		},
		{
			name: "Тестирование c подоперацией, возвращающей ошибку",
			fields: fields{
				operationType: "and",
				operations: []Operation{
					&OperationStub{
						operationType: "test",
						value:         nil,
						field:         "test",
						sql:           "",
						sqlErr:        true,
					},
				},
				field: "where",
			},
			args:    args{},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование c подоперациями",
			fields: fields{
				operationType: "or",
				operations: []Operation{
					&OperationStub{
						operationType: "test",
						value:         nil,
						field:         "test",
						sql:           "test = 1",
						sqlErr:        false,
					},
					&OperationStub{
						operationType: "data",
						value:         nil,
						field:         "data",
						sql:           "data = 1",
						sqlErr:        false,
					},
				},
				field: "where",
			},
			args:    args{},
			want:    "(test = 1 or data = 1)",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := andOrOperation{
				operationType: tt.fields.operationType,
				operations:    tt.fields.operations,
				field:         tt.fields.field,
			}

			got, err := a.ToSQL(tt.args.object, tt.args.fieldsMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if got != tt.want {
				t.Errorf("ToSQL() = %v, want %v", got, tt.want)
			}
		})
	}
}
