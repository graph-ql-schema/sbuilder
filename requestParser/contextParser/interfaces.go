package contextParser

import (
	"context"

	"github.com/valyala/fasthttp"
)

// Парсер запроса для формирования контекста
type ContextParserInterface interface {
	// Парсинг запроса для выделения контекста
	Parse(ctx *fasthttp.RequestCtx) context.Context
}
