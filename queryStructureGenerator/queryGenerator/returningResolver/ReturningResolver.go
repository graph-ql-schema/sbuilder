package returningResolver

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"encoding/json"
	"fmt"
	"reflect"

	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/valueConverter"
	"github.com/graphql-go/graphql"
)

// Резолвер поля returning для обновления/создания сущности
type returningResolver struct {
	object         *graphql.Object
	valueConverter gql_sql_converter.GraphQlSqlConverterInterface
}

// Резолвинг поля returning для обновления/создания сущности
func (r returningResolver) Resolve(p graphql.ResolveParams) (interface{}, error) {
	if p.Source == nil {
		return nil, nil
	}

	data, err := json.Marshal(p.Source)
	if nil != err {
		return nil, nil
	}

	var resultMap map[string]interface{}
	err = json.Unmarshal(data, &resultMap)

	if nil != err {
		return nil, nil
	}

	value := reflect.ValueOf(resultMap)
	for _, mapKey := range value.MapKeys() {
		key := fmt.Sprintf("%s", mapKey.Interface())
		if key != "returning" {
			continue
		}

		returning := []map[string]interface{}{}
		unprocessedReturning := value.MapIndex(mapKey).Interface()
		if reflect.TypeOf(unprocessedReturning).Kind() != reflect.Slice {
			return unprocessedReturning, nil
		}

		s := reflect.ValueOf(unprocessedReturning)
		for i := 0; i < s.Len(); i++ {
			item := s.Index(i).Interface()
			if reflect.TypeOf(item).Kind() != reflect.Map {
				return nil, fmt.Errorf(`one of returning items has invalid format`)
			}

			processedItem := map[string]interface{}{}
			value := reflect.ValueOf(item)
			for _, mapKey := range value.MapKeys() {
				itemVal, err := r.parseValue(value.MapIndex(mapKey).Interface(), mapKey.String())
				if nil != err {
					return nil, fmt.Errorf(`failed to process returning item value for field '%v'. value: '%v'`, mapKey.String(), value.MapIndex(mapKey).Interface())
				}

				processedItem[mapKey.String()] = itemVal
			}

			returning = append(returning, processedItem)
		}

		return returning, nil
	}

	return nil, nil
}

// Конвертация значения в валидный для GraphQL тип
func (r returningResolver) parseValue(value interface{}, field string) (interface{}, error) {
	if nil == value {
		return nil, nil
	}

	if reflect.TypeOf(value).Kind() != reflect.Slice {
		return r.valueConverter.ToBaseType(r.object, field, value)
	}

	items := []interface{}{}
	s := reflect.ValueOf(value)
	for i := 0; i < s.Len(); i++ {
		item := s.Index(i).Interface()
		converted, err := r.valueConverter.ToBaseType(r.object, field, item)
		if nil != err {
			return nil, err
		}

		items = append(items, converted)
	}

	return items, nil
}

// Фабрика сервиса резолвера
func newReturningResolver(object *graphql.Object) ReturningResolverInterface {
	return &returningResolver{
		object:         object,
		valueConverter: valueConverter.NewValueConverter(),
	}
}

// Фабрика сервиса резолвера с кастомизаторами
func newReturningResolverWithCustomizations(
	object *graphql.Object,
	customizations []customizationService.CustomizationInterface,
) ReturningResolverInterface {
	return &returningResolver{
		object:         object,
		valueConverter: valueConverter.NewValueConverterWithCustomizations(customizations),
	}
}

// Фабрика резолвера
func ReturningResolverFactory(object *graphql.Object) func(p graphql.ResolveParams) (interface{}, error) {
	return newReturningResolver(object).Resolve
}

// Фабрика резолвера с кастомизаторами
func ReturningResolverFactoryWithCustomizations(
	object *graphql.Object,
	customizations []customizationService.CustomizationInterface,
) func(p graphql.ResolveParams) (interface{}, error) {
	return newReturningResolverWithCustomizations(object, customizations).Resolve
}
