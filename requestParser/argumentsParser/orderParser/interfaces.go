package orderParser

import (
	"context"
)

// Направления сортировки
type OrderDirection string

// Результат парсинга параметров сортировки
type Order struct {
	Priority  int            `json:"priority"`
	By        string         `json:"by"`
	Direction OrderDirection `json:"order"`
}

// Парсер параметров сортировки
type OrderParserInterface interface {
	// Парсинг параметров сортировки
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (order []Order, err error)
}
