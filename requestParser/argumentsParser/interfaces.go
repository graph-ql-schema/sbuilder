package argumentsParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/groupByParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/orderParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/paginationParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/setParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"context"
)

// Результирующая структура парсинга параметров запроса
type ParsedArguments struct {
	Objects    objectsParser.Objects
	Set        setParser.Set
	GroupBy    groupByParser.GroupBy
	OrderBy    []orderParser.Order
	Pagination *paginationParser.Pagination
	Where      whereOrHavingParser.Operation
	Having     whereOrHavingParser.Operation
}

// Парсер аргументов запроса
type ArgumentsParserInterface interface {
	// Парсинг аргументов запроса
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (*ParsedArguments, error)
}
