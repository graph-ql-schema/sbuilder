package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aggregateFieldsGenerator/subFieldsResolver"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации поля
func Test_variantsAggregateFieldsGeneratorProcessor_GenerateField(t *testing.T) {
	var object = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"id": &graphql.Field{
					Type: graphql.ID,
					Name: "Role ID",
				},
				"name": &graphql.Field{
					Type: graphql.String,
					Name: "Role name",
				},
			},
			Description: "Role entity",
		},
	)

	factory := func(
		fieldCode string,
		fieldType graphql.Type,
	) func(p graphql.ResolveParams) (interface{}, error) {
		return nil
	}

	type fields struct {
		logger                *logrus.Entry
		fieldTypeGetter       gql_root_type_getter.GraphQlRootTypeGetterInterface
		fieldsResolverFactory subFieldsResolver.TSubFieldsResolverFactory
	}
	type args struct {
		object *graphql.Object
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Field
		want1  string
	}{
		{
			name: "Тестирование генерации на объекте без подходящих полей.",
			fields: fields{
				logger:                helpers.NewLogger(`test`),
				fieldTypeGetter:       &for_tests.GraphQlRootTypeGetterMock{},
				fieldsResolverFactory: factory,
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name:        "test",
						Fields:      graphql.Fields{},
						Description: "Test entity",
					},
				),
			},
			want:  nil,
			want1: "",
		},
		{
			name: "Тестирование генерации на объекте c подходящими полями.",
			fields: fields{
				logger:                helpers.NewLogger(`test`),
				fieldTypeGetter:       &for_tests.GraphQlRootTypeGetterMock{},
				fieldsResolverFactory: factory,
			},
			args: args{
				object: object,
			},
			want: &graphql.Field{
				Type: graphql.NewNonNull(graphql.NewObject(
					graphql.ObjectConfig{
						Name: `role_aggregate_variants_field_response_type`,
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type:        graphql.NewList(graphql.ID),
								Name:        `role_aggregate_variants_field_value_id`,
								Description: `Получение списка уникальных значений для поля id`,
								Resolve:     nil,
							},
							"name": &graphql.Field{
								Type:        graphql.NewList(graphql.String),
								Name:        `role_aggregate_variants_field_value_name`,
								Description: `Получение списка уникальных значений для поля name`,
								Resolve:     nil,
							},
						},
						Description: `Доступный для выбора список полей для получения уникальных значений по ним для сущности 'role'`,
					},
				)),
				Name:        `role_aggregate_variants_field_type`,
				Description: `Получение списка уникальных значений для полей сущности 'role'`,
			},
			want1: constants.VariantsOperationSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variantsAggregateFieldsGeneratorProcessor{
				logger:                tt.fields.logger,
				fieldTypeGetter:       tt.fields.fieldTypeGetter,
				fieldsResolverFactory: tt.fields.fieldsResolverFactory,
			}
			got, got1 := v.GenerateField(context.Background(), tt.args.object)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateField() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GenerateField() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
