package whereOrHavingParser

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
)

// Операция отрицания
type notOperation struct {
	value Operation
}

// Получение типа текущей операции
func (n notOperation) Type() string {
	return constants.NotSchemaKey
}

// Получение значения для текущей операции
func (n notOperation) Value() interface{} {
	return n.value
}

// Получение кода поля для операции
func (n notOperation) Field() string {
	return constants.NotSchemaKey
}

// Конвертация в SQL
func (n notOperation) ToSQL(object *graphql.Object, fieldsMap map[string]string) (string, error) {
	subOperationRes, err := n.value.ToSQL(object, fieldsMap)
	if nil != err {
		return "", fmt.Errorf(`'NOT' error: %v`, err.Error())
	}

	return fmt.Sprintf(`not (%v)`, subOperationRes), nil
}

// Фабрика операции
func NewNotOperation(value Operation) Operation {
	return &notOperation{
		value: value,
	}
}
