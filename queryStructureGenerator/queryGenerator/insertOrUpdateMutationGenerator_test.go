package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/queryGenerator/returningResolver"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации мутации на вставку/изменение значений
func Test_insertOrUpdateMutationGenerator_Generate(t *testing.T) {
	resolver := func(object *graphql.Object) func(p graphql.ResolveParams) (interface{}, error) {
		return nil
	}

	object := graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		logger               *logrus.Entry
		typeGetter           gql_root_type_getter.GraphQlRootTypeGetterInterface
		processors           []queryGeneratorProcessorInterface
		aliasGenerator       aliasGenerationService.AliasGenerationServiceInterface
		operationType        string
		descriptionGenerator tDescriptionGenerator
		suffix               constants.QueryType
		resolverFactory      returningResolver.TReturningResolverFactory
	}
	type args struct {
		object   *graphql.Object
		resolver TResolver
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Field
	}{
		{
			name: "Тестирование генерации мутации без валидных процессоров.",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				typeGetter:     &for_tests.GraphQlRootTypeGetterMock{},
				processors:     []queryGeneratorProcessorInterface{},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
				operationType:  "insert",
				descriptionGenerator: func(object *graphql.Object) string {
					return "test"
				},
				suffix:          "test",
				resolverFactory: resolver,
			},
			args: args{
				object:   object,
				resolver: nil,
			},
			want: nil,
		},
		{
			name: "Тестирование генерации мутации с валидным процессором.",
			fields: fields{
				logger:     helpers.NewLogger(`test`),
				typeGetter: &for_tests.GraphQlRootTypeGetterMock{},
				processors: []queryGeneratorProcessorInterface{
					&queryGeneratorProcessorMock{
						name:         "test",
						isCalled:     false,
						returnResult: &graphql.ArgumentConfig{},
					},
				},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
				operationType:  "insert",
				descriptionGenerator: func(object *graphql.Object) string {
					return "test"
				},
				suffix:          "test",
				resolverFactory: resolver,
			},
			args: args{
				object:   object,
				resolver: nil,
			},
			want: &graphql.Field{
				Type: graphql.NewObject(
					graphql.ObjectConfig{
						Name: `Role_insert_result_type`,
						Fields: graphql.Fields{
							"affected_rows": &graphql.Field{
								Type:        graphql.NewNonNull(graphql.Int),
								Name:        `Role_insert_affected`,
								Description: "Количество записей, обработанных запросом",
							},
							"returning": &graphql.Field{
								Type:        graphql.NewList(object),
								Name:        `Role_insert_rows`,
								Description: "Массив объектов, обработанных в ходе выполнения запроса",
								Resolve:     nil,
							},
						},
						Description: `Результат обработки значений для сущности 'Role'`,
					},
				),
				Name:        `Role_insert_mutation`,
				Description: `test`,
				Args: graphql.FieldConfigArgument{
					"test": &graphql.ArgumentConfig{},
				},
				Resolve: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := insertOrUpdateMutationGenerator{
				logger:               tt.fields.logger,
				typeGetter:           tt.fields.typeGetter,
				processors:           tt.fields.processors,
				aliasGenerator:       tt.fields.aliasGenerator,
				operationType:        tt.fields.operationType,
				descriptionGenerator: tt.fields.descriptionGenerator,
				suffix:               tt.fields.suffix,
				resolverFactory:      tt.fields.resolverFactory,
			}
			if got := i.Generate(context.Background(), tt.args.object, tt.args.resolver); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения суффикса
func Test_insertOrUpdateMutationGenerator_Suffix(t *testing.T) {
	resolver := func(object *graphql.Object) func(p graphql.ResolveParams) (interface{}, error) {
		return nil
	}

	type fields struct {
		logger               *logrus.Entry
		typeGetter           gql_root_type_getter.GraphQlRootTypeGetterInterface
		processors           []queryGeneratorProcessorInterface
		aliasGenerator       aliasGenerationService.AliasGenerationServiceInterface
		operationType        string
		descriptionGenerator tDescriptionGenerator
		suffix               constants.QueryType
		resolverFactory      returningResolver.TReturningResolverFactory
	}
	tests := []struct {
		name   string
		fields fields
		want   constants.QueryType
	}{
		{
			name: "Тестирование генерации мутации с валидным процессором.",
			fields: fields{
				logger:     helpers.NewLogger(`test`),
				typeGetter: &for_tests.GraphQlRootTypeGetterMock{},
				processors: []queryGeneratorProcessorInterface{
					&queryGeneratorProcessorMock{
						name:         "test",
						isCalled:     false,
						returnResult: &graphql.ArgumentConfig{},
					},
				},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
				operationType:  "insert",
				descriptionGenerator: func(object *graphql.Object) string {
					return "test"
				},
				suffix:          "test",
				resolverFactory: resolver,
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := insertOrUpdateMutationGenerator{
				logger:               tt.fields.logger,
				typeGetter:           tt.fields.typeGetter,
				processors:           tt.fields.processors,
				aliasGenerator:       tt.fields.aliasGenerator,
				operationType:        tt.fields.operationType,
				descriptionGenerator: tt.fields.descriptionGenerator,
				suffix:               tt.fields.suffix,
				resolverFactory:      tt.fields.resolverFactory,
			}
			if got := i.Suffix(); got != tt.want {
				t.Errorf("Suffix() = %v, want %v", got, tt.want)
			}
		})
	}
}
