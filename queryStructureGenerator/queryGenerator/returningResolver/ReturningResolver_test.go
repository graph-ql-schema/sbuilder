package returningResolver

import (
	"reflect"
	"testing"

	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	for_tests "bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование резолвинга сущности
func Test_returningResolver_Resolve(t *testing.T) {
	type Entity struct {
		Test string
	}

	type EntityWithSlice struct {
		Test []string
	}

	type fields struct {
		object         *graphql.Object
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	type args struct {
		p graphql.ResolveParams
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование на пустой структуре",
			fields: fields{
				object: nil,
				valueConverter: for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
				},
			},
			args: args{
				p: graphql.ResolveParams{
					Source: "returning",
				},
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "Тестирование на не пустой структуре",
			fields: fields{
				object: nil,
				valueConverter: for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
				},
			},
			args: args{
				p: graphql.ResolveParams{
					Source: map[string]interface{}{
						"returning": "test",
					},
				},
			},
			want:    "test",
			wantErr: false,
		},
		{
			name: "Тестирование на объекте с ошибкой",
			fields: fields{
				object: nil,
				valueConverter: for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: true,
					ToBaseTypeRes: "test-1",
				},
			},
			args: args{
				p: graphql.ResolveParams{
					Source: struct {
						Returning []Entity `json:"returning"`
					}{
						[]Entity{
							{Test: "test-1"},
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на объекте",
			fields: fields{
				object: nil,
				valueConverter: for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: "test-1",
				},
			},
			args: args{
				p: graphql.ResolveParams{
					Source: struct {
						Returning []Entity `json:"returning"`
					}{
						[]Entity{
							{Test: "test-1"},
						},
					},
				},
			},
			want: []map[string]interface{}{
				{"Test": "test-1"},
			},
			wantErr: false,
		},
		{
			name: "Тестирование на объекте со слайсом",
			fields: fields{
				object: nil,
				valueConverter: for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: "test-1",
				},
			},
			args: args{
				p: graphql.ResolveParams{
					Source: struct {
						Returning []EntityWithSlice `json:"returning"`
					}{
						[]EntityWithSlice{
							{Test: []string{"test-1", "test-2"}},
						},
					},
				},
			},
			want: []map[string]interface{}{
				{"Test": []interface{}{
					"test-1",
					"test-1",
				}},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := returningResolver{
				object:         tt.fields.object,
				valueConverter: tt.fields.valueConverter,
			}
			got, err := r.Resolve(tt.args.p)
			if (err != nil) != tt.wantErr {
				t.Errorf("Resolve() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Resolve() got = %v, want %v", got, tt.want)
			}
		})
	}
}
