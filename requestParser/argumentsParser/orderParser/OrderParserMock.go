package orderParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования
type OrderParserMock struct {
	Result []Order
	IsErr  bool
}

// Парсинг параметров сортировки
func (o OrderParserMock) Parse(
	context.Context,
	map[string]interface{},
) (order []Order, err error) {
	if o.IsErr {
		return nil, fmt.Errorf(`test`)
	}

	return o.Result, nil
}
