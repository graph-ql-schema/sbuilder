package sbuilder

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/queryGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/resolverGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/wsResolveGenerator"
	"context"
	"fmt"
	"github.com/functionalfoundry/graphqlws"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
)

// Генератор схемы запросов к GraphQL
type graphQlSchemaBuilder struct {
	logger              *logrus.Entry
	aliasGenerator      aliasGenerationService.AliasGenerationServiceInterface
	serverFactory       tServerFactory
	wsServerFactory     tWsServerFactory
	wsDispatcherFactory tWsDispatcherFactory
	resolverFactory     resolverGenerator.ResolverGeneratorInterface
	wsResolveGenerator  wsResolveGenerator.WsResolveGeneratorInterface
	schemaGenerators    []queryGenerator.QueryGeneratorInterface
	sManager            graphqlws.SubscriptionManager

	queries      graphql.Fields
	mutations    graphql.Fields
	subscription graphql.Fields
	serverConfig GraphQlServerConfig

	schema *graphql.Schema

	customizations []customizationService.CustomizationInterface
}

// Регистрация сущности для обработки сервером.
// Метод генерирует валидное GraphQL API для данной сущности при помощи переданных параметров
// запросов (queries), где в качестве ключа передается тип запроса для генерации, а в качестве
// значения - резолвер сущности.
func (g *graphQlSchemaBuilder) RegisterEntity(
	object *graphql.Object,
	queries EntityQueries,
) {
	queriesKeys := make([]constants.QueryType, len(queries))
	queriesKeysIndex := 0
	for query := range queries {
		queriesKeys[queriesKeysIndex] = query
		queriesKeysIndex++
	}

	for gType, resolver := range queries {
		genRes := g.resolverFactory.Generate(context.TODO(), object, resolver)
		for _, generator := range g.schemaGenerators {
			if gType == generator.Suffix() {
				key := fmt.Sprintf(`%v%v`, g.aliasGenerator.Generate(object.Name()), generator.Suffix())
				field := generator.Generate(context.TODO(), object, genRes)
				if field == nil {
					continue
				}

				if gType == constants.ListQuery || gType == constants.AggregateQuery {
					g.queries[key] = field
				} else {
					g.mutations[key] = field
				}
			}
		}
	}

	g.logger.WithFields(logrus.Fields{
		"code":    200,
		"object":  object.Name(),
		"queries": queriesKeys,
	}).Info("Generate API")
}

// Регистрация сущности как метод подписки на события изменения сущности
// Метод генерирует валидную подписку для отслеживания событий изменения данных
// сущности. По сути позволяет в дальнейшем получать все виды нотификаций об
// изменениях для переданной сущности.
func (g *graphQlSchemaBuilder) RegisterEntityChangeSubscription(
	object *graphql.Object,
	middleware wsResolveGenerator.TSubscriptionMiddleware,
) {
	// Валидный результат работы резолвера подписчика
	type ResolverResponse struct {
		EventType  types.TEventType `json:"eventType"`
		EntityId   string           `json:"entityId"`
		EntityData interface{}      `json:"data"`
	}

	subscriptionName := fmt.Sprintf(`%vChanges`, object.Name())
	g.subscription[subscriptionName] = &graphql.Field{
		Name: subscriptionName,
		Type: graphql.NewObject(graphql.ObjectConfig{
			Name: fmt.Sprintf(`%v_response`, subscriptionName),
			Fields: graphql.Fields{
				"eventType": &graphql.Field{
					Name: "eventType",
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name: fmt.Sprintf("%v_response_eventType_enum", subscriptionName),
						Values: graphql.EnumValueConfigMap{
							EventTypeCreated: {
								Value:       EventTypeCreated,
								Description: "Событие создания сущности",
							},
							EventTypeUpdated: {
								Value:       EventTypeUpdated,
								Description: "Событие обновления сущности",
							},
							EventTypeDeleted: {
								Value:       EventTypeDeleted,
								Description: "Событие удаления сущности",
							},
						},
						Description: "Типы возникшего события",
					}),
					Description: "Тип возникшего события",
				},
				"entityId": {
					Name:        "entityId",
					Type:        graphql.ID,
					Description: "ID сущности, с которой произошло событие",
				},
				"data": {
					Name:        "data",
					Type:        object,
					Description: "Данные сущности, после события. Null для событий удаления",
				},
			},
			Description: fmt.Sprintf(`Событие изменения сущности "%v"`, object.Name()),
		}),
		Args: graphql.FieldConfigArgument{
			"eventType": &graphql.ArgumentConfig{
				Type: graphql.NewList(graphql.NewEnum(graphql.EnumConfig{
					Name: fmt.Sprintf("%v_argument_eventType_enum", subscriptionName),
					Values: graphql.EnumValueConfigMap{
						EventTypeCreated: {
							Value:       EventTypeCreated,
							Description: "Событие создания сущности",
						},
						EventTypeUpdated: {
							Value:       EventTypeUpdated,
							Description: "Событие обновления сущности",
						},
						EventTypeDeleted: {
							Value:       EventTypeDeleted,
							Description: "Событие удаления сущности",
						},
					},
					Description: "Типы возникшего события",
				})),
				DefaultValue: []interface{}{EventTypeCreated, EventTypeUpdated, EventTypeDeleted},
				Description:  "Типы возникшего события, которые необходимо слушать",
			},
			"entityId": &graphql.ArgumentConfig{
				Type:        graphql.NewList(graphql.ID),
				Description: "ID сущностей, события которых необходимо прослушивать",
			},
		},
		Resolve:     g.wsResolveGenerator.MakeResolver(object, middleware),
		Description: fmt.Sprintf(`Подписка чтения событий изменения сущности "%v"`, object.Name()),
	}

	g.logger.WithFields(logrus.Fields{
		"code":   200,
		"object": object.Name(),
	}).Info("Generate WS subscriber")
}

// Регистрация базового запроса без какой либо обработки со стороны сервиса
// Метод предназначен для регистрации кастома GraphQL API, не входящего в библиотеку
func (g *graphQlSchemaBuilder) RegisterQuery(
	key string,
	field *graphql.Field,
) {
	g.queries[key] = field
}

// Регистрация базовой мутации без какой либо обработки со стороны сервиса
// Метод предназначен для регистрации кастома GraphQL API, не входящего в библиотеку
func (g *graphQlSchemaBuilder) RegisterMutation(
	key string,
	field *graphql.Field,
) {
	g.mutations[key] = field
}

// Регистрация базовой подписки без какой либо обработки со стороны сервиса
// Метод предназначен для регистрации кастома GraphQL API, не входящего в библиотеку
func (g *graphQlSchemaBuilder) RegisterSubscription(key string, field *graphql.Field) {
	g.subscription[key] = field
}

// Установка кастомных параметров конфигурации сервера
// По умолчанию используются:
// 	- Host: 0.0.0.0
//  - Port: 8080
//  - Uri: /graphql
func (g *graphQlSchemaBuilder) SetServerConfig(config GraphQlServerConfig) {
	if 0 == len(config.Host) {
		config.Host = "0.0.0.0"
	}

	if config.Port < 1 {
		config.Port = 8080
	}

	if 0 == len(config.Uri) {
		config.Uri = `/graphql`
	}

	if config.WsPort < 1 {
		config.Port = 9000
	}

	if 0 == len(config.WsUri) {
		config.WsUri = `/subscriptions`
	}

	g.serverConfig = config
}

// Генерация схемы GraphQL
func (g *graphQlSchemaBuilder) buildSchema() (*graphql.Schema, error) {
	if nil != g.schema {
		return g.schema, nil
	}

	schemaConfig := graphql.SchemaConfig{}

	if 0 != len(g.queries) {
		schemaConfig.Query = graphql.NewObject(
			graphql.ObjectConfig{
				Name:        "Query",
				Fields:      g.queries,
				Description: "Список допустимых запросов к GraphQL API",
			},
		)
	}

	if 0 != len(g.mutations) {
		schemaConfig.Mutation = graphql.NewObject(
			graphql.ObjectConfig{
				Name:        "Mutation",
				Fields:      g.mutations,
				Description: "Список допустимых мутаций для GraphQL API",
			},
		)
	}

	if 0 != len(g.subscription) {
		schemaConfig.Subscription = graphql.NewObject(
			graphql.ObjectConfig{
				Name:        "Subscription",
				Fields:      g.subscription,
				Description: "Список допустимых подписок для GraphQL API",
			},
		)
	}

	scheme, err := graphql.NewSchema(schemaConfig)
	if nil != err {
		return nil, err
	}

	g.schema = &scheme
	g.sManager = graphqlws.NewSubscriptionManager(g.schema)

	return g.schema, nil
}

// Создание экземпляра сервера для обработки GraphQL запросов.
func (g *graphQlSchemaBuilder) BuildServer(middleware ...Middleware) (GraphQlServerInterface, error) {
	schema, err := g.buildSchema()
	if nil != err {
		return nil, err
	}

	return g.serverFactory(
		g.serverConfig,
		*schema,
		middleware...,
	), nil
}

// Создание экземпляра сервера для обработки WS запросов к GraphQL
func (g *graphQlSchemaBuilder) BuildWsServer(authCallback tAuthCallback) (GraphQlServerInterface, error) {
	_, err := g.buildSchema()
	if nil != err {
		return nil, err
	}

	return g.wsServerFactory(
		g.serverConfig,
		g.sManager,
		authCallback,
	), nil
}

// Создание сервиса доставки уведомлений до конечного пользователя по WS
func (g *graphQlSchemaBuilder) BuildWsDispatcher() (WsDispatcherInterface, error) {
	schema, err := g.buildSchema()
	if nil != err {
		return nil, err
	}

	return g.wsDispatcherFactory(
		g.sManager,
		*schema,
	), nil
}

// Фабрика билдера запросов
func NewGraphQlSchemaBuilder() GraphQlSchemaBuilderInterface {
	return &graphQlSchemaBuilder{
		logger:              helpers.NewLogger(`graphQlSchemaBuilder`),
		aliasGenerator:      aliasGenerationService.AliasGenerationService(),
		serverFactory:       newGraphQlServer,
		wsServerFactory:     newWsGraphQlServer,
		wsDispatcherFactory: newWsDispatcher,
		resolverFactory:     resolverGenerator.NewResolverGenerator(),
		wsResolveGenerator:  wsResolveGenerator.NewWsResolveGenerator(),
		schemaGenerators: []queryGenerator.QueryGeneratorInterface{
			queryGenerator.QueryGeneratorFactory(),
			queryGenerator.AggregateQueryGeneratorFactory(),
			queryGenerator.InsertMutationGenerator(),
			queryGenerator.UpdateMutationGenerator(),
			queryGenerator.DeleteMutationGenerator(),
		},
		sManager:     nil,
		queries:      graphql.Fields{},
		mutations:    graphql.Fields{},
		subscription: graphql.Fields{},
		serverConfig: GraphQlServerConfig{
			Host:   "0.0.0.0",
			Port:   8080,
			Uri:    "/graphql",
			WsPort: 9000,
			WsUri:  "/subscriptions",
		},
		schema: nil,
	}
}

// Фабрика билдера запросов с кастомизацией
func NewGraphQlSchemaBuilderWithCustomization(
	customizations []customizationService.CustomizationInterface,
) GraphQlSchemaBuilderInterface {
	return &graphQlSchemaBuilder{
		logger:              helpers.NewLogger(`graphQlSchemaBuilder`),
		aliasGenerator:      aliasGenerationService.AliasGenerationService(),
		serverFactory:       newGraphQlServer,
		wsServerFactory:     newWsGraphQlServer,
		wsDispatcherFactory: newWsDispatcher,
		resolverFactory:     resolverGenerator.NewResolverGeneratorWithCustomizations(customizations),
		wsResolveGenerator:  wsResolveGenerator.NewWsResolveGenerator(),
		schemaGenerators: []queryGenerator.QueryGeneratorInterface{
			queryGenerator.QueryGeneratorFactoryWithCustomizations(customizations),
			queryGenerator.AggregateQueryGeneratorFactory(),
			queryGenerator.InsertMutationGeneratorWithCustomizations(customizations),
			queryGenerator.UpdateMutationGeneratorWithCustomizations(customizations),
			queryGenerator.DeleteMutationGenerator(),
		},
		sManager:     nil,
		queries:      graphql.Fields{},
		mutations:    graphql.Fields{},
		subscription: graphql.Fields{},
		serverConfig: GraphQlServerConfig{
			Host:   "0.0.0.0",
			Port:   8080,
			Uri:    "/graphql",
			WsPort: 9000,
			WsUri:  "/subscriptions",
		},
		schema:         nil,
		customizations: customizations,
	}
}
