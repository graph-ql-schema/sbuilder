package groupByParser

import (
	"context"
)

// Тип, описывающий срез полей группировки
type GroupBy = []string

// Интерфейс парсера параметров группировки запроса
type GroupByParserInterface interface {
	// Парсинг полей группировки
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (GroupBy, error)
}

// Тип, описывающий фабрику парсера параметров группировки запроса
type TGroupByParserFactory = func() GroupByParserInterface
