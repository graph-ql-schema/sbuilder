package groupByParametersGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Генератор параметров сортировки
type groupByParametersGenerator struct {
	logger     *logrus.Entry
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Генерация параметров сортировки
func (g groupByParametersGenerator) Generate(ctx context.Context, object *graphql.Object, namePrefix string) *graphql.ArgumentConfig {
	g.logger.WithFields(logrus.Fields{
		"code":       100,
		"object":     object.Name(),
		"namePrefix": namePrefix,
	}).Debug(`Started group by parameters generation for object`)

	name := fmt.Sprintf(`%v_group_by_parameters`, namePrefix)
	return &graphql.ArgumentConfig{
		Type: graphql.NewList(graphql.NewEnum(graphql.EnumConfig{
			Name:        fmt.Sprintf(`%v_group_by`, name),
			Values:      g.getEnumConfigMapByObject(object),
			Description: "Варианты выбора полей для группировки",
		})),
		Description: "Группировка по переданным параметрам",
	}
}

// Генерация коллекции значений для группировки
func (g groupByParametersGenerator) getEnumConfigMapByObject(object *graphql.Object) graphql.EnumValueConfigMap {
	configMap := graphql.EnumValueConfigMap{}
	for code, field := range object.Fields() {
		fieldType := g.typeGetter.GetRootType(field.Type)
		if _, ok := fieldType.(*graphql.Object); ok {
			continue
		}

		configMap[code] = &graphql.EnumValueConfig{
			Value:       code,
			Description: fmt.Sprintf(`Значение %v`, code),
		}
	}

	return configMap
}

// Фабрика генератора параметров сортировки
func GroupByParametersGenerator() GroupByParametersGeneratorInterface {
	return &groupByParametersGenerator{
		logger:     helpers.NewLogger(`groupByParametersGenerator`),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
