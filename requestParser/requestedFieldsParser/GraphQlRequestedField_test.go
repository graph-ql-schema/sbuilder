package requestedFieldsParser

import (
	"reflect"
	"testing"
)

// Тестирование получения названия
func Test_graphQlRequestedField_GetFieldName(t *testing.T) {
	type fields struct {
		fieldName string
		subFields GraphQlRequestedFields
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения названия",
			fields: fields{
				fieldName: "test",
				subFields: nil,
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := graphQlRequestedField{
				fieldName: tt.fields.fieldName,
				subFields: tt.fields.subFields,
			}
			if got := g.GetFieldName(); got != tt.want {
				t.Errorf("GetFieldName() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование проверки на наличие дочерних полей
func Test_graphQlRequestedField_HasSubFields(t *testing.T) {
	type fields struct {
		fieldName string
		subFields GraphQlRequestedFields
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "Тестирование отсутствующей коллекции",
			fields: fields{
				fieldName: "test",
				subFields: nil,
			},
			want: false,
		},
		{
			name: "Тестирование пустой коллекции",
			fields: fields{
				fieldName: "test",
				subFields: GraphQlRequestedFields{},
			},
			want: false,
		},
		{
			name: "Тестирование не пустой коллекции",
			fields: fields{
				fieldName: "test",
				subFields: GraphQlRequestedFields{
					&graphQlRequestedField{},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := graphQlRequestedField{
				fieldName: tt.fields.fieldName,
				subFields: tt.fields.subFields,
			}
			if got := g.HasSubFields(); got != tt.want {
				t.Errorf("HasSubFields() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения дочерней коллекции полей
func Test_graphQlRequestedField_GetSubFields(t *testing.T) {
	type fields struct {
		fieldName string
		subFields GraphQlRequestedFields
	}
	tests := []struct {
		name   string
		fields fields
		want   GraphQlRequestedFields
	}{
		{
			name: "Тестирование отсутствующей коллекции",
			fields: fields{
				fieldName: "test",
				subFields: nil,
			},
			want: GraphQlRequestedFields{},
		},
		{
			name: "Тестирование пустой коллекции",
			fields: fields{
				fieldName: "test",
				subFields: GraphQlRequestedFields{},
			},
			want: GraphQlRequestedFields{},
		},
		{
			name: "Тестирование не пустой коллекции",
			fields: fields{
				fieldName: "test",
				subFields: GraphQlRequestedFields{
					&graphQlRequestedField{},
				},
			},
			want: GraphQlRequestedFields{
				&graphQlRequestedField{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := graphQlRequestedField{
				fieldName: tt.fields.fieldName,
				subFields: tt.fields.subFields,
			}
			if got := g.GetSubFields(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetSubFields() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование фабрики сущности
func TestNewGraphQlRequestedField(t *testing.T) {
	type args struct {
		fieldName string
		subFields GraphQlRequestedFields
	}
	tests := []struct {
		name string
		args args
		want GraphQlRequestedFieldInterface
	}{
		{
			name: "Вариант 1",
			args: args{
				fieldName: "test",
				subFields: nil,
			},
			want: &graphQlRequestedField{
				fieldName: "test",
				subFields: GraphQlRequestedFields{},
			},
		},
		{
			name: "Вариант 2",
			args: args{
				fieldName: "test-2",
				subFields: GraphQlRequestedFields{
					&graphQlRequestedField{},
				},
			},
			want: &graphQlRequestedField{
				fieldName: "test-2",
				subFields: GraphQlRequestedFields{
					&graphQlRequestedField{},
				},
			},
		},
		{
			name: "Вариант 3",
			args: args{
				fieldName: "test-3",
				subFields: GraphQlRequestedFields{},
			},
			want: &graphQlRequestedField{
				fieldName: "test-3",
				subFields: GraphQlRequestedFields{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewGraphQlRequestedField(tt.args.fieldName, tt.args.subFields); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewGraphQlRequestedField() = %v, want %v", got, tt.want)
			}
		})
	}
}
