package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/paginationParametersGenerator"
	"context"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
)

// Процессор генерации параметров Offset
type offsetQueryGeneratorProcessor struct {
	logger              *logrus.Entry
	paginationGenerator paginationParametersGenerator.PaginationParametersGeneratorInterface
}

// Получение названия генерируемого поля
func (l offsetQueryGeneratorProcessor) Name() string {
	return constants.OffsetSchemaKey
}

// Генерация
func (l offsetQueryGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
) *graphql.ArgumentConfig {
	l.logger.WithFields(logrus.Fields{
		"code":       100,
		"object":     object.Name(),
		"namePrefix": namePrefix,
	}).Debug(`Generation parameters for 'offset' parameter`)

	return l.paginationGenerator.GenerateOffset()
}

func newOffsetQueryGeneratorProcessor() queryGeneratorProcessorInterface {
	return &offsetQueryGeneratorProcessor{
		logger:              helpers.NewLogger(`offsetQueryGeneratorProcessor`),
		paginationGenerator: paginationParametersGenerator.PaginationParametersGenerator(),
	}
}
