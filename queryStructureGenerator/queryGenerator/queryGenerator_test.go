package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации
func Test_queryGenerator_Generate(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		logger         *logrus.Entry
		processors     []queryGeneratorProcessorInterface
		aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
	}
	type args struct {
		object   *graphql.Object
		resolver TResolver
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Field
	}{
		{
			name: "Тестирование генерации #1",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				processors: []queryGeneratorProcessorInterface{
					&queryGeneratorProcessorMock{
						name:         "test",
						isCalled:     false,
						returnResult: nil,
					},
				},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
			},
			args: args{
				object:   objectConfig,
				resolver: nil,
			},
			want: &graphql.Field{
				Type:        graphql.NewList(objectConfig),
				Name:        `Листинг сущностей 'role'`,
				Description: `Листинг сущностей 'role'`,
				Args:        graphql.FieldConfigArgument{},
				Resolve:     nil,
			},
		},
		{
			name: "Тестирование генерации #2",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				processors: []queryGeneratorProcessorInterface{
					&queryGeneratorProcessorMock{
						name:         "test",
						isCalled:     false,
						returnResult: &graphql.ArgumentConfig{},
					},
				},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
			},
			args: args{
				object:   objectConfig,
				resolver: nil,
			},
			want: &graphql.Field{
				Type:        graphql.NewList(objectConfig),
				Name:        `Листинг сущностей 'role'`,
				Description: `Листинг сущностей 'role'`,
				Args: graphql.FieldConfigArgument{
					"test": &graphql.ArgumentConfig{},
				},
				Resolve: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				logger:         tt.fields.logger,
				processors:     tt.fields.processors,
				aliasGenerator: tt.fields.aliasGenerator,
			}
			if got := q.Generate(context.Background(), tt.args.object, tt.args.resolver); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения суффикса
func Test_queryGenerator_Suffix(t *testing.T) {
	type fields struct {
		logger         *logrus.Entry
		processors     []queryGeneratorProcessorInterface
		aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   constants.QueryType
	}{
		{
			name:   "Тестирование получения суффикса",
			fields: fields{},
			want:   "_list",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				logger:         tt.fields.logger,
				processors:     tt.fields.processors,
				aliasGenerator: tt.fields.aliasGenerator,
			}
			if got := q.Suffix(); got != tt.want {
				t.Errorf("Suffix() = %v, want %v", got, tt.want)
			}
		})
	}
}
