package constants

// Типы запросов, доступных для генерации
type QueryType string

const (
	ListQuery      QueryType = "_list"
	AggregateQuery QueryType = "_aggregate"
	InsertMutation QueryType = "_insert"
	UpdateMutation QueryType = "_update"
	DeleteMutation QueryType = "_delete"
)
