package sbuilder

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/functionalfoundry/graphqlws"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

// Сервер GraphQL API для WS
type wsGraphQlServer struct {
	configuration GraphQlServerConfig
	isStarted     bool
	isAlive       bool
	server        *http.Server
	sManager      graphqlws.SubscriptionManager
	authCallback  tAuthCallback
	logger        *logrus.Entry
}

// Запуск сервера
func (w *wsGraphQlServer) Run() error {
	handler := graphqlws.NewHandler(graphqlws.HandlerConfig{
		SubscriptionManager: w.sManager,
		Authenticate:        w.authCallback,
	})

	router := http.NewServeMux()
	router.Handle(w.configuration.WsUri, handler)

	host := fmt.Sprintf(`%v:%v`, w.configuration.Host, w.configuration.WsPort)
	w.server = &http.Server{
		Addr:    host,
		Handler: router,
	}

	w.logger.WithField("host", host).Info(`Starting WS GraphQL server`)

	w.isStarted = true
	err := w.server.ListenAndServe()
	w.isAlive = false

	return err
}

// Остановка сервера
func (w *wsGraphQlServer) GracefulShutdown() {
	w.logger.Debug(`Initialize graceful shutdown. Timeout 10 sec.`)

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	for connection := range w.sManager.Subscriptions() {
		w.sManager.RemoveSubscriptions(connection)
		connection.SendError(fmt.Errorf(`server terminating`))
	}

	_ = w.server.Shutdown(ctx)

	w.logger.Debug(`Server is shutdown`)
}

// Возвращает статус состояния запуска сервиса
func (w *wsGraphQlServer) IsStarted() bool {
	return w.isStarted
}

// Возвращает статус сервиса: живой или нет
func (w *wsGraphQlServer) IsAlive() bool {
	return w.isAlive
}

// Фабрика сервера
func newWsGraphQlServer(
	configuration GraphQlServerConfig,
	sManager graphqlws.SubscriptionManager,
	authCallback tAuthCallback,
) GraphQlServerInterface {
	return &wsGraphQlServer{
		configuration: configuration,
		isStarted:     false,
		isAlive:       true,
		server:        nil,
		sManager:      sManager,
		authCallback:  authCallback,
		logger:        helpers.NewLogger("GraphQL WS"),
	}
}
