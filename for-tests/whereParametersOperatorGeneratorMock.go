package for_tests

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования. Реализация WhereParametersOperatorGeneratorInterface
type WhereParametersOperatorGeneratorMock struct {
	IsCalled     bool
	ReturnResult *graphql.InputObjectConfigFieldMap
}

// Генерация параметров
func (w *WhereParametersOperatorGeneratorMock) GenerateOperations(
	context.Context,
	*graphql.Object,
	string,
	uint8,
) *graphql.InputObjectConfigFieldMap {
	w.IsCalled = true

	return w.ReturnResult
}
