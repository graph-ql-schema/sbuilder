package whereParametersOperatorGenerator

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Генератор операторов поля для Where
type WhereParametersOperatorGeneratorInterface interface {
	// Генерация параметров
	GenerateOperations(context.Context, *graphql.Object, string, uint8) *graphql.InputObjectConfigFieldMap
}

// Обработчик генератора операторов поля для Where
type whereParametersOperatorGeneratorProcessorInterface interface {
	// Проверка доступности процессора
	IsAvailable(ctx context.Context, field *graphql.FieldDefinition, level uint8) bool

	// Код оператора, для вставки в схему GraphQL
	OperatorCode() string

	// Генерация параметров
	Generate(
		ctx context.Context,
		field *graphql.FieldDefinition,
		level uint8,
		namePrefix string,
	) *graphql.InputObjectFieldConfig
}
