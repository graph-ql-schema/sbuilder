package sbuilder

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
)

// Набор экспортируемых типов данных для удобства использования основного функционала библиотеки
// Экспортирует константы типов запросов и связанные с генерацией API типы данных
type Parameters = types.Parameters
type EntityQueries = types.EntityQueries
type QueryType = constants.QueryType

// Набор экспортируемых типов данных для кастомизиции
type CustomizationInterface = customizationService.CustomizationInterface
type WhereOrHavingParserProcessorInterface = whereOrHavingParser.WhereOrHavingParserProcessorInterface
type Operation = whereOrHavingParser.Operation
type WhereOrHavingParserInterface = whereOrHavingParser.WhereOrHavingParserInterface

const (
	ListQuery      = constants.ListQuery
	AggregateQuery = constants.AggregateQuery
	InsertMutation = constants.InsertMutation
	UpdateMutation = constants.UpdateMutation
	DeleteMutation = constants.DeleteMutation
)

const (
	EventTypeCreated types.TEventType = "created"
	EventTypeUpdated types.TEventType = "updated"
	EventTypeDeleted types.TEventType = "deleted"
)

// Типы кастомизаций
const (
	CustomizationTypeWhereParametersOperatorGenerator = constants.CustomizationTypeWhereParametersOperatorGenerator
	CustomizationTypeOperationProcessor = constants.CustomizationTypeOperationProcessor
	CustomizationTypeValueConverter = constants.CustomizationTypeValueConverter
)