package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Генератор мутации удаления сущности
type deleteMutationGenerator struct {
	logger         *logrus.Entry
	typeGetter     gql_root_type_getter.GraphQlRootTypeGetterInterface
	processors     []queryGeneratorProcessorInterface
	aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
}

// Получение суффикса для названия мутации
func (d deleteMutationGenerator) Suffix() constants.QueryType {
	return constants.DeleteMutation
}

// Генерация запроса на получение сущности
func (d deleteMutationGenerator) Generate(
	ctx context.Context,
	object *graphql.Object,
	resolver TResolver,
) *graphql.Field {
	d.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(fmt.Sprintf(`Generation delete mutation for entity '%v'`, object.Name()))

	name := fmt.Sprintf(`%v_delete`, d.aliasGenerator.Generate(object.Name()))
	arguments := graphql.FieldConfigArgument{}
	for _, processor := range d.processors {
		res := processor.Generate(ctx, object, name)
		if nil == res {
			continue
		}

		arguments[processor.Name()] = res
	}

	if 0 == len(arguments) {
		return nil
	}

	return &graphql.Field{
		Type:        d.generateResponse(object, name),
		Name:        fmt.Sprintf(`%v_mutation`, name),
		Description: fmt.Sprintf("Мутация удаления значений объекта '%v'", object.Name()),
		Args:        arguments,
		Resolve:     resolver,
	}
}

// Генератор объекта результата обработки запроса
func (d deleteMutationGenerator) generateResponse(object *graphql.Object, namePrefix string) *graphql.Object {
	return graphql.NewObject(
		graphql.ObjectConfig{
			Name: fmt.Sprintf(`%v_result_type`, namePrefix),
			Fields: graphql.Fields{
				"affected_rows": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.Int),
					Name:        fmt.Sprintf(`%v_affected`, namePrefix),
					Description: "Количество записей, удаленных запросом",
				},
			},
			Description: fmt.Sprintf(`Результат удаления значений для сущности '%v'`, object.Name()),
		},
	)
}

// Фабрика генератор
func DeleteMutationGenerator() QueryGeneratorInterface {
	return &deleteMutationGenerator{
		logger:     helpers.NewLogger(`deleteMutationGenerator`),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		processors: []queryGeneratorProcessorInterface{
			newWhereOrHavingQueryGeneratorProcessor(constants.WhereSchemaKey),
		},
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
	}
}
