package orderParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Парсер параметров сортировки
type orderParser struct {
	logger *logrus.Entry
}

// Парсинг параметров сортировки
func (o orderParser) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (order []Order, err error) {
	o.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started order parameters parsing")

	orderRequestData, ok := arguments[constants.OrderBySchemaKey]
	if !ok {
		return nil, nil
	}

	orderParsed, ok := orderRequestData.([]interface{})
	if !ok {
		err = fmt.Errorf("order parameters is not in valid format")
		o.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	result := make([]Order, len(orderParsed))
	resultIndex := 0

	for _, orderByNotParsed := range orderParsed {
		orderBy, ok := orderByNotParsed.(map[string]interface{})
		if !ok {
			o.logger.WithFields(logrus.Fields{
				"code":      400,
				"arguments": arguments,
				"orderBy":   orderBy,
			}).Warning("Order parameters format is not valid")

			continue
		}

		by, byOk := orderBy[`by`]
		priority, priorityOk := orderBy[`priority`]
		direction, directionOk := orderBy[`direction`]

		if !directionOk || !priorityOk || !byOk {
			o.logger.WithFields(logrus.Fields{
				"code":      400,
				"arguments": arguments,
				"orderBy":   orderBy,
			}).Warning("Order parameters format is not valid")

			continue
		}

		byParsed, byOk := by.(string)
		priorityParsed, priorityOk := priority.(int)
		directionParsed, directionOk := direction.(string)

		if !directionOk || !priorityOk || !byOk {
			o.logger.WithFields(logrus.Fields{
				"code":      400,
				"arguments": arguments,
				"orderBy":   orderBy,
			}).Warning("Order parameters has invalid type")

			continue
		}

		if directionParsed != "asc" && directionParsed != "desc" {
			o.logger.WithFields(logrus.Fields{
				"code":      400,
				"arguments": arguments,
				"orderBy":   orderBy,
			}).Warning("Order direction should be 'asc' or 'desc'")

			continue
		}

		if priorityParsed <= 0 {
			o.logger.WithFields(logrus.Fields{
				"code":      400,
				"arguments": arguments,
				"orderBy":   orderBy,
			}).Warning("Order priority less that 0")

			continue
		}

		result[resultIndex] = Order{
			Priority:  priorityParsed,
			By:        byParsed,
			Direction: OrderDirection(directionParsed),
		}

		resultIndex++
	}

	sort := make([]Order, resultIndex)
	for i := 0; i < resultIndex; i++ {
		sort[i] = result[i]
	}

	o.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
		"result":    sort,
	}).Debug("Order parameters parsed")

	return sort, nil
}

// Фабрика парсера
func NewOrderParser() OrderParserInterface {
	return &orderParser{
		logger: helpers.NewLogger(`orderParser`),
	}
}
