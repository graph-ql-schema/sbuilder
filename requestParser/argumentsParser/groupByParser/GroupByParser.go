package groupByParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Парсер параметров группировки запроса
type groupByParser struct {
	logger *logrus.Entry
}

// Парсинг полей группировки
func (g groupByParser) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (GroupBy, error) {
	g.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug(`Started GroupBy parameters parsing`)

	groupByParameters, ok := arguments[constants.GroupBySchemaKey]
	if !ok {
		err := fmt.Errorf(`GroupBy items isn't passed, or schema key is incorrect. Valid schema key: "%v"`, constants.GroupBySchemaKey)
		g.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	var result GroupBy
	if reflect.TypeOf(groupByParameters).Kind() == reflect.Slice {
		s := reflect.ValueOf(groupByParameters)
		result = make(GroupBy, s.Len())

		for i := 0; i < s.Len(); i++ {
			item, ok := s.Index(i).Interface().(string)
			if !ok {
				err := fmt.Errorf(`one of passed GroupBy items is not string`)
				g.logger.WithFields(logrus.Fields{
					"code": 400,
				}).Warning(err.Error())

				return nil, err
			}

			result[i] = item
		}
	} else {
		err := fmt.Errorf(`GroupBy items isn't an array`)
		g.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	g.logger.WithFields(logrus.Fields{
		"code":   100,
		"result": result,
	}).Debug("GroupBy parameters parsed")

	return result, nil
}

// Фабрика парсера параметров группировки
func NewGroupByParser() GroupByParserInterface {
	return &groupByParser{
		logger: helpers.NewLogger(`groupByParser`),
	}
}
