package setParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования
type SetParserMock struct {
	Result Set
	IsErr  bool
}

// Парсинг значений для обновления сущности
func (s SetParserMock) Parse(
	context.Context,
	map[string]interface{},
) (Set, error) {
	if s.IsErr {
		return nil, fmt.Errorf(`test`)
	}

	return s.Result, nil
}
