package whereParametersLogicOperationsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"github.com/graphql-go/graphql"
)

// Тестирование установки генератора
func Test_whereParametersLogicOperationsGeneratorProcessor_SetWhereParametersOperatorGenerator(t *testing.T) {
	generator := &for_tests.WhereParametersGeneratorMock{}

	type fields struct {
		maxLevel      uint8
		generator     interfaces.WhereParametersGeneratorInterface
		logger        *logrus.Entry
		operationName string
		operationCode string
	}
	type args struct {
		operator interfaces.WhereParametersGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование установки генератора",
			fields: fields{
				maxLevel:      0,
				generator:     nil,
				logger:        nil,
				operationName: "and",
				operationCode: "and",
			},
			args: args{
				operator: generator,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &whereParametersLogicOperationsGeneratorProcessor{
				maxLevel:      tt.fields.maxLevel,
				generator:     tt.fields.generator,
				logger:        tt.fields.logger,
				operationName: tt.fields.operationName,
				operationCode: tt.fields.operationCode,
			}

			a.SetWhereParametersOperatorGenerator(tt.args.operator)

			if !reflect.DeepEqual(a.generator, tt.args.operator) {
				t.Errorf("SetWhereParametersOperatorGenerator() Generator %v, want %v", a.generator, tt.args.operator)
			}
		})
	}
}

// Тестирование получения кода операции
func Test_whereParametersLogicOperationsGeneratorProcessor_OperationCode(t *testing.T) {
	type fields struct {
		maxLevel      uint8
		generator     interfaces.WhereParametersGeneratorInterface
		logger        *logrus.Entry
		operationName string
		operationCode string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения кода операции",
			fields: fields{
				maxLevel:      1,
				generator:     nil,
				logger:        nil,
				operationName: "and",
				operationCode: "_and",
			},
			want: "_and",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := whereParametersLogicOperationsGeneratorProcessor{
				maxLevel:      tt.fields.maxLevel,
				generator:     tt.fields.generator,
				logger:        tt.fields.logger,
				operationName: tt.fields.operationName,
				operationCode: tt.fields.operationCode,
			}
			if got := a.OperationCode(); got != tt.want {
				t.Errorf("OperationCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование корректности кода операции в результате
func Test_whereParametersLogicOperationsGeneratorProcessor_Generate_OperationName(t *testing.T) {
	type fields struct {
		maxLevel      uint8
		generator     interfaces.WhereParametersGeneratorInterface
		logger        *logrus.Entry
		operationName string
		operationCode string
	}
	type args struct {
		object     *graphql.Object
		level      uint8
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование корректности кода операции в результате. Вариант 1.",
			fields: fields{
				maxLevel: 100,
				generator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger:        helpers.NewLogger(`test`),
				operationName: "and",
				operationCode: "_and",
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "test",
			},
			want: "test_and!",
		},
		{
			name: "Тестирование корректности кода операции в результате. Вариант 2.",
			fields: fields{
				maxLevel: 100,
				generator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger:        helpers.NewLogger(`test`),
				operationName: "and",
				operationCode: "_and",
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "",
			},
			want: "and!",
		},
		{
			name: "Тестирование корректности кода операции в результате. Вариант 3.",
			fields: fields{
				maxLevel: 100,
				generator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger:        helpers.NewLogger(`test`),
				operationName: "and",
				operationCode: "_and",
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "test_test",
			},
			want: "test_test_and!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := whereParametersLogicOperationsGeneratorProcessor{
				maxLevel:      tt.fields.maxLevel,
				generator:     tt.fields.generator,
				logger:        tt.fields.logger,
				operationName: tt.fields.operationName,
				operationCode: tt.fields.operationCode,
			}
			if got := a.Generate(context.Background(), tt.args.object, tt.args.level, tt.args.namePrefix); !reflect.DeepEqual(got.Type.Name(), tt.want) {
				t.Errorf("Generate() Type name = %v, want %v", got.Type.Name(), tt.want)
			}
		})
	}
}

// Тестирование корректности ответа при различных вариантах параметров, когда результата быть не должно.
func Test_whereParametersLogicOperationsGeneratorProcessor_Generate_NilResultTest(t *testing.T) {
	type fields struct {
		maxLevel      uint8
		generator     interfaces.WhereParametersGeneratorInterface
		logger        *logrus.Entry
		operationName string
		operationCode string
	}
	type args struct {
		object     *graphql.Object
		level      uint8
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование передачи пустого объекта",
			fields: fields{
				maxLevel: 100,
				generator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger:        helpers.NewLogger(`test`),
				operationName: "and",
				operationCode: "_and",
			},
			args: args{
				object:     &graphql.Object{},
				level:      1,
				namePrefix: "test",
			},
		},
		{
			name: "Тестирование передачи превышения максимального уровня генерации",
			fields: fields{
				maxLevel: 1,
				generator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger:        helpers.NewLogger(`test`),
				operationName: "and",
				operationCode: "_and",
			},
			args: args{
				object:     &graphql.Object{},
				level:      2,
				namePrefix: "test",
			},
		},
		{
			name: "Тестирование возвращения пустого объекта генератором",
			fields: fields{
				maxLevel: 100,
				generator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: nil,
				},
				logger:        helpers.NewLogger(`test`),
				operationName: "and",
				operationCode: "_and",
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := whereParametersLogicOperationsGeneratorProcessor{
				maxLevel:      tt.fields.maxLevel,
				generator:     tt.fields.generator,
				logger:        tt.fields.logger,
				operationName: tt.fields.operationName,
				operationCode: tt.fields.operationCode,
			}
			if got := a.Generate(context.Background(), tt.args.object, tt.args.level, tt.args.namePrefix); got != nil {
				t.Errorf("Generate() = %v, want %v", got, nil)
			}
		})
	}
}
