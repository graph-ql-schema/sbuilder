package setParser

import (
	"context"
)

// Тип, описывающий параметры обновления сущности
type Set = map[string]interface{}

// Парсер параметров обновления сущности
type SetParserInterface interface {
	// Парсинг значений для обновления сущности
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (Set, error)
}
