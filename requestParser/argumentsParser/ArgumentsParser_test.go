package argumentsParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/groupByParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/orderParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/paginationParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/setParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
)

// Тестирование парсера
func Test_argumentsParser_Parse(t *testing.T) {
	type fields struct {
		logger               *logrus.Entry
		objectsService       objectsParser.ObjectsParserInterface
		setService           setParser.SetParserInterface
		groupByService       groupByParser.GroupByParserInterface
		orderService         orderParser.OrderParserInterface
		paginationService    paginationParser.PaginationParserInterface
		whereOrHavingService whereOrHavingParser.WhereOrHavingParserInterface
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *ParsedArguments
		wantErr bool
	}{
		{
			name: "Тестирование результата генерации группировки (ошибка)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService:     &setParser.SetParserMock{},
				groupByService: &groupByParser.GroupByParserMock{
					Result: nil,
					IsErr:  true,
				},
				orderService:         &orderParser.OrderParserMock{},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.GroupBySchemaKey: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование результата генерации группировки (без ошибки)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService:     &setParser.SetParserMock{},
				groupByService: &groupByParser.GroupByParserMock{
					Result: groupByParser.GroupBy{"test"},
					IsErr:  false,
				},
				orderService:         &orderParser.OrderParserMock{},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.GroupBySchemaKey: "test",
				},
			},
			want: &ParsedArguments{
				Objects:    nil,
				Set:        nil,
				GroupBy:    groupByParser.GroupBy{"test"},
				OrderBy:    nil,
				Pagination: nil,
				Where:      nil,
				Having:     nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование результата генерации сортировки (ошибка)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService:     &setParser.SetParserMock{},
				groupByService: &groupByParser.GroupByParserMock{},
				orderService: &orderParser.OrderParserMock{
					Result: nil,
					IsErr:  true,
				},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.OrderBySchemaKey: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование результата генерации сортировки (без ошибки)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService:     &setParser.SetParserMock{},
				groupByService: &groupByParser.GroupByParserMock{},
				orderService: &orderParser.OrderParserMock{
					Result: []orderParser.Order{
						{
							Priority:  1,
							By:        "test",
							Direction: "desc",
						},
					},
					IsErr: false,
				},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.OrderBySchemaKey: "test",
				},
			},
			want: &ParsedArguments{
				Objects: nil,
				Set:     nil,
				GroupBy: nil,
				OrderBy: []orderParser.Order{
					{
						Priority:  1,
						By:        "test",
						Direction: "desc",
					},
				},
				Pagination: nil,
				Where:      nil,
				Having:     nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование результата генерации объектов вставки (ошибка)",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{
					Result: objectsParser.Objects{},
					IsErr:  true,
				},
				setService:           &setParser.SetParserMock{},
				groupByService:       &groupByParser.GroupByParserMock{},
				orderService:         &orderParser.OrderParserMock{},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.ObjectsSchemaKey: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование результата генерации объектов вставки (без ошибки)",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{
					Result: objectsParser.Objects{
						{"test": 1},
					},
					IsErr: false,
				},
				setService:           &setParser.SetParserMock{},
				groupByService:       &groupByParser.GroupByParserMock{},
				orderService:         &orderParser.OrderParserMock{},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.ObjectsSchemaKey: "test",
				},
			},
			want: &ParsedArguments{
				Objects: objectsParser.Objects{
					{"test": 1},
				},
				Set:        nil,
				GroupBy:    nil,
				OrderBy:    nil,
				Pagination: nil,
				Where:      nil,
				Having:     nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование результата генерации значений обновления (ошибка)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService: &setParser.SetParserMock{
					Result: nil,
					IsErr:  true,
				},
				groupByService:       &groupByParser.GroupByParserMock{},
				orderService:         &orderParser.OrderParserMock{},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.SetSchemaKey: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование результата генерации значений обновления (без ошибки)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService: &setParser.SetParserMock{
					Result: setParser.Set{
						"test": 1,
					},
					IsErr: false,
				},
				groupByService:       &groupByParser.GroupByParserMock{},
				orderService:         &orderParser.OrderParserMock{},
				paginationService:    &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.SetSchemaKey: "test",
				},
			},
			want: &ParsedArguments{
				Objects: nil,
				Set: setParser.Set{
					"test": 1,
				},
				GroupBy:    nil,
				OrderBy:    nil,
				Pagination: nil,
				Where:      nil,
				Having:     nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование результата генерации пагенации (ошибка)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService:     &setParser.SetParserMock{},
				groupByService: &groupByParser.GroupByParserMock{},
				orderService:   &orderParser.OrderParserMock{},
				paginationService: &paginationParser.PaginationParserMock{
					Result: nil,
					IsErr:  true,
				},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.LimitSchemaKey:  "test",
					constants.OffsetSchemaKey: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование результата генерации пагенации (без ошибки)",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				objectsService: &objectsParser.ObjectsParserMock{},
				setService:     &setParser.SetParserMock{},
				groupByService: &groupByParser.GroupByParserMock{},
				orderService:   &orderParser.OrderParserMock{},
				paginationService: &paginationParser.PaginationParserMock{
					Result: &paginationParser.Pagination{
						Limit:  1,
						Offset: 2,
					},
					IsErr: false,
				},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.LimitSchemaKey:  "test",
					constants.OffsetSchemaKey: "test",
				},
			},
			want: &ParsedArguments{
				Objects: nil,
				Set:     nil,
				GroupBy: nil,
				OrderBy: nil,
				Pagination: &paginationParser.Pagination{
					Limit:  1,
					Offset: 2,
				},
				Where:  nil,
				Having: nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование результата генерации фильтрации Where (ошибка)",
			fields: fields{
				logger:            helpers.NewLogger(`test`),
				objectsService:    &objectsParser.ObjectsParserMock{},
				setService:        &setParser.SetParserMock{},
				groupByService:    &groupByParser.GroupByParserMock{},
				orderService:      &orderParser.OrderParserMock{},
				paginationService: &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     true,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование результата генерации фильтрации Where (без ошибки)",
			fields: fields{
				logger:            helpers.NewLogger(`test`),
				objectsService:    &objectsParser.ObjectsParserMock{},
				setService:        &setParser.SetParserMock{},
				groupByService:    &groupByParser.GroupByParserMock{},
				orderService:      &orderParser.OrderParserMock{},
				paginationService: &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{
					OperationResult: &whereOrHavingParser.OperationStub{},
					ErrorResult:     false,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: "test",
				},
			},
			want: &ParsedArguments{
				Objects:    nil,
				Set:        nil,
				GroupBy:    nil,
				OrderBy:    nil,
				Pagination: nil,
				Where:      &whereOrHavingParser.OperationStub{},
				Having:     nil,
			},
			wantErr: false,
		},
		{
			name: "Тестирование результата генерации фильтрации Having (ошибка)",
			fields: fields{
				logger:            helpers.NewLogger(`test`),
				objectsService:    &objectsParser.ObjectsParserMock{},
				setService:        &setParser.SetParserMock{},
				groupByService:    &groupByParser.GroupByParserMock{},
				orderService:      &orderParser.OrderParserMock{},
				paginationService: &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     true,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.HavingSchemaKey: "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование результата генерации фильтрации Having (без ошибки)",
			fields: fields{
				logger:            helpers.NewLogger(`test`),
				objectsService:    &objectsParser.ObjectsParserMock{},
				setService:        &setParser.SetParserMock{},
				groupByService:    &groupByParser.GroupByParserMock{},
				orderService:      &orderParser.OrderParserMock{},
				paginationService: &paginationParser.PaginationParserMock{},
				whereOrHavingService: &whereOrHavingParser.WhereOrHavingParserMock{
					OperationResult: &whereOrHavingParser.OperationStub{},
					ErrorResult:     false,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.HavingSchemaKey: "test",
				},
			},
			want: &ParsedArguments{
				Objects:    nil,
				Set:        nil,
				GroupBy:    nil,
				OrderBy:    nil,
				Pagination: nil,
				Where:      nil,
				Having:     &whereOrHavingParser.OperationStub{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := argumentsParser{
				logger:               tt.fields.logger,
				objectsService:       tt.fields.objectsService,
				setService:           tt.fields.setService,
				groupByService:       tt.fields.groupByService,
				orderService:         tt.fields.orderService,
				paginationService:    tt.fields.paginationService,
				whereOrHavingService: tt.fields.whereOrHavingService,
			}
			got, err := a.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
