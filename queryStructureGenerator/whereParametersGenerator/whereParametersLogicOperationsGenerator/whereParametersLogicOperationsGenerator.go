package whereParametersLogicOperationsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/whereParametersOperatorGenerator"
	"context"
	"github.com/graphql-go/graphql"
)

// Генератор логических операций для Where
type whereParametersLogicOperationsGenerator struct {
	processors []whereParametersLogicOperationsGeneratorProcessorInterface
	generator  interfaces.WhereParametersGeneratorInterface
	maxLevel   uint8
}

// Установка генератора параметров для процессора
func (w *whereParametersLogicOperationsGenerator) SetWhereParametersOperatorGenerator(generator interfaces.WhereParametersGeneratorInterface) {
	w.generator = generator
	for _, processor := range w.processors {
		processor.SetWhereParametersOperatorGenerator(generator)
	}
}

// Генерация параметров
func (w whereParametersLogicOperationsGenerator) GenerateParametersForObject(
	ctx context.Context,
	object *graphql.Object,
	level uint8,
	namePrefix string,
) *graphql.InputObjectConfigFieldMap {
	result := graphql.InputObjectConfigFieldMap{}

	for _, processor := range w.processors {
		generationResult := processor.Generate(ctx, object, level, namePrefix)
		if nil == generationResult {
			continue
		}

		result[processor.OperationCode()] = generationResult
	}

	return &result
}

// Фабрика генератора логических операций для Where
func WhereParametersLogicOperationsGenerator(
	maxLevel uint8,
	generator whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface,
) WhereParametersLogicOperationsGeneratorInterface {
	return &whereParametersLogicOperationsGenerator{
		processors: []whereParametersLogicOperationsGeneratorProcessorInterface{
			newNotWhereParametersLogicOperationsGeneratorProcessor(generator),
			newWhereParametersLogicOperationsGeneratorProcessor(
				constants.AndSchemaKey,
				"AND",
				maxLevel,
			),
			newWhereParametersLogicOperationsGeneratorProcessor(
				constants.OrSchemaKey,
				"OR",
				maxLevel,
			),
		},
		generator: nil,
		maxLevel:  maxLevel,
	}
}
