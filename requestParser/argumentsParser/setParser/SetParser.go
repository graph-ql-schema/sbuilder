package setParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Парсер параметров обновления сущности
type setParser struct {
	logger *logrus.Entry
}

// Парсинг значений для обновления сущности
func (s setParser) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (Set, error) {
	s.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started update values parsing")

	updateValues, ok := arguments[constants.SetSchemaKey]
	if !ok {
		err := fmt.Errorf(`update values (SET) is not passed, or schema key is incorrect. Valid schema key: "%v"`, constants.SetSchemaKey)
		s.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	result, ok := updateValues.(Set)
	if !ok {
		err := fmt.Errorf(`update values (SET) isn't an update object`)
		s.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	s.logger.WithFields(logrus.Fields{
		"code":   100,
		"result": result,
	}).Debug("Update values (SET) parsed")

	return result, nil
}

// Фабрика парсера объектов для вставки
func NewSetParser() SetParserInterface {
	return &setParser{
		logger: helpers.NewLogger(`setParser`),
	}
}
