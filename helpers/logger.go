package helpers

import (
	"fmt"
	log "github.com/sirupsen/logrus"
)

// Фабрика логгера
func NewLogger(prefix string) *log.Entry {
	return log.WithField("prefix", fmt.Sprintf("sbuilder/%s", prefix))
}
