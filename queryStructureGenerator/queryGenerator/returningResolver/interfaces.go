package returningResolver

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"github.com/graphql-go/graphql"
)

// Резолвер поля returning для обновления/создания сущности
type ReturningResolverInterface interface {
	// Резолвинг поля returning для обновления/создания сущности
	Resolve(p graphql.ResolveParams) (interface{}, error)
}

// Тип, описывающий фабрику резолвера
type TReturningResolverFactory = func(object *graphql.Object) func(p graphql.ResolveParams) (interface{}, error)

// Тип, описывающий фабрику резолвера с кастомизаторами
type TReturningResolverFactoryWithCustomizations = func(
	object *graphql.Object,
	customizations []customizationService.CustomizationInterface,
) func(p graphql.ResolveParams) (interface{}, error)
