package aliasGenerationService

import (
	"fmt"
	"regexp"
	"strings"
)

// Генератор алиса по текущей строке. Заменяет все недопустимые символы в строке на альтернативу и возвращает результат.
type aliasGenerationService struct {
	replacements symbolReplaceMap
}

// Генерация алиса
func (a aliasGenerationService) Generate(source string) string {
	literalsToReplace := symbolReplaceMap{
		" ": `\s`,
		"-": `\-`,
	}

	result := strings.ToLower(source)
	includedSymbols := []string{}
	for sourceSymbol, _ := range a.replacements {
		if replacedSymbol, ok := literalsToReplace[sourceSymbol]; ok {
			sourceSymbol = replacedSymbol
		}

		includedSymbols = append(includedSymbols, sourceSymbol)
	}

	pattern := fmt.Sprintf(`[^%v]`, strings.Join(includedSymbols, ``))
	regexpRes := regexp.MustCompile(pattern)
	result = regexpRes.ReplaceAllString(result, ``)

	for sourceSymbol, replacement := range a.replacements {
		result = strings.ReplaceAll(result, sourceSymbol, replacement)
	}

	return result
}

// Фабрика генератор
func AliasGenerationService() AliasGenerationServiceInterface {
	return &aliasGenerationService{
		replacements: replaceMap,
	}
}
