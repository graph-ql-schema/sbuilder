package whereParametersLogicOperationsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"github.com/graphql-go/graphql"
)

// Процессор генератора оператора "and" и "or"
type whereParametersLogicOperationsGeneratorProcessor struct {
	maxLevel      uint8
	generator     interfaces.WhereParametersGeneratorInterface
	logger        *logrus.Entry
	operationName string
	operationCode string
}

// Установка генератора параметров для процессора
func (a *whereParametersLogicOperationsGeneratorProcessor) SetWhereParametersOperatorGenerator(
	generator interfaces.WhereParametersGeneratorInterface,
) {
	a.generator = generator
}

// Код операции, для вставки в схему GraphQL
func (a whereParametersLogicOperationsGeneratorProcessor) OperationCode() string {
	return a.operationCode
}

// Генерация параметров
func (a whereParametersLogicOperationsGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	level uint8,
	namePrefix string,
) *graphql.InputObjectFieldConfig {
	if level > a.maxLevel || 0 == len(object.Fields()) {
		return nil
	}

	name := strings.Replace(a.operationCode, "_", "", 1)
	if 0 != len(namePrefix) {
		name = fmt.Sprintf("%v%v", namePrefix, a.operationCode)
	}

	a.logger.WithFields(logrus.Fields{
		"code":              200,
		"object":            object.Name(),
		"level":             level,
		"input-object-name": name,
	}).Debug(fmt.Sprintf(`Generated %v operator for object`, a.operationName))

	fields := a.generator.BuildParameters(ctx, object, name, level+1)
	if nil == fields {
		return nil
	}

	return &graphql.InputObjectFieldConfig{
		Description: fmt.Sprintf(`Оператор %v для объединения условий`, a.operationName),
		Type: graphql.NewList(graphql.NewNonNull(graphql.NewInputObject(graphql.InputObjectConfig{
			Name:        name,
			Fields:      *fields,
			Description: fmt.Sprintf(`Объект листинга для оператора %v для объединения условий`, a.operationName),
		}))),
	}
}

// Фабрика процессора
func newWhereParametersLogicOperationsGeneratorProcessor(
	operationCode string,
	operationName string,
	maxLevel uint8,
) whereParametersLogicOperationsGeneratorProcessorInterface {
	return &whereParametersLogicOperationsGeneratorProcessor{
		maxLevel:      maxLevel,
		generator:     nil,
		logger:        helpers.NewLogger(fmt.Sprintf(`whereParametersLogicOperationsGeneratorProcessor(Operation - %v)`, operationName)),
		operationName: operationName,
		operationCode: operationCode,
	}
}
