package whereOrHavingParser

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"github.com/graphql-go/graphql"
)

// Операция IN
type inOperation struct {
	operationType  string
	operationValue []interface{}
	field          string
	valueConverter gql_sql_converter.GraphQlSqlConverterInterface
}

// Получение типа текущей операции
func (i inOperation) Type() string {
	return i.operationType
}

// Получение значения для текущей операции
func (i inOperation) Value() interface{} {
	return i.operationValue
}

// Получение кода поля для операции
func (i inOperation) Field() string {
	return i.field
}

// Конвертация в SQL
func (i inOperation) ToSQL(object *graphql.Object, fieldsMap map[string]string) (string, error) {
	fieldName := i.field
	if mappedField, ok := fieldsMap[fieldName]; ok {
		fieldName = mappedField
	}

	isWithNull := false
	values := []string{}
	for _, value := range i.operationValue {
		val, err := i.valueConverter.ToSQLValue(object, i.field, value)
		if nil != err {
			return "", fmt.Errorf(`'%v' field: %v`, i.field, err.Error())
		}

		if val == "null" {
			isWithNull = true
			continue
		}

		values = append(values, val)
	}

	sql := "1<>1"
	if 0 != len(values) {
		sql = fmt.Sprintf(`%v in (%v)`, fieldName, strings.Join(values, ","))
		if isWithNull {
			sql = fmt.Sprintf(`(%v or %v is null)`, sql, fieldName)
		}
	} else {
		sql = fmt.Sprintf(`%v is null`, fieldName)
	}

	return sql, nil
}

// Фабрика операций IN
type TNewInOperation = func(string, []interface{}, string) Operation

func NewInOperation(
	operationType string,
	operationValue []interface{},
	field string,
) Operation {
	return &inOperation{
		operationType:  operationType,
		operationValue: operationValue,
		field:          field,
		valueConverter: gql_sql_converter.NewGraphQlSqlConverter(),
	}
}
