package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
)

// Процессор генерации оператора "_between"
type betweenWhereParametersOperatorGeneratorProcessor struct {
	availableTypes  []graphql.Type
	maxLevel        uint8
	logger          *logrus.Entry
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Проверка доступности процессора
func (b betweenWhereParametersOperatorGeneratorProcessor) IsAvailable(ctx context.Context, field *graphql.FieldDefinition, level uint8) bool {
	if level > b.maxLevel {
		return false
	}

	if field.Resolve != nil {
		return false
	}

	fieldRootType := b.fieldTypeGetter.GetRootType(field.Type)
	for _, fType := range b.availableTypes {
		if fType == fieldRootType {
			return true
		}
	}

	return false
}

// Код оператора, для вставки в схему GraphQL
func (b betweenWhereParametersOperatorGeneratorProcessor) OperatorCode() string {
	return constants.BetweenSchemaKey
}

// Генерация параметров
func (b betweenWhereParametersOperatorGeneratorProcessor) Generate(
	ctx context.Context,
	field *graphql.FieldDefinition,
	level uint8,
	namePrefix string,
) *graphql.InputObjectFieldConfig {
	rType := b.fieldTypeGetter.GetRootType(field.Type)
	b.logger.WithFields(logrus.Fields{
		"code":       100,
		"field":      field.Name,
		"level":      level,
		"namePrefix": namePrefix,
	}).Debug(`Generated _IN operator for field`)

	return &graphql.InputObjectFieldConfig{
		Type: graphql.NewInputObject(graphql.InputObjectConfig{
			Name: fmt.Sprintf(`%v_between`, namePrefix),
			Fields: graphql.InputObjectConfigFieldMap{
				"_from": &graphql.InputObjectFieldConfig{
					Type:        rType,
					Description: "Параметр от",
				},
				"_to": &graphql.InputObjectFieldConfig{
					Type:        rType,
					Description: "Параметр до",
				},
			},
			Description: "",
		}),
		Description: "Операция сравнения значения в формате: 'между двух значений'",
	}
}

// Фабрика процессора
func newBetweenWhereParametersOperatorGeneratorProcessor(
	availableTypes []graphql.Type,
	maxLevel uint8,
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface,
) whereParametersOperatorGeneratorProcessorInterface {
	return &betweenWhereParametersOperatorGeneratorProcessor{
		availableTypes:  availableTypes,
		maxLevel:        maxLevel,
		logger:          helpers.NewLogger(`betweenWhereParametersOperatorGeneratorProcessor`),
		fieldTypeGetter: fieldTypeGetter,
	}
}
