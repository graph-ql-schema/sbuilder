package for_tests

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования генератора
type WhereParametersLogicOperationsGeneratorMock struct {
	IsCalled     bool
	ReturnResult *graphql.InputObjectConfigFieldMap
}

// Установка генератора параметров для процессора
func (w WhereParametersLogicOperationsGeneratorMock) SetWhereParametersOperatorGenerator(interfaces.WhereParametersGeneratorInterface) {
}

// Генерация параметров
func (w *WhereParametersLogicOperationsGeneratorMock) GenerateParametersForObject(context.Context, *graphql.Object, uint8, string) *graphql.InputObjectConfigFieldMap {
	w.IsCalled = true

	return w.ReturnResult
}
