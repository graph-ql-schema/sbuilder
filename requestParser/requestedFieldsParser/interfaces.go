package requestedFieldsParser

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Интерфейс сущности запрошенного поля
type GraphQlRequestedFieldInterface interface {
	// Получение названия поля
	GetFieldName() string

	// Есть ли у поля дочерние элементы
	HasSubFields() bool

	// Получение дочерних элементов поля
	GetSubFields() []GraphQlRequestedFieldInterface
}

// Срез запрошенных полей
type GraphQlRequestedFields = []GraphQlRequestedFieldInterface

// Тип, описывающий фабрику сущностей полей запроса
type TFieldFactory = func(fieldName string, subFields GraphQlRequestedFields) GraphQlRequestedFieldInterface

// Интерфейс парсера полей запроса
type RequestedFieldsParserInterface interface {
	// Парсинг запроса
	ParseRequest(
		ctx context.Context,
		params graphql.ResolveParams,
	) (GraphQlRequestedFields, error)
}
