package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Проверяет доступность процессора
func Test_equalsWhereParametersOperatorGeneratorProcessor_IsAvailable(t *testing.T) {
	type fields struct {
		maxLevel        uint8
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		field *graphql.FieldDefinition
		level uint8
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование доступности на простом поле с доступной вложенностью.",
			fields: fields{
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{},
				level: 1,
			},
			want: true,
		},
		{
			name: "Тестирование доступности на простом поле с превышенной вложенностью.",
			fields: fields{
				maxLevel:        1,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{},
				level: 100,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := equalsWhereParametersOperatorGeneratorProcessor{
				maxLevel:        tt.fields.maxLevel,
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}
			if got := e.IsAvailable(context.Background(), tt.args.field, tt.args.level); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения корректного кода оператора
func Test_equalsWhereParametersOperatorGeneratorProcessor_OperatorCode(t *testing.T) {
	type fields struct {
		maxLevel        uint8
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "Тестирование получения корректного кода оператора",
			fields: fields{},
			want:   constants.EqualsSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := equalsWhereParametersOperatorGeneratorProcessor{
				maxLevel:        tt.fields.maxLevel,
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}
			if got := e.OperatorCode(); got != tt.want {
				t.Errorf("OperatorCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генератора
func Test_equalsWhereParametersOperatorGeneratorProcessor_Generate(t *testing.T) {
	type fields struct {
		maxLevel        uint8
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		field      *graphql.FieldDefinition
		level      uint8
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   graphql.Type
	}{
		{
			name: "Тестирование сгенерированного типа на базовом типе: Int",
			fields: fields{
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.Int,
					Name: "Test",
				},
				level:      1,
				namePrefix: "test",
			},
			want: graphql.Int,
		},
		{
			name: "Тестирование сгенерированного типа на базовом типе: NotNull Bool",
			fields: fields{
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.NewNonNull(graphql.Boolean),
					Name: "Test",
				},
				level:      1,
				namePrefix: "test",
			},
			want: graphql.NewNonNull(graphql.Boolean),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := equalsWhereParametersOperatorGeneratorProcessor{
				maxLevel:        tt.fields.maxLevel,
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}
			if got := e.Generate(context.Background(), tt.args.field, tt.args.level, tt.args.namePrefix); !reflect.DeepEqual(got.Type, tt.want) {
				t.Errorf("Generate() = %v, want %v", got.Type, tt.want)
			}
		})
	}
}
