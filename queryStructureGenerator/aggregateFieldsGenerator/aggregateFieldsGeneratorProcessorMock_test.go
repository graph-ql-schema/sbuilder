package aggregateFieldsGenerator

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type aggregateFieldsGeneratorProcessorMock struct {
	ReturnField *graphql.Field
	ReturnCode  string
}

// Генерация поля для сущности
func (a aggregateFieldsGeneratorProcessorMock) GenerateField(context.Context, *graphql.Object) (*graphql.Field, string) {
	return a.ReturnField, a.ReturnCode
}
