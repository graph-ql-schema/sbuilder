package for_tests

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type AggregateFieldsGenerator struct {
	Result   *graphql.Object
	IsCalled bool
}

// Генерация сущности
func (a *AggregateFieldsGenerator) Generate(context.Context, *graphql.Object) *graphql.Object {
	a.IsCalled = true

	return a.Result
}
