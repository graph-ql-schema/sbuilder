package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"github.com/graphql-go/graphql"
)

// Тестирование получение названия
func Test_whereOrHavingQueryGeneratorProcessor_Name(t *testing.T) {
	type fields struct {
		logger         *logrus.Entry
		whereGenerator interfaces.WhereParametersGeneratorInterface
		name           string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получение названия",
			fields: fields{
				logger:         nil,
				whereGenerator: nil,
				name:           "test",
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := whereOrHavingQueryGeneratorProcessor{
				logger:         tt.fields.logger,
				whereGenerator: tt.fields.whereGenerator,
				name:           tt.fields.name,
			}
			if got := w.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генератора
func Test_whereOrHavingQueryGeneratorProcessor_Generate(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		logger         *logrus.Entry
		whereGenerator interfaces.WhereParametersGeneratorInterface
		name           string
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.ArgumentConfig
	}{
		{
			name: "Тестирование получение названия",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				whereGenerator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				name: "where",
			},
			args: args{
				object:     objectConfig,
				namePrefix: "role",
			},
			want: &graphql.ArgumentConfig{
				Type: graphql.NewInputObject(graphql.InputObjectConfig{
					Name:        "role_query_where_object",
					Fields:      graphql.InputObjectConfigFieldMap{},
					Description: `Role: Фильтр 'where' для листинга сущностей.`,
				}),
				Description: `Role: Фильтр 'where' для листинга сущностей.`,
			},
		},
		{
			name: "Тестирование получение названия",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				whereGenerator: &for_tests.WhereParametersGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				name: "having",
			},
			args: args{
				object:     objectConfig,
				namePrefix: "role",
			},
			want: &graphql.ArgumentConfig{
				Type: graphql.NewInputObject(graphql.InputObjectConfig{
					Name:        "role_query_having_object",
					Fields:      graphql.InputObjectConfigFieldMap{},
					Description: `Role: Фильтр 'having' для листинга сущностей.`,
				}),
				Description: `Role: Фильтр 'having' для листинга сущностей.`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := whereOrHavingQueryGeneratorProcessor{
				logger:         tt.fields.logger,
				whereGenerator: tt.fields.whereGenerator,
				name:           tt.fields.name,
			}
			if got := w.Generate(context.Background(), tt.args.object, tt.args.namePrefix); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
