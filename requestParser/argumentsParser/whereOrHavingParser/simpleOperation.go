package whereOrHavingParser

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"github.com/graphql-go/graphql"
)

// Простая операция фильтрации
type simpleOperation struct {
	operationType  string
	operationValue interface{}
	field          string
	sqlOperator    string
	valueConverter gql_sql_converter.GraphQlSqlConverterInterface
}

// Получение типа текущей операции
func (s simpleOperation) Type() string {
	return s.operationType
}

// Получение значения для текущей операции
func (s simpleOperation) Value() interface{} {
	return s.operationValue
}

// Получение кода поля для операции
func (s simpleOperation) Field() string {
	return s.field
}

// Конвертация в SQL
func (s simpleOperation) ToSQL(object *graphql.Object, fieldsMap map[string]string) (string, error) {
	fieldName := s.field
	if mappedField, ok := fieldsMap[fieldName]; ok {
		fieldName = mappedField
	}

	value, err := s.valueConverter.ToSQLValue(object, s.field, s.operationValue)
	if nil != err {
		return "", fmt.Errorf(`'%v' field: %v`, s.field, err.Error())
	}

	if value == "null" {
		return fmt.Sprintf(`%v is null`, fieldName), nil
	}

	return fmt.Sprintf(`%v %v %v`, fieldName, s.sqlOperator, value), nil
}

// Фабрика операции
type TNewSimpleOperation = func(string, interface{}, string, string) Operation

func NewSimpleOperation(
	operationType string,
	operationValue interface{},
	field string,
	sqlOperator string,
) Operation {
	return &simpleOperation{
		operationType:  operationType,
		operationValue: operationValue,
		field:          field,
		sqlOperator:    sqlOperator,
		valueConverter: gql_sql_converter.NewGraphQlSqlConverter(),
	}
}
