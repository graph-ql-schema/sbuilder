package sbuilder

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/contextParser"
	"github.com/graphql-go/graphql"
	"github.com/valyala/fasthttp"
)

// Данные запроса GraphQL
type graphQlData struct {
	Query         string                 `json:"query"`
	OperationName string                 `json:"operationName"`
	Variables     map[string]interface{} `json:"variables"`
}

// Сервер GraphQL API
type graphQlServer struct {
	configuration GraphQlServerConfig
	schema        graphql.Schema
	isStarted     bool
	isAlive       bool
	server        *fasthttp.Server
	middleware    []Middleware

	contextService contextParser.ContextParserInterface
	logger         *logrus.Entry
}

// Возвращает статус состояния запуска сервиса
func (g *graphQlServer) IsStarted() bool {
	return g.isStarted
}

// Возвращает статус сервиса: живой или нет
func (g *graphQlServer) IsAlive() bool {
	return g.isAlive
}

// Запуск сервера
func (g *graphQlServer) Run() error {
	server := fmt.Sprintf(`%v:%v`, g.configuration.Host, g.configuration.Port)

	g.logger.WithField("host", server).Info(`Started HTTP GraphQL server`)
	g.isStarted = true

	fastHttpServer := &fasthttp.Server{
		Handler: func(ctx *fasthttp.RequestCtx) {
			ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
			ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")
			ctx.Response.Header.Set("Content-Type", "application/json")

			if string(ctx.Method()) == "OPTIONS" {
				ctx.SetStatusCode(fasthttp.StatusOK)
				return
			}

			starts := time.Now().UnixNano() / int64(time.Millisecond)
			if g.configuration.Uri != string(ctx.Path()) || string(ctx.Method()) != "POST" {
				ctx.SetStatusCode(fasthttp.StatusNotFound)

				_ = json.NewEncoder(ctx).Encode(map[string]string{
					"error": "not found",
				})
				return
			}

			wCtx := g.contextService.Parse(ctx)

			var data graphQlData
			if err := json.NewDecoder(bytes.NewReader(ctx.Request.Body())).Decode(&data); err != nil {
				ctx.SetStatusCode(fasthttp.StatusBadRequest)

				_ = json.NewEncoder(ctx).Encode(map[string]string{
					"error": "Failed to parse request. Request should be in JSON format.",
				})

				return
			}

			var params = graphql.Params{
				Schema:         g.schema,
				RequestString:  data.Query,
				VariableValues: data.Variables,
				OperationName:  data.OperationName,
				Context:        wCtx,
			}

			next := graphql.Do
			for _, middleware := range g.middleware {
				next = middleware.Run(next)
			}

			result := next(params)
			ctx.SetStatusCode(fasthttp.StatusOK)

			for _, err := range result.Errors {
				g.logger.WithError(err.OriginalError()).Warning("Query execution error")
			}

			finish := time.Now().UnixNano() / int64(time.Millisecond)
			g.logRequest(finish-starts, map[string]interface{}{
				"request": data,
				"ctx":     ctx,
			})

			_ = json.NewEncoder(ctx).Encode(result)
		},
	}
	g.server = fastHttpServer
	err := fastHttpServer.ListenAndServe(server)

	g.isAlive = false
	return err
}

// Остановка сервера
func (g *graphQlServer) GracefulShutdown() {
	g.logger.WithFields(logrus.Fields{
		"code": 1001,
	}).Info(`Starting graceful shutdown`)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	errChan := make(chan error)
	defer close(errChan)

	go func() {
		errChan <- g.server.Shutdown()
	}()

	select {
	case <-errChan:
		break
	case <-ctx.Done():
		break
	}

	g.logger.WithFields(logrus.Fields{
		"code": 1002,
	}).Info(`Server graceful stopped`)
}

// Логирование обработанного запроса GraphQL
func (g *graphQlServer) logRequest(processTime int64, data map[string]interface{}) {
	g.logger.WithFields(logrus.Fields{
		"processTime": processTime,
	}).Info(`Processed GraphQL request`)

	g.logger.WithFields(logrus.Fields{
		"data": data,
	}).Debug(`Data for processed GraphQL request`)
}

// Фабрика сервера
func newGraphQlServer(
	configuration GraphQlServerConfig,
	schema graphql.Schema,
	middleware ...Middleware,
) GraphQlServerInterface {
	return &graphQlServer{
		configuration:  configuration,
		schema:         schema,
		isStarted:      false,
		isAlive:        true,
		server:         nil,
		middleware:     middleware,
		contextService: contextParser.NewContextParser(),
		logger:         helpers.NewLogger(`graphQlServer`),
	}
}
