package for_tests

import "github.com/graphql-go/graphql"

// Сервис получения корневого типа GraphQL по переданному.
type GraphQlRootTypeGetterMock struct {
	IsCalled        bool
	IsNotNullReturn bool
	IsListResult    bool
	IsNullResult    bool
}

// Проверяет, что переданный тип является Nullable
func (g *GraphQlRootTypeGetterMock) IsNullable(graphql.Type) bool {
	return g.IsNullResult
}

// Проверяет, что переданный тип является List
func (g *GraphQlRootTypeGetterMock) IsList(graphql.Type) bool {
	return g.IsListResult
}

// Проверяет, что переданный тип не является NotNull
func (g *GraphQlRootTypeGetterMock) IsNotNull(graphql.Type) bool {
	return g.IsNotNullReturn
}

// Получение корневого типа поля, которое обернут в NotNull или в List
func (g *GraphQlRootTypeGetterMock) GetRootType(gType graphql.Type) graphql.Type {
	g.IsCalled = true

	return gType
}
