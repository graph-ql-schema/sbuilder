package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/orderParametersGenerator"
	"context"
	"github.com/graphql-go/graphql"
)

// Процессор генерации параметров сортировки
type orderGeneratorProcessor struct {
	orderGenerator orderParametersGenerator.OrderParametersGeneratorInterface
}

// Получение названия генерируемого поля
func (o orderGeneratorProcessor) Name() string {
	return constants.OrderBySchemaKey
}

// Генерация
func (o orderGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
) *graphql.ArgumentConfig {
	return o.orderGenerator.Generate(ctx, object, namePrefix)
}

// Фабрика процессора
func newOrderGeneratorProcessor() queryGeneratorProcessorInterface {
	return &orderGeneratorProcessor{
		orderGenerator: orderParametersGenerator.OrderParametersGenerator(),
	}
}
