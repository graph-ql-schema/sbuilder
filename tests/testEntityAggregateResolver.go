package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strings"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/tests/pgsql"
)

type Aggregate = map[string]float32
type Variants = map[string][]interface{}

type TestAggregateRaw struct {
	Count    int             `db:"count" json:"count"`
	Avg      json.RawMessage `db:"avg" json:"avg"`
	Min      json.RawMessage `db:"min" json:"min"`
	Max      json.RawMessage `db:"max" json:"max"`
	Sum      json.RawMessage `db:"sum" json:"sum"`
	Variants json.RawMessage `db:"variants" json:"variants"`
}

type TestAggregate struct {
	Count    int       `db:"count" json:"count"`
	Avg      Aggregate `db:"avg" json:"avg"`
	Min      Aggregate `db:"min" json:"min"`
	Max      Aggregate `db:"max" json:"max"`
	Sum      Aggregate `db:"sum" json:"sum"`
	Variants Variants  `db:"variants" json:"variants"`
}

// Резолвер аггрегирующего запроса
func testEntityAggregateResolver(params sbuilder.Parameters) (i interface{}, err error) {
	requestedFieldsSql, err := buildRequestedFieldsForAggregate(params.Fields, map[string]string{
		"active_date": "active_from",
	})

	if nil != err {
		return nil, err
	}

	if 0 == len(requestedFieldsSql) {
		return []TestAggregateRaw{}, nil
	}

	q := fmt.Sprintf(`select %v from test_data.test`, requestedFieldsSql)

	if nil != params.Arguments.Where {
		sql, err := params.Arguments.Where.ToSQL(params.GraphQlObject, map[string]string{
			"active_date": "active_from",
		})
		if nil != err {
			return nil, err
		}

		q += fmt.Sprintf(` where %v`, sql)
	}

	if nil != params.Arguments.GroupBy {
		q += fmt.Sprintf(` group by %v`, strings.Join(params.Arguments.GroupBy, `,`))
	}

	if nil != params.Arguments.Having {
		sql, err := params.Arguments.Having.ToSQL(params.GraphQlObject, map[string]string{
			"active_date": "active_from",
		})
		if nil != err {
			return nil, err
		}

		q += fmt.Sprintf(` having %v`, sql)
	}

	if nil == params.Arguments.GroupBy && nil != params.Arguments.Having {
		return nil, fmt.Errorf(`having can be used only with group by`)
	}

	if sort := generateOrderBySql(
		params.Arguments.OrderBy,
		map[string]string{
			"active_date": "active_from",
		},
	); 0 != len(sort) {
		q += " order by " + sort
	}

	if nil != params.Arguments.Pagination {
		q += fmt.Sprintf(` limit %v offset %v`, params.Arguments.Pagination.Limit, params.Arguments.Pagination.Offset)
	}

	log.Println(q)

	data := []TestAggregateRaw{}
	err = pgsql.Connection.Select(&data, q)
	if err != nil {
		return nil, err
	}

	parsedData := []TestAggregate{}
	for _, originData := range data {
		var avg Aggregate
		_ = json.Unmarshal(originData.Avg, &avg)

		var min Aggregate
		_ = json.Unmarshal(originData.Min, &min)

		var max Aggregate
		_ = json.Unmarshal(originData.Max, &max)

		var sum Aggregate
		_ = json.Unmarshal(originData.Sum, &sum)

		var variants Variants
		_ = json.Unmarshal(originData.Variants, &variants)

		parsedData = append(parsedData, TestAggregate{
			Count:    originData.Count,
			Avg:      avg,
			Min:      min,
			Max:      max,
			Sum:      sum,
			Variants: variants,
		})
	}

	return parsedData, nil
}
