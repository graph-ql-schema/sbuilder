package sbuilder

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"context"
	"fmt"
	"github.com/functionalfoundry/graphqlws"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
)

// Диспетчер уведомлений для WS сервера
type wsDispatcher struct {
	subscriptionManager graphqlws.SubscriptionManager
	schema              graphql.Schema
	logger              *logrus.Entry
}

// Получение текущего менеджера подписок на события
func (w wsDispatcher) GetManager() graphqlws.SubscriptionManager {
	return w.subscriptionManager
}

// Отправка события всем подписчикам
// При отправке данных будет отправлена соответствующая нотификация
// всем зарегистрированным пользователям на сервере WS
func (w wsDispatcher) DispatchEvent(
	ctx context.Context,
	object *graphql.Object,
	eventType types.TEventType,
	entityId string,
	entityData interface{},
) error {
	if nil == ctx {
		ctx = context.Background()
	}

	if nil == w.subscriptionManager {
		if nil != w.logger {
			w.logger.Error(`You should setup correct subscription manager to service`)
		}

		return fmt.Errorf(`you should setup correct subscription manager to service`)
	}

	for connection := range w.subscriptionManager.Subscriptions() {
		for _, subscription := range w.subscriptionManager.Subscriptions()[connection] {
			subscriptionCtx := context.WithValue(ctx, "object", object)
			subscriptionCtx = context.WithValue(subscriptionCtx, "eventType", eventType)
			subscriptionCtx = context.WithValue(subscriptionCtx, "entityId", entityId)
			subscriptionCtx = context.WithValue(subscriptionCtx, "entityData", entityData)
			subscriptionCtx = context.WithValue(subscriptionCtx, "connection", connection)

			params := graphql.Params{
				Schema:         w.schema,
				RequestString:  subscription.Query,
				VariableValues: subscription.Variables,
				OperationName:  subscription.OperationName,
				Context:        subscriptionCtx,
			}
			result := graphql.Do(params)

			isNotNilResult := false
			if data, ok := result.Data.(map[string]interface{}); ok {
				for _, res := range data {
					if nil != res {
						isNotNilResult = true
						break
					}
				}
			}

			if !isNotNilResult && !result.HasErrors() {
				continue
			}

			subscription.SendData(&graphqlws.DataMessagePayload{
				Data:   result.Data,
				Errors: graphqlws.ErrorsFromGraphQLErrors(result.Errors),
			})
		}
	}

	return nil
}

// Фабрика сервиса доставки уведомлений
func newWsDispatcher(
	subscriptionManager graphqlws.SubscriptionManager,
	schema graphql.Schema,
) WsDispatcherInterface {
	return &wsDispatcher{
		subscriptionManager: subscriptionManager,
		schema:              schema,
		logger:              helpers.NewLogger("WSDispatcher"),
	}
}
