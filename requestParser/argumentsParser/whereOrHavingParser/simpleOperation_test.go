package whereOrHavingParser

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	for_tests2 "bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование получения типа операции
func Test_simpleOperation_Type(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue interface{}
		field          string
		sqlOperator    string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения типа операции",
			fields: fields{
				operationType:  "test",
				operationValue: nil,
				field:          "",
				sqlOperator:    "",
				valueConverter: nil,
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				sqlOperator:    tt.fields.sqlOperator,
				valueConverter: tt.fields.valueConverter,
			}
			if got := s.Type(); got != tt.want {
				t.Errorf("Type() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения значения
func Test_simpleOperation_Value(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue interface{}
		field          string
		sqlOperator    string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   interface{}
	}{
		{
			name: "Тестирование получения значения",
			fields: fields{
				operationType:  "",
				operationValue: 1,
				field:          "",
				sqlOperator:    "",
				valueConverter: nil,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				sqlOperator:    tt.fields.sqlOperator,
				valueConverter: tt.fields.valueConverter,
			}
			if got := s.Value(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тетсирование получения кода поля
func Test_simpleOperation_Field(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue interface{}
		field          string
		sqlOperator    string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тетсирование получения кода поля",
			fields: fields{
				operationType:  "",
				operationValue: nil,
				field:          "test",
				sqlOperator:    "",
				valueConverter: nil,
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				sqlOperator:    tt.fields.sqlOperator,
				valueConverter: tt.fields.valueConverter,
			}
			if got := s.Field(); got != tt.want {
				t.Errorf("Field() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации SQL
func Test_simpleOperation_ToSQL(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue interface{}
		field          string
		sqlOperator    string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	type args struct {
		object    *graphql.Object
		fieldsMap map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование с возвращением ошибки от конвертера значения",
			fields: fields{
				operationType:  "test",
				operationValue: 10,
				field:          "test",
				sqlOperator:    "=",
				valueConverter: &for_tests2.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: true,
					ToSQLValueRes: "",
				},
			},
			args: args{
				object:    nil,
				fieldsMap: nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование без ошибки от конвертера значения",
			fields: fields{
				operationType:  "test",
				operationValue: 10,
				field:          "test",
				sqlOperator:    "=",
				valueConverter: &for_tests2.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: false,
					ToSQLValueRes: "10",
				},
			},
			args: args{
				object:    nil,
				fieldsMap: nil,
			},
			want:    "test = 10",
			wantErr: false,
		},
		{
			name: "Тестирование с подменой кода поля",
			fields: fields{
				operationType:  "test",
				operationValue: 10,
				field:          "test",
				sqlOperator:    "=",
				valueConverter: &for_tests2.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: false,
					ToSQLValueRes: "10",
				},
			},
			args: args{
				object: nil,
				fieldsMap: map[string]string{
					"test": "mapped",
				},
			},
			want:    "mapped = 10",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				sqlOperator:    tt.fields.sqlOperator,
				valueConverter: tt.fields.valueConverter,
			}
			got, err := s.ToSQL(tt.args.object, tt.args.fieldsMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToSQL() got = %v, want %v", got, tt.want)
			}
		})
	}
}
