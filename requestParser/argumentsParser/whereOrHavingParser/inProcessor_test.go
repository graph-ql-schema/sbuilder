package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

var inOperationMockFactory TNewInOperation = func(
	operationType string,
	operationValue []interface{},
	field string,
) Operation {
	values := []string{}
	for _, val := range operationValue {
		values = append(values, fmt.Sprintf(`%v`, val))
	}

	return &OperationStub{
		operationType: operationType,
		value:         operationValue,
		field:         field,
		sql:           fmt.Sprintf(`%v in [%v]`, field, strings.Join(values, ",")),
		sqlErr:        false,
	}
}

// Проверка доступности процессора
func Test_inProcessor_isAvailable(t *testing.T) {
	type fields struct {
		logger  *logrus.Entry
		factory TNewInOperation
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: inOperationMockFactory,
			},
			args: args{
				arguments: nil,
			},
			want: false,
		},
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: inOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": []interface{}{
						1,
						2,
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование на корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: inOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.InSchemaKey: []interface{}{
							1,
							2,
						},
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inProcessor{
				logger:  tt.fields.logger,
				factory: tt.fields.factory,
			}
			if got := i.IsAvailable(context.Background(), tt.args.arguments); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга
func Test_inProcessor_parse(t *testing.T) {
	type fields struct {
		logger  *logrus.Entry
		factory TNewInOperation
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantOperation Operation
		wantErr       bool
	}{
		{
			name: "Тестирование на корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: inOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.InSchemaKey: []interface{}{
							1,
							2,
						},
					},
				},
			},
			wantOperation: &OperationStub{
				operationType: constants.InSchemaKey,
				value: []interface{}{
					1,
					2,
				},
				field:  "test",
				sql:    `test in [1,2]`,
				sqlErr: false,
			},
			wantErr: false,
		},
		{
			name: "Тестирование с не корректным оператором",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: inOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						"_test": []interface{}{
							1,
							2,
						},
					},
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: inOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			wantOperation: nil,
			wantErr:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inProcessor{
				logger:  tt.fields.logger,
				factory: tt.fields.factory,
			}
			gotOperation, err := i.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOperation, tt.wantOperation) {
				t.Errorf("parse() gotOperation = %v, want %v", gotOperation, tt.wantOperation)
			}
		})
	}
}
