package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/queryGenerator/returningResolver"
	"github.com/graphql-go/graphql"
)

// Тип, описывающий генератор описания для объекта
type tDescriptionGenerator = func(object *graphql.Object) string

// Генератор мутации для вставки сущности
type insertOrUpdateMutationGenerator struct {
	logger               *logrus.Entry
	typeGetter           gql_root_type_getter.GraphQlRootTypeGetterInterface
	processors           []queryGeneratorProcessorInterface
	aliasGenerator       aliasGenerationService.AliasGenerationServiceInterface
	operationType        string
	descriptionGenerator tDescriptionGenerator
	suffix               constants.QueryType
	resolverFactory      returningResolver.TReturningResolverFactory
	customizations 		 []customizationService.CustomizationInterface
	resolverFactoryWithCustomizations returningResolver.TReturningResolverFactoryWithCustomizations
}

// Получение суффикса для названия мутации
func (i insertOrUpdateMutationGenerator) Suffix() constants.QueryType {
	return i.suffix
}

// Генерация мутации сущности
func (i insertOrUpdateMutationGenerator) Generate(
	ctx context.Context,
	object *graphql.Object,
	resolver TResolver,
) *graphql.Field {
	i.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(fmt.Sprintf(`Generation %v mutation for entity '%v'`, i.operationType, object.Name()))

	name := fmt.Sprintf(`%v_%v`, i.aliasGenerator.Generate(object.Name()), i.operationType)
	arguments := graphql.FieldConfigArgument{}
	for _, processor := range i.processors {
		res := processor.Generate(ctx, object, name)
		if nil == res {
			continue
		}

		arguments[processor.Name()] = res
	}

	if 0 == len(arguments) {
		return nil
	}

	return &graphql.Field{
		Type:        i.generateResponse(object, name),
		Name:        fmt.Sprintf(`%v_mutation`, name),
		Description: i.descriptionGenerator(object),
		Args:        arguments,
		Resolve:     resolver,
	}
}

// Генератор объекта результата обработки запроса
func (i insertOrUpdateMutationGenerator) generateResponse(object *graphql.Object, namePrefix string) *graphql.Object {
	resolve := i.resolverFactory(object)
	if nil != i.customizations {
		resolve = i.resolverFactoryWithCustomizations(object, i.customizations)
	}

	return graphql.NewObject(
		graphql.ObjectConfig{
			Name: fmt.Sprintf(`%v_result_type`, namePrefix),
			Fields: graphql.Fields{
				"affected_rows": &graphql.Field{
					Type:        graphql.NewNonNull(graphql.Int),
					Name:        fmt.Sprintf(`%v_affected`, namePrefix),
					Description: "Количество записей, обработанных запросом",
				},
				"returning": &graphql.Field{
					Type:        graphql.NewList(object),
					Name:        fmt.Sprintf(`%v_rows`, namePrefix),
					Description: "Массив объектов, обработанных в ходе выполнения запроса",
					Resolve:     resolve,
				},
			},
			Description: fmt.Sprintf(`Результат обработки значений для сущности '%v'`, object.Name()),
		},
	)
}

// Фабрика генератора мутаций вставки
func InsertMutationGenerator() QueryGeneratorInterface {
	return &insertOrUpdateMutationGenerator{
		logger:     helpers.NewLogger(`insertOrUpdateMutationGenerator (Operation: insert)`),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		processors: []queryGeneratorProcessorInterface{
			newObjectInsertGeneratorProcessor(),
		},
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
		operationType:  "insert",
		descriptionGenerator: func(object *graphql.Object) string {
			return fmt.Sprintf("Мутация вставки значений объекта '%v'", object.Name())
		},
		suffix:          constants.InsertMutation,
		resolverFactory: returningResolver.ReturningResolverFactory,
	}
}

// Фабрика генератора мутаций вставки с кастомизаторами
func InsertMutationGeneratorWithCustomizations(customizations []customizationService.CustomizationInterface) QueryGeneratorInterface {
	return &insertOrUpdateMutationGenerator{
		logger:     helpers.NewLogger(`insertOrUpdateMutationGenerator (Operation: insert)`),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		processors: []queryGeneratorProcessorInterface{
			newObjectInsertGeneratorProcessor(),
		},
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
		operationType:  "insert",
		descriptionGenerator: func(object *graphql.Object) string {
			return fmt.Sprintf("Мутация вставки значений объекта '%v'", object.Name())
		},
		suffix:          constants.InsertMutation,
		resolverFactory: returningResolver.ReturningResolverFactory,
		customizations:  customizations,
		resolverFactoryWithCustomizations: returningResolver.ReturningResolverFactoryWithCustomizations,
	}
}

// Фабрика генератора мутации обновления
func UpdateMutationGenerator() QueryGeneratorInterface {
	return &insertOrUpdateMutationGenerator{
		logger:     helpers.NewLogger(`insertOrUpdateMutationGenerator (Operation: update)`),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		processors: []queryGeneratorProcessorInterface{
			newSetObjectGeneratorProcessor(),
			newWhereOrHavingQueryGeneratorProcessor(constants.WhereSchemaKey),
		},
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
		operationType:  "update",
		descriptionGenerator: func(object *graphql.Object) string {
			return fmt.Sprintf("Мутация обновления значений объекта '%v'", object.Name())
		},
		suffix:          constants.UpdateMutation,
		resolverFactory: returningResolver.ReturningResolverFactory,
	}
}

// Фабрика генератора мутации обновления
func UpdateMutationGeneratorWithCustomizations(customizations []customizationService.CustomizationInterface) QueryGeneratorInterface {
	return &insertOrUpdateMutationGenerator{
		logger:     helpers.NewLogger(`insertOrUpdateMutationGenerator (Operation: update)`),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		processors: []queryGeneratorProcessorInterface{
			newSetObjectGeneratorProcessor(),
			newWhereOrHavingQueryGeneratorProcessor(constants.WhereSchemaKey),
		},
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
		operationType:  "update",
		descriptionGenerator: func(object *graphql.Object) string {
			return fmt.Sprintf("Мутация обновления значений объекта '%v'", object.Name())
		},
		suffix:          constants.UpdateMutation,
		resolverFactory: returningResolver.ReturningResolverFactory,
		customizations:  customizations,
		resolverFactoryWithCustomizations: returningResolver.ReturningResolverFactoryWithCustomizations,
	}
}
