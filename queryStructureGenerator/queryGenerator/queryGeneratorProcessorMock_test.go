package queryGenerator

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type queryGeneratorProcessorMock struct {
	name         string
	isCalled     bool
	returnResult *graphql.ArgumentConfig
}

// Получение названия генерируемого поля
func (a queryGeneratorProcessorMock) Name() string {
	return a.name
}

// Генерация
func (a *queryGeneratorProcessorMock) Generate(context.Context, *graphql.Object, string) *graphql.ArgumentConfig {
	a.isCalled = true

	return a.returnResult
}
