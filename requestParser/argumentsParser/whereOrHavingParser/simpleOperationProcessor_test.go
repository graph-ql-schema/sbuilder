package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

var simpleOperationMockFactory TNewSimpleOperation = func(
	operationType string,
	operationValue interface{},
	field string,
	sqlOperator string,
) Operation {
	return &OperationStub{
		operationType: operationType,
		value:         operationValue,
		field:         field,
		sql:           fmt.Sprintf(`%v %v %v`, field, sqlOperator, operationValue),
		sqlErr:        false,
	}
}

// Проверка доступности процессора
func Test_simpleOperationProcessor_isAvailable(t *testing.T) {
	type fields struct {
		logger  *logrus.Entry
		factory TNewSimpleOperation
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: simpleOperationMockFactory,
			},
			args: args{
				arguments: nil,
			},
			want: false,
		},
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: simpleOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": 1,
				},
			},
			want: false,
		},
		{
			name: "Тестирование на корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: simpleOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.EqualsSchemaKey: 1,
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperationProcessor{
				logger:  tt.fields.logger,
				factory: tt.fields.factory,
			}
			if got := s.IsAvailable(context.Background(), tt.args.arguments); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга
func Test_simpleOperationProcessor_parse(t *testing.T) {
	type fields struct {
		logger  *logrus.Entry
		factory TNewSimpleOperation
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantOperation Operation
		wantErr       bool
	}{
		{
			name: "Тестирование на корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: simpleOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						constants.EqualsSchemaKey: 1,
					},
				},
			},
			wantOperation: &OperationStub{
				operationType: constants.EqualsSchemaKey,
				value:         1,
				field:         "test",
				sql:           `test = 1`,
				sqlErr:        false,
			},
			wantErr: false,
		},
		{
			name: "Тестирование с не известным оператором",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: simpleOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{
					"test": map[string]interface{}{
						"_test": 1,
					},
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "Тестирование на не корректной структуре",
			fields: fields{
				logger:  helpers.NewLogger(`test`),
				factory: simpleOperationMockFactory,
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			wantOperation: nil,
			wantErr:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperationProcessor{
				logger:  tt.fields.logger,
				factory: tt.fields.factory,
			}
			gotOperation, err := s.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOperation, tt.wantOperation) {
				t.Errorf("parse() gotOperation = %v, want %v", gotOperation, tt.wantOperation)
			}
		})
	}
}
