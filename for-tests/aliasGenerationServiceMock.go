package for_tests

// Заглушка для тестирования генератора
type AliasGenerationServiceMock struct{}

// Генерация алиса
func (a AliasGenerationServiceMock) Generate(s string) string {
	return s
}
