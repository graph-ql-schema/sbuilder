package whereParametersOperatorGenerator

import "github.com/graphql-go/graphql"

var typesForMultipleValueOperations []graphql.Type
var typesForInOperation []graphql.Type

func init() {
	typesForMultipleValueOperations = []graphql.Type{
		graphql.Int,
		graphql.Float,
		graphql.DateTime,
	}

	typesForInOperation = []graphql.Type{
		graphql.Int,
		graphql.ID,
		graphql.Float,
		graphql.DateTime,
		graphql.String,
	}
}
