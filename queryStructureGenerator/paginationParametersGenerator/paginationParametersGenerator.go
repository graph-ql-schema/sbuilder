package paginationParametersGenerator

import (
	"github.com/graphql-go/graphql"
)

// Генератор параметров пагинации
type paginationParametersGenerator struct{}

// Генерация параметров "Limit"
func (p paginationParametersGenerator) GenerateLimit() *graphql.ArgumentConfig {
	return &graphql.ArgumentConfig{
		Type:         graphql.Int,
		DefaultValue: 30,
		Description:  "Параметр пагинации: Количество элементов в выдаче.",
	}
}

// Генерация параметров "Offset"
func (p paginationParametersGenerator) GenerateOffset() *graphql.ArgumentConfig {
	return &graphql.ArgumentConfig{
		Type:         graphql.Int,
		DefaultValue: 0,
		Description:  "Параметр пагинации: Смещение для элементов в выдаче.",
	}
}

// Фабрика генератора
func PaginationParametersGenerator() PaginationParametersGeneratorInterface {
	return &paginationParametersGenerator{}
}
