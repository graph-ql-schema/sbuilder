package for_tests

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type GraphQlSqlConverterMock struct {
	ToBaseTypeErr bool
	ToBaseTypeRes interface{}
	ToSQLValueErr bool
	ToSQLValueRes string
}

// Конвертация в базовый тип, например в строку или число
func (g GraphQlSqlConverterMock) ToBaseType(*graphql.Object, string, interface{}) (interface{}, error) {
	if g.ToBaseTypeErr {
		return nil, fmt.Errorf(`test`)
	}

	return g.ToBaseTypeRes, nil
}

// Конвертация в SQL like значение
func (g GraphQlSqlConverterMock) ToSQLValue(*graphql.Object, string, interface{}) (string, error) {
	if g.ToSQLValueErr {
		return "", fmt.Errorf(`test`)
	}

	return g.ToSQLValueRes, nil
}
