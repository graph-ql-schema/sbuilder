package paginationParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования
type PaginationParserMock struct {
	Result *Pagination
	IsErr  bool
}

// Парсинг параметров пагенации
func (p PaginationParserMock) Parse(
	context.Context,
	map[string]interface{},
) (pagination *Pagination, err error) {
	if p.IsErr {
		return nil, fmt.Errorf(`test`)
	}

	return p.Result, nil
}
