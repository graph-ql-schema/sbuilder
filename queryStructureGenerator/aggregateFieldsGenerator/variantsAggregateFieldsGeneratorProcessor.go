package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/nullable"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aggregateFieldsGenerator/subFieldsResolver"
	"github.com/graphql-go/graphql"
)

// Генератор поля вариантов для аггрегирующей сущности
type variantsAggregateFieldsGeneratorProcessor struct {
	logger                *logrus.Entry
	fieldTypeGetter       gql_root_type_getter.GraphQlRootTypeGetterInterface
	fieldsResolverFactory subFieldsResolver.TSubFieldsResolverFactory
}

// Генерация поля для сущности
func (v variantsAggregateFieldsGeneratorProcessor) GenerateField(ctx context.Context, object *graphql.Object) (*graphql.Field, string) {
	fields := v.getAvailableFields(object)
	if 0 == len(fields) {
		return nil, ""
	}

	v.logger.WithFields(logrus.Fields{
		"code":   100,
		"field":  constants.VariantsOperationSchemaKey,
		"object": object.Name(),
	}).Debug(fmt.Sprintf(`Generation aggregation field '%v'`, constants.VariantsOperationSchemaKey))

	namePrefix := fmt.Sprintf(`%v_aggregate_variants_field`, strings.ReplaceAll(strings.ToLower(object.Name()), ` `, `_`))

	fieldsConf := graphql.Fields{}
	for code, field := range fields {
		if nil != field.Resolve {
			continue
		}

		fType := v.fieldTypeGetter.GetRootType(field.Type)
		if scalar, ok := fType.(*graphql.Scalar); ok && v.fieldTypeGetter.IsNullable(field.Type) {
			fType = nullable.NewNullable(scalar)
		}

		fieldsConf[code] = &graphql.Field{
			Type:        graphql.NewList(fType),
			Name:        fmt.Sprintf(`%v_value_%v`, namePrefix, code),
			Description: fmt.Sprintf(`Получение списка уникальных значений для поля %v`, code),
			Resolve:     v.fieldsResolverFactory(code, fType),
		}
	}

	return &graphql.Field{
		Type: graphql.NewNonNull(graphql.NewObject(
			graphql.ObjectConfig{
				Name:        fmt.Sprintf(`%v_response_type`, namePrefix),
				Fields:      fieldsConf,
				Description: fmt.Sprintf(`Доступный для выбора список полей для получения уникальных значений по ним для сущности '%v'`, object.Name()),
			},
		)),
		Name:        fmt.Sprintf(`%v_type`, namePrefix),
		Description: fmt.Sprintf(`Получение списка уникальных значений для полей сущности '%v'`, object.Name()),
	}, constants.VariantsOperationSchemaKey
}

// Получение доступных для генерации полей
func (v variantsAggregateFieldsGeneratorProcessor) getAvailableFields(object *graphql.Object) map[string]*graphql.FieldDefinition {
	result := map[string]*graphql.FieldDefinition{}
	for code, field := range object.Fields() {
		fType := v.fieldTypeGetter.GetRootType(field.Type)
		if _, ok := fType.(*graphql.Object); ok {
			continue
		}

		result[code] = field
	}

	return result
}

func newVariantsAggregateFieldsGeneratorProcessor() aggregateFieldsGeneratorProcessorInterface {
	return &variantsAggregateFieldsGeneratorProcessor{
		logger:                helpers.NewLogger(`variantsAggregateFieldsGeneratorProcessor`),
		fieldTypeGetter:       gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		fieldsResolverFactory: subFieldsResolver.SubFieldsResolverFactory,
	}
}
