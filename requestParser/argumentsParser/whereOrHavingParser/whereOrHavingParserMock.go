package whereOrHavingParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования парсера фильтра
type WhereOrHavingParserMock struct {
	OperationResult Operation
	ErrorResult     bool
	IsCalled        bool
}

// Парсинг параметров фильтрации
func (w *WhereOrHavingParserMock) Parse(
	context.Context,
	map[string]interface{},
) (operation Operation, err error) {
	w.IsCalled = true

	if w.ErrorResult {
		return nil, fmt.Errorf(`test`)
	}

	return w.OperationResult, nil
}
