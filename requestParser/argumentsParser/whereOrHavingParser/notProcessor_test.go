package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Тестирование установки парсера
func Test_notProcessor_setParser(t *testing.T) {
	type fields struct {
		logger *logrus.Entry
		parser WhereOrHavingParserInterface
	}
	type args struct {
		parser WhereOrHavingParserInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "Тестирование установки парсера",
			fields: fields{},
			args: args{
				parser: &WhereOrHavingParserMock{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &notProcessor{
				logger: tt.fields.logger,
				parser: tt.fields.parser,
			}

			n.SetParser(tt.args.parser)
			if n.parser != tt.args.parser {
				t.Errorf("setParser() Parser is not set correctly")
			}
		})
	}
}

// Тестирование доступности процессора
func Test_notProcessor_isAvailable(t *testing.T) {
	type fields struct {
		logger *logrus.Entry
		parser WhereOrHavingParserInterface
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на пустой коллекции",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			want: false,
		},
		{
			name: "Тестирование на коллекции c фильтром",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{},
			},
			args: args{
				arguments: map[string]interface{}{
					"_not": map[string]interface{}{},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &notProcessor{
				logger: tt.fields.logger,
				parser: tt.fields.parser,
			}
			if got := n.IsAvailable(context.Background(), tt.args.arguments); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга процессора
func Test_notProcessor_parse(t *testing.T) {
	type fields struct {
		logger *logrus.Entry
		parser WhereOrHavingParserInterface
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantOperation Operation
		wantErr       bool
	}{
		{
			name: "Тестирование с не корректным форматом",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     false,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.NotSchemaKey: 1,
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "Тестирование без возврата корректного значения",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     false,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.NotSchemaKey: map[string]interface{}{
						"test": 0,
					},
				},
			},
			wantOperation: nil,
			wantErr:       false,
		},
		{
			name: "Тестирование c возвратом корректного значения",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: &OperationStub{},
					ErrorResult:     false,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.NotSchemaKey: map[string]interface{}{
						"test": 0,
					},
				},
			},
			wantOperation: &notOperation{
				value: &OperationStub{},
			},
			wantErr: false,
		},
		{
			name: "Тестирование c возвратом ошбики",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     true,
					IsCalled:        false,
				},
			},
			args: args{
				arguments: map[string]interface{}{
					constants.NotSchemaKey: map[string]interface{}{
						"test":   0,
						"test-2": 0,
					},
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &notProcessor{
				logger: tt.fields.logger,
				parser: tt.fields.parser,
			}
			gotOperation, err := n.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOperation, tt.wantOperation) {
				t.Errorf("parse() gotOperation = %v, want %v", gotOperation, tt.wantOperation)
			}
		})
	}
}
