package paginationParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
)

// Парсер параметров пагенации
type paginationParser struct {
	logger *logrus.Entry
}

// Парсинг параметров пагенации
func (p paginationParser) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (pagination *Pagination, err error) {
	result := &Pagination{}
	p.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started pagination parameters parsing")

	limit, limitOk := arguments[`limit`]
	if limitOk {
		limitVal, ok := limit.(int)
		if !ok {
			err := fmt.Errorf(`failed to parse limit parameter, type is not 'Int'`)
			p.logger.WithFields(logrus.Fields{
				"code": 400,
			}).Warning(err.Error())

			return nil, err
		}

		if limitVal < 0 {
			err := fmt.Errorf(`failed to parse limit parameter, value less that 0`)
			p.logger.WithFields(logrus.Fields{
				"code": 400,
			}).Warning(err.Error())

			return nil, err
		}

		result.Limit = uint64(limitVal)
	}

	offset, offsetOk := arguments[`offset`]
	if offsetOk {
		offsetVal, ok := offset.(int)
		if !ok {
			err := fmt.Errorf(`failed to parse offset parameter, type is not 'Int'`)
			p.logger.WithFields(logrus.Fields{
				"code": 400,
			}).Warning(err.Error())

			return nil, err
		}

		if offsetVal < 0 {
			err := fmt.Errorf(`failed to parse offset parameter, value less that 0`)
			p.logger.WithFields(logrus.Fields{
				"code": 400,
			}).Warning(err.Error())

			return nil, err
		}

		result.Offset = uint64(offsetVal)
	}

	p.logger.WithFields(logrus.Fields{
		"code":   100,
		"result": result,
	}).Debug("Parsed pagination parameters")

	if offsetOk && limitOk {
		return result, nil
	}

	return nil, nil
}

// Фабрика парсера параметров пагенации
func NewPaginationParser() PaginationParserInterface {
	return &paginationParser{
		logger: helpers.NewLogger(`paginationParser`),
	}
}
