package whereOrHavingParser

import (
	"context"
	"reflect"
	"testing"
)

// Тестирование парсинга
func Test_whereOrHavingParser_Parse(t *testing.T) {
	type fields struct {
		processors []whereOrHavingParserProcessorInterface
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantOperation Operation
		wantErr       bool
	}{
		{
			name: "Без доступных процессоров",
			fields: fields{
				processors: []whereOrHavingParserProcessorInterface{
					&whereOrHavingParserProcessorMock{
						isAvailable: false,
					},
				},
			},
			args:          args{},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "С доступным процессором, но без результата",
			fields: fields{
				processors: []whereOrHavingParserProcessorInterface{
					&whereOrHavingParserProcessorMock{
						isAvailable:  true,
						IsParseError: false,
						ParseResult:  nil,
					},
				},
			},
			args:          args{},
			wantOperation: nil,
			wantErr:       false,
		},
		{
			name: "С доступным процессором, но с ошибкой обработки",
			fields: fields{
				processors: []whereOrHavingParserProcessorInterface{
					&whereOrHavingParserProcessorMock{
						isAvailable:  true,
						IsParseError: true,
						ParseResult:  nil,
					},
				},
			},
			args:          args{},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "С доступным процессором и корректным результатом",
			fields: fields{
				processors: []whereOrHavingParserProcessorInterface{
					&whereOrHavingParserProcessorMock{
						isAvailable:  true,
						IsParseError: false,
						ParseResult:  &OperationStub{},
					},
				},
			},
			args:          args{},
			wantOperation: &OperationStub{},
			wantErr:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &whereOrHavingParser{
				processors: tt.fields.processors,
			}
			gotOperation, err := w.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOperation, tt.wantOperation) {
				t.Errorf("Parse() gotOperation = %v, want %v", gotOperation, tt.wantOperation)
			}
		})
	}
}

// Тестирование установки варсера в процессоры
func Test_whereOrHavingParser_setParserToProcessors(t *testing.T) {
	type fields struct {
		processors []whereOrHavingParserProcessorInterface
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "Тестирование установки варсера в процессоры",
			fields: fields{
				processors: []whereOrHavingParserProcessorInterface{
					&whereOrHavingParserProcessorMock{},
					&whereOrHavingParserProcessorMock{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &whereOrHavingParser{
				processors: tt.fields.processors,
			}

			w.setParserToProcessors()

			for i, processor := range tt.fields.processors {
				typedProcessor, _ := processor.(*whereOrHavingParserProcessorMock)
				if typedProcessor.parser != w {
					t.Errorf("setParserToProcessors() processor(%v), parser is not set", i)
				}
			}
		})
	}
}
