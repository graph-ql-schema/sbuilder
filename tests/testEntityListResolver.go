package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"time"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/tests/pgsql"
)

type TestData struct {
	Id         int64     `db:"id" json:"id"`
	ActiveDate time.Time `db:"active_from" json:"active_date"`
	Name       string    `db:"name" json:"name"`
	Stat       *int      `db:"stat" json:"stat"`
	IsActive   bool      `db:"is_active" json:"is_active"`
}

// Резолвер листинга сущностей
func testEntityListResolver(params sbuilder.Parameters) (i interface{}, err error) {
	sql := ""
	if nil != params.Arguments.Where {
		sql, err = params.Arguments.Where.ToSQL(params.GraphQlObject, map[string]string{
			"active_date": "active_from",
		})

		if nil != err {
			return nil, err
		}
	}

	q := `SELECT * FROM test_data.test`
	if 0 != len(sql) {
		q += ` where ` + sql
	}

	if sort := generateOrderBySql(
		params.Arguments.OrderBy,
		map[string]string{
			"active_date": "active_from",
		},
	); 0 != len(sort) {
		q += " order by " + sort
	}

	if nil != params.Arguments.Pagination {
		q += fmt.Sprintf(` limit %v offset %v`, params.Arguments.Pagination.Limit, params.Arguments.Pagination.Offset)
	}

	log.Println(q)

	data := []TestData{}
	err = pgsql.Connection.Select(&data, q)

	if nil != err {
		log.Println(err.Error())
		return []TestData{}, nil
	}

	return data, nil
}
