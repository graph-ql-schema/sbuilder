package paginationParser

import (
	"context"
)

// Результат парсинга параметров пагенации
type Pagination struct {
	Limit  uint64
	Offset uint64
}

// Парсер параметров пагенации
type PaginationParserInterface interface {
	// Парсинг параметров пагенации
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (pagination *Pagination, err error)
}
