package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/paginationParametersGenerator"
	"context"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
)

// Процессор генерации параметров Limit
type limitQueryGeneratorProcessor struct {
	logger              *logrus.Entry
	paginationGenerator paginationParametersGenerator.PaginationParametersGeneratorInterface
}

// Получение названия генерируемого поля
func (l limitQueryGeneratorProcessor) Name() string {
	return constants.LimitSchemaKey
}

// Генерация
func (l limitQueryGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
) *graphql.ArgumentConfig {
	l.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(`Generation parameters for 'limit' parameter`)

	return l.paginationGenerator.GenerateLimit()
}

func newLimitQueryGeneratorProcessor() queryGeneratorProcessorInterface {
	return &limitQueryGeneratorProcessor{
		logger:              helpers.NewLogger(`limitQueryGeneratorProcessor`),
		paginationGenerator: paginationParametersGenerator.PaginationParametersGenerator(),
	}
}
