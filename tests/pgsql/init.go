package pgsql

import (
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var Connection *sqlx.DB

func init() {
	// this Pings the database trying to connect, panics on error
	// use sqlx.Open() for sql.Open() semantics
	db, err := sqlx.Connect("postgres", "host=localhost user=postgres password=postgres dbname=test sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}

	Connection = db
	Connection.SetMaxIdleConns(10)
	Connection.SetMaxOpenConns(20)
}
