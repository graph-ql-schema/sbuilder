package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/tests/pgsql"
)

// Резолвер удаления тестовых сущностей
func testEntityDeleteResolver(params sbuilder.Parameters) (i interface{}, err error) {
	transaction, err := pgsql.Connection.Begin()
	if nil != err {
		return nil, err
	}

	if nil == transaction {
		return nil, fmt.Errorf(`can't start transaction`)
	}

	mappedFields := map[string]string{
		"active_date": "active_from",
	}

	query := `delete from test_data.test`

	where := ""
	if nil != params.Arguments.Where {
		where, err = params.Arguments.Where.ToSQL(params.GraphQlObject, mappedFields)

		if nil != err {
			_ = transaction.Rollback()
			return nil, err
		}
	}

	if 0 != len(where) {
		query += ` where ` + where
	}

	log.Println(query)
	res, err := transaction.Exec(query)
	if nil != err {
		_ = transaction.Rollback()
		return nil, err
	}

	affectedRows, err := res.RowsAffected()
	if nil != err {
		_ = transaction.Rollback()
		return nil, err
	}

	_ = wsDispatcher.DispatchEvent(context.Background(), sqlTestObj, sbuilder.EventTypeDeleted, "1", nil)

	_ = transaction.Commit()

	return map[string]interface{}{
		"affected_rows": affectedRows,
	}, nil
}
