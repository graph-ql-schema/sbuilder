package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

type simpleDataType = map[string]interface{}

var simpleOperationsToSqlOperator = map[string]string{
	constants.MoreSchemaKey:         ">",
	constants.LessSchemaKey:         "<",
	constants.MoreOrEqualsSchemaKey: ">=",
	constants.LessOrEqualsSchemaKey: "<=",
	constants.EqualsSchemaKey:       "=",
	constants.LikeSchemaKey:         "like",
}

// Процессор парсинга простых операторов
type simpleOperationProcessor struct {
	logger  *logrus.Entry
	factory TNewSimpleOperation
}

// Установка парсера в процессор
func (s simpleOperationProcessor) SetParser(WhereOrHavingParserInterface) {}

// Проверка доступности процессора
func (s simpleOperationProcessor) IsAvailable(ctx context.Context, arguments map[string]interface{}) bool {
	for _, data := range arguments {
		_, ok := data.(simpleDataType)
		if ok {
			return true
		}
	}

	return false
}

// Парсинг параметров фильтрации процессором
func (s simpleOperationProcessor) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (operation Operation, err error) {
	s.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started filter simple parameters parsing")

	var data simpleDataType
	var field string

	for code, argument := range arguments {
		parsedArgument, ok := argument.(simpleDataType)
		if ok {
			field = code
			data = parsedArgument

			break
		}
	}

	if nil == data {
		err = fmt.Errorf(`no available structures passed to simple operation processor`)
		s.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	var operationCode string
	var operationValue interface{}

	for code, value := range data {
		operationCode = code
		operationValue = value

		break
	}

	sqlOperator, ok := simpleOperationsToSqlOperator[operationCode]
	if !ok {
		err = fmt.Errorf(`field '%v', undeffined operation passed to simple operation processor`, field)
		s.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	val := s.factory(
		operationCode,
		operationValue,
		field,
		sqlOperator,
	)

	s.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
		"value":     val,
	}).Debug("Parsed filter simple value")

	return val, nil
}

// Фабрика процессора
func newSimpleOperationProcessor() whereOrHavingParserProcessorInterface {
	return &simpleOperationProcessor{
		logger:  helpers.NewLogger(`simpleOperationProcessor`),
		factory: NewSimpleOperation,
	}
}
