package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	for_tests "bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/paginationParametersGenerator"
	"github.com/graphql-go/graphql"
)

// Тестирование получения названия.
func Test_offsetQueryGeneratorProcessor_Name(t *testing.T) {
	type fields struct {
		logger              *logrus.Entry
		paginationGenerator paginationParametersGenerator.PaginationParametersGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "Тестирование получения названия",
			fields: fields{},
			want:   constants.OffsetSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := offsetQueryGeneratorProcessor{
				logger:              tt.fields.logger,
				paginationGenerator: tt.fields.paginationGenerator,
			}
			if got := l.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование работы генератора
func Test_offsetQueryGeneratorProcessor_Generate(t *testing.T) {
	type fields struct {
		logger              *logrus.Entry
		paginationGenerator paginationParametersGenerator.PaginationParametersGeneratorInterface
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование работы генератора",
			fields: fields{
				logger:              helpers.NewLogger(`test`),
				paginationGenerator: &for_tests.PaginationParametersGeneratorMock{},
			},
			args: args{
				object: graphql.NewObject(graphql.ObjectConfig{
					Name: "Test",
					Fields: graphql.Fields{
						"id": &graphql.Field{
							Name:        "id",
							Type:        graphql.ID,
							Description: "id",
						},
					},
					Description: "test",
				}),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := offsetQueryGeneratorProcessor{
				logger:              tt.fields.logger,
				paginationGenerator: tt.fields.paginationGenerator,
			}

			_ = l.Generate(context.Background(), tt.args.object, tt.args.namePrefix)
			paginationGenerator := tt.fields.paginationGenerator.(*for_tests.PaginationParametersGeneratorMock)

			if !paginationGenerator.GenerateOffsetIsCalled {
				t.Errorf("Generate() GenerateOffset in PaginationParametersGenerator is not called")
			}
		})
	}
}
