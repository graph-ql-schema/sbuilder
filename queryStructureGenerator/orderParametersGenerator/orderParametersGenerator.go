package orderParametersGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Генератор параметров сортировки
type orderParametersGenerator struct {
	logger     *logrus.Entry
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Генерация параметров сортировки
func (o orderParametersGenerator) Generate(ctx context.Context, object *graphql.Object, namePrefix string) *graphql.ArgumentConfig {
	o.logger.WithFields(logrus.Fields{
		"code":       100,
		"object":     object.Name(),
		"namePrefix": namePrefix,
	}).Debug(`Started order parameters generation for object`)

	name := fmt.Sprintf(`%v_order_parameters`, namePrefix)
	return &graphql.ArgumentConfig{
		Type: graphql.NewList(graphql.NewInputObject(graphql.InputObjectConfig{
			Name: fmt.Sprintf(`%v_object`, name),
			Fields: graphql.InputObjectConfigFieldMap{
				"by": &graphql.InputObjectFieldConfig{
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name:        fmt.Sprintf(`%v_object_by`, name),
						Values:      o.getEnumConfigMapByObject(object),
						Description: "Варианты выбора полей для сортировки",
					}),
					Description: "Варианты выбора полей для сортировки",
				},
				"direction": &graphql.InputObjectFieldConfig{
					Type: graphql.NewEnum(graphql.EnumConfig{
						Name: fmt.Sprintf(`%v_object_order`, name),
						Values: graphql.EnumValueConfigMap{
							"asc": &graphql.EnumValueConfig{
								Value:       "asc",
								Description: "Сортировка по возрастанию",
							},
							"desc": &graphql.EnumValueConfig{
								Value:       "desc",
								Description: "Сортировка по убыванию",
							},
						},
						Description: "Варианты сортировки",
					}),
					DefaultValue: "asc",
					Description:  "Варианты сортировки",
				},
				"priority": &graphql.InputObjectFieldConfig{
					Type:         graphql.Int,
					DefaultValue: 500,
					Description:  "Приоритет применения сортировки. Должен быть больше 0.",
				},
			},
			Description: "Сортировка по переданным параметрам",
		})),
		Description: "Сортировка по переданным параметрам",
	}
}

// Генерация коллекции значений для сортировки
func (o orderParametersGenerator) getEnumConfigMapByObject(object *graphql.Object) graphql.EnumValueConfigMap {
	configMap := graphql.EnumValueConfigMap{}
	for code, field := range object.Fields() {
		fieldType := o.typeGetter.GetRootType(field.Type)
		if _, ok := fieldType.(*graphql.Object); ok {
			continue
		}

		configMap[code] = &graphql.EnumValueConfig{
			Value:       code,
			Description: fmt.Sprintf(`Значение %v`, code),
		}
	}

	return configMap
}

// Фабрика генератора параметров сортировки
func OrderParametersGenerator() OrderParametersGeneratorInterface {
	return &orderParametersGenerator{
		logger:     helpers.NewLogger(`orderParametersGenerator`),
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
