package wsResolveGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"fmt"
	"github.com/functionalfoundry/graphqlws"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
)

// Генератор резолвера сущностей для подписчиков
type wsResolveGenerator struct {
	logger *logrus.Entry
}

// Создание резолвера
func (w wsResolveGenerator) MakeResolver(
	object *graphql.Object,
	middleware TSubscriptionMiddleware,
) TResolver {
	return func(p graphql.ResolveParams) (i interface{}, e error) {
		ctx := p.Context
		if nil == ctx {
			if nil != w.logger {
				w.logger.Error(`Context is not passed`)
			}

			return nil, fmt.Errorf(`context is not passed`)
		}

		eventObject, ok := ctx.Value("object").(*graphql.Object)
		if !ok {
			if nil != w.logger {
				w.logger.Error(`Event object is not set, you should pass correct event object`)
			}

			return nil, fmt.Errorf(`event object is not set, you should pass correct event object`)
		}

		eventType, ok := ctx.Value("eventType").(types.TEventType)
		if !ok {
			if nil != w.logger {
				w.logger.
					WithField("eventType", ctx.Value("eventType")).
					Error(`Event type is not set or is not of TEventType`)
			}

			return nil, fmt.Errorf(`event type is not set or is not of TEventType`)
		}

		entityId, ok := ctx.Value("entityId").(string)
		if !ok {
			if nil != w.logger {
				w.logger.
					WithField("entityId", ctx.Value("entityId")).
					Error(`Entity id is not set, you should pass correct entity ID`)
			}

			return nil, fmt.Errorf(`entity id is not set, you should pass correct entity ID`)
		}

		entityData := ctx.Value("entityData")

		connection, ok := ctx.Value("connection").(graphqlws.Connection)
		if !ok {
			if nil != w.logger {
				w.logger.Error(`Connection is not set to request`)
			}

			return nil, fmt.Errorf(`connection is not set to request`)
		}

		if object.Name() != eventObject.Name() {
			return nil, nil
		}

		if nil != p.Args["eventType"] {
			eventTypes := p.Args["eventType"].([]interface{})
			availableEventTypes := make([]types.TEventType, len(eventTypes))
			for i, id := range eventTypes {
				availableEventTypes[i] = fmt.Sprintf(`%v`, id)
			}

			if 0 != len(availableEventTypes) {
				if !w.inArray(eventType, availableEventTypes) {
					return nil, nil
				}
			}
		}

		if nil != p.Args["entityId"] {
			entityIds := p.Args["entityId"].([]interface{})
			availableEntityIds := make([]string, len(entityIds))
			for i, id := range entityIds {
				availableEntityIds[i] = fmt.Sprintf(`%v`, id)
			}

			if 0 != len(availableEntityIds) {
				if !w.inArray(entityId, availableEntityIds) {
					return nil, nil
				}
			}
		}

		entityData, err := middleware(SubscriptionMiddlewareParams{
			User:       connection.User(),
			Object:     eventObject,
			EventType:  eventType,
			EntityId:   entityId,
			EntityData: entityData,
		})

		if nil != err {
			return nil, err
		}

		return &ResolverResponse{
			EventType:  eventType,
			EntityId:   entityId,
			EntityData: entityData,
		}, nil
	}
}

// Проверка наличия элемента в массиве
func (w wsResolveGenerator) inArray(value string, values []string) bool {
	for _, eType := range values {
		if eType == value {
			return true
		}
	}

	return false
}

// Конструктор сервиса
func NewWsResolveGenerator() WsResolveGeneratorInterface {
	return &wsResolveGenerator{
		logger: helpers.NewLogger("WsResolver"),
	}
}
