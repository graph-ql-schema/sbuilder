package objectsParser

import (
	"context"
)

// Типы, описывающие переданные парамтеры объектов
type Object = map[string]interface{}
type Objects = []Object

// Парсер объектов для вставки
type ObjectsParserInterface interface {
	// Парсинг значений для вставки
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (Objects, error)
}

// Тип, описывающий фабрику парсера объектов для вставки
type TObjectsParserFactory = func() ObjectsParserInterface
