package paginationParametersGenerator

import (
	"reflect"
	"testing"

	"github.com/graphql-go/graphql"
)

// Тетирование генерации параметров "Limit"
func Test_paginationParametersGenerator_GenerateLimit(t *testing.T) {
	tests := []struct {
		name string
		want *graphql.ArgumentConfig
	}{
		{
			name: "Тетирование генерации параметров 'Limit'",
			want: &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 30,
				Description:  "Параметр пагинации: Количество элементов в выдаче.",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := paginationParametersGenerator{}
			if got := p.GenerateLimit(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateLimit() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тетирование генерации параметров "Offset"
func Test_paginationParametersGenerator_GenerateOffset(t *testing.T) {
	tests := []struct {
		name string
		want *graphql.ArgumentConfig
	}{
		{
			name: "Тетирование генерации параметров 'Offset'",
			want: &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
				Description:  "Параметр пагинации: Смещение для элементов в выдаче.",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := paginationParametersGenerator{}
			if got := p.GenerateOffset(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateOffset() = %v, want %v", got, tt.want)
			}
		})
	}
}
