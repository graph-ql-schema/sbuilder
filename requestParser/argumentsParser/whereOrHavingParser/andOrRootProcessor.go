package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
)

// Процессор парсинга оператора AND/OR для фильтра
type andOrRootProcessor struct {
	logger        *logrus.Entry
	parser        WhereOrHavingParserInterface
	rootKey       string
	operationType string
}

// Установка парсера в процессор
func (a *andOrRootProcessor) SetParser(parser WhereOrHavingParserInterface) {
	a.parser = parser
}

// Проверка доступности процессора
func (a *andOrRootProcessor) IsAvailable(ctx context.Context, arguments map[string]interface{}) bool {
	_, ok := arguments[a.rootKey]

	return ok
}

// Парсинг параметров фильтрации процессором
func (a andOrRootProcessor) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (operation Operation, err error) {
	a.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug(fmt.Sprintf("Started filter '%v' parameters parsing", a.rootKey))

	data, _ := arguments[a.rootKey]
	typedData, ok := data.(map[string]interface{})
	if !ok {
		err = fmt.Errorf(`'%v' operation fiter data is not in valid format`, a.rootKey)
		a.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	subOperations := make([]Operation, len(typedData))
	subOperationsIndex := 0
	for code, operationData := range typedData {
		res, err := a.parser.Parse(context.Background(), map[string]interface{}{
			code: operationData,
		})

		if nil != err {
			return nil, err
		}

		if nil == res {
			continue
		}

		subOperations[subOperationsIndex] = res
		subOperationsIndex++
	}

	operations := make([]Operation, subOperationsIndex)
	for i := 0; i < subOperationsIndex; i++ {
		operations[i] = subOperations[i]
	}

	a.logger.WithFields(logrus.Fields{
		"code":       100,
		"arguments":  arguments,
		"operations": operations,
	}).Debug(fmt.Sprintf("Parsed filter '%v' parameters", a.rootKey))

	if 0 == len(operations) {
		return nil, nil
	}

	return NewAndOrOperation(a.operationType, operations, a.rootKey), nil
}

// Фабрика процессора корневой структуры
func newRootProcessor(rootType string) whereOrHavingParserProcessorInterface {
	return &andOrRootProcessor{
		logger:        helpers.NewLogger(fmt.Sprintf(`andOrRootProcessor(root: %v)`, rootType)),
		rootKey:       rootType,
		operationType: constants.AndSchemaKey,
	}
}

// Фабрика AND процессора для корневого оператора
func newAndRootProcessor() whereOrHavingParserProcessorInterface {
	return &andOrRootProcessor{
		logger:        helpers.NewLogger(`andOrRootProcessor(root: _and)`),
		rootKey:       constants.AndSchemaKey,
		operationType: constants.AndSchemaKey,
	}
}

// Фабрика OR процессора для корневого оператора
func newOrRootProcessor() whereOrHavingParserProcessorInterface {
	return &andOrRootProcessor{
		logger:        helpers.NewLogger(`andOrRootProcessor(root: _or)`),
		rootKey:       constants.OrSchemaKey,
		operationType: constants.OrSchemaKey,
	}
}
