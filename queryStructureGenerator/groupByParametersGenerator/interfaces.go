package groupByParametersGenerator

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Генератор параметров сортировки
type GroupByParametersGeneratorInterface interface {
	// Генерация параметров сортировки
	Generate(ctx context.Context, object *graphql.Object, namePrefix string) *graphql.ArgumentConfig
}
