package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"
	"sync"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
	"golang.org/x/sync/errgroup"
)

// Генератор операторов поля для Where
type whereParametersOperatorGenerator struct {
	processors      []whereParametersOperatorGeneratorProcessorInterface
	logger          *logrus.Entry
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Генерация параметров
func (w whereParametersOperatorGenerator) GenerateOperations(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
	level uint8,
) *graphql.InputObjectConfigFieldMap {
	w.logger.WithFields(logrus.Fields{
		"code":       100,
		"object":     object.Name(),
		"namePrefix": namePrefix,
		"level":      level,
	}).Debug(`Generation operations for object`)

	mx := new(sync.Mutex)
	config := graphql.InputObjectConfigFieldMap{}
	g, wCtx := errgroup.WithContext(ctx)

	fields := object.Fields()
	for name, field := range fields {
		fieldClosure := field
		nameClosure := name

		g.Go(func() error {
			fieldType := w.fieldTypeGetter.GetRootType(fieldClosure.Type)
			if _, ok := fieldType.(*graphql.Object); ok {
				return nil
			}

			fieldConfig := w.generateConfigForField(wCtx, fieldClosure, namePrefix, level)
			if nil == fieldConfig {
				return nil
			}

			mx.Lock()
			defer mx.Unlock()

			config[nameClosure] = fieldConfig

			return nil
		})
	}

	_ = g.Wait()
	if 0 == len(config) {
		return nil
	}

	return &config
}

// Генерация конфигурации для одного поля
func (w whereParametersOperatorGenerator) generateConfigForField(
	ctx context.Context,
	field *graphql.FieldDefinition,
	namePrefix string,
	level uint8,
) *graphql.InputObjectFieldConfig {
	name := fmt.Sprintf(`%v_%v`, namePrefix, strings.ReplaceAll(strings.ToLower(field.Name), ` `, `_`))
	description := fmt.Sprintf(`Фильтр по полю '%v' для сущности`, field.Name)
	fields := graphql.InputObjectConfigFieldMap{}

	for _, processor := range w.processors {
		if processor.IsAvailable(ctx, field, level) {
			config := processor.Generate(ctx, field, level, fmt.Sprintf(`%v_operation`, name))
			if nil == config {
				continue
			}

			fields[processor.OperatorCode()] = config
		}
	}

	if 0 == len(fields) {
		return nil
	}

	return &graphql.InputObjectFieldConfig{
		Type: graphql.NewInputObject(graphql.InputObjectConfig{
			Name:        name,
			Fields:      fields,
			Description: description,
		}),
		Description: description,
	}
}

// Фабрика генератора
func WhereParametersOperatorGenerator(
	maxLevel uint8,
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface,
) WhereParametersOperatorGeneratorInterface {
	processors := []whereParametersOperatorGeneratorProcessorInterface{
		newEqualsWhereParametersOperatorGeneratorProcessor(maxLevel, fieldTypeGetter),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.MoreSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.LessSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.MoreOrEqualsSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.LessOrEqualsSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			[]graphql.Type{graphql.String},
			constants.LikeSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newInWhereParametersOperatorGeneratorProcessor(
			typesForInOperation,
			maxLevel,
			fieldTypeGetter,
		),
		newBetweenWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			maxLevel,
			fieldTypeGetter,
		),
	}

	return &whereParametersOperatorGenerator{
		processors:      processors,
		logger:          helpers.NewLogger(`whereParametersOperatorGenerator`),
		fieldTypeGetter: fieldTypeGetter,
	}
}

// Фабрика генератора с кастомизацией
func WhereParametersOperatorGeneratorWithCustomizations(
	maxLevel uint8,
	fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface,
	customizations []customizationService.CustomizationInterface,
) WhereParametersOperatorGeneratorInterface {
	processors := []whereParametersOperatorGeneratorProcessorInterface{
		newEqualsWhereParametersOperatorGeneratorProcessor(maxLevel, fieldTypeGetter),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.MoreSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.LessSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.MoreOrEqualsSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			constants.LessOrEqualsSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newConfigurableWhereParametersOperatorGeneratorProcessor(
			[]graphql.Type{graphql.String},
			constants.LikeSchemaKey,
			maxLevel,
			fieldTypeGetter,
		),
		newInWhereParametersOperatorGeneratorProcessor(
			typesForInOperation,
			maxLevel,
			fieldTypeGetter,
		),
		newBetweenWhereParametersOperatorGeneratorProcessor(
			typesForMultipleValueOperations,
			maxLevel,
			fieldTypeGetter,
		),
	}

	// Добавление кастомных процессоров
	for _, customization := range customizations {
		if customization.GetType() == constants.CustomizationTypeWhereParametersOperatorGenerator {
			processors = append(processors, customization.Customize().(whereParametersOperatorGeneratorProcessorInterface))
		}
	}

	return &whereParametersOperatorGenerator{
		processors:      processors,
		logger:          helpers.NewLogger(`whereParametersOperatorGenerator`),
		fieldTypeGetter: fieldTypeGetter,
	}
}
