package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Тестирование установки парсера
func Test_andOrRootProcessor_setParser(t *testing.T) {
	type fields struct {
		logger        *logrus.Entry
		parser        WhereOrHavingParserInterface
		rootKey       string
		operationType string
	}
	type args struct {
		parser WhereOrHavingParserInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "Тестирование установки парсера",
			fields: fields{},
			args: args{
				parser: &WhereOrHavingParserMock{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &andOrRootProcessor{
				logger:        tt.fields.logger,
				parser:        tt.fields.parser,
				rootKey:       tt.fields.rootKey,
				operationType: tt.fields.operationType,
			}

			a.SetParser(tt.args.parser)
			if a.parser != tt.args.parser {
				t.Errorf("setParser() Parser is not set correctly")
			}
		})
	}
}

// Тестирование проверки доступности процессора
func Test_andOrRootProcessor_isAvailable(t *testing.T) {
	type fields struct {
		logger        *logrus.Entry
		parser        WhereOrHavingParserInterface
		rootKey       string
		operationType string
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на пустой коллекции",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				parser:        &WhereOrHavingParserMock{},
				rootKey:       constants.WhereSchemaKey,
				operationType: "and",
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			want: false,
		},
		{
			name: "Тестирование на коллекции c фильтром",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				parser:        &WhereOrHavingParserMock{},
				rootKey:       constants.WhereSchemaKey,
				operationType: "and",
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: map[string]interface{}{},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &andOrRootProcessor{
				logger:        tt.fields.logger,
				parser:        tt.fields.parser,
				rootKey:       tt.fields.rootKey,
				operationType: tt.fields.operationType,
			}
			if got := a.IsAvailable(context.Background(), tt.args.arguments); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга процессора
func Test_andOrRootProcessor_parse(t *testing.T) {
	type fields struct {
		logger        *logrus.Entry
		parser        WhereOrHavingParserInterface
		rootKey       string
		operationType string
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantOperation Operation
		wantErr       bool
	}{
		{
			name: "Тестирование с не корректным форматом",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     false,
					IsCalled:        false,
				},
				rootKey:       constants.WhereSchemaKey,
				operationType: "and",
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: 1,
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
		{
			name: "Тестирование без возврата корректного значения",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     false,
					IsCalled:        false,
				},
				rootKey:       constants.WhereSchemaKey,
				operationType: "and",
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: map[string]interface{}{
						"test": 0,
					},
				},
			},
			wantOperation: nil,
			wantErr:       false,
		},
		{
			name: "Тестирование c возвратом корректного значения",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: &OperationStub{},
					ErrorResult:     false,
					IsCalled:        false,
				},
				rootKey:       constants.WhereSchemaKey,
				operationType: "and",
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: map[string]interface{}{
						"test": 0,
					},
				},
			},
			wantOperation: &andOrOperation{
				operationType: "and",
				operations:    []Operation{&OperationStub{}},
				field:         constants.WhereSchemaKey,
			},
			wantErr: false,
		},
		{
			name: "Тестирование c возвратом корректного значения",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: &OperationStub{},
					ErrorResult:     false,
					IsCalled:        false,
				},
				rootKey:       constants.WhereSchemaKey,
				operationType: "and",
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: map[string]interface{}{
						"test":   0,
						"test-2": 0,
					},
				},
			},
			wantOperation: &andOrOperation{
				operationType: "and",
				operations:    []Operation{&OperationStub{}, &OperationStub{}},
				field:         constants.WhereSchemaKey,
			},
			wantErr: false,
		},
		{
			name: "Тестирование c возвратом корректного значения. Проверка на другом operationType",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: &OperationStub{},
					ErrorResult:     false,
					IsCalled:        false,
				},
				rootKey:       constants.WhereSchemaKey,
				operationType: "or",
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: map[string]interface{}{
						"test":   0,
						"test-2": 0,
					},
				},
			},
			wantOperation: &andOrOperation{
				operationType: "or",
				operations:    []Operation{&OperationStub{}, &OperationStub{}},
				field:         constants.WhereSchemaKey,
			},
			wantErr: false,
		},
		{
			name: "Тестирование c возвратом ошбики",
			fields: fields{
				logger: helpers.NewLogger(`test`),
				parser: &WhereOrHavingParserMock{
					OperationResult: nil,
					ErrorResult:     true,
					IsCalled:        false,
				},
				rootKey:       constants.WhereSchemaKey,
				operationType: "and",
			},
			args: args{
				arguments: map[string]interface{}{
					constants.WhereSchemaKey: map[string]interface{}{
						"test":   0,
						"test-2": 0,
					},
				},
			},
			wantOperation: nil,
			wantErr:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &andOrRootProcessor{
				logger:        tt.fields.logger,
				parser:        tt.fields.parser,
				rootKey:       tt.fields.rootKey,
				operationType: tt.fields.operationType,
			}
			gotOperation, err := a.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOperation, tt.wantOperation) {
				t.Errorf("parse() gotOperation = %v, want %v", gotOperation, tt.wantOperation)
			}
		})
	}
}
