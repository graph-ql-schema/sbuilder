package queryGenerator

import (
	"context"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/groupByParametersGenerator"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации
func Test_groupByGeneratorProcessor_Generate(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		groupByGenerator groupByParametersGenerator.GroupByParametersGeneratorInterface
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				groupByGenerator: &for_tests.GroupByParametersGeneratorMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "role",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupByGeneratorProcessor{
				groupByGenerator: tt.fields.groupByGenerator,
			}
			_ = g.Generate(context.Background(), tt.args.object, tt.args.namePrefix)
			groupByGenerator := tt.fields.groupByGenerator.(*for_tests.GroupByParametersGeneratorMock)
			if !groupByGenerator.IsCalled {
				t.Errorf("Generate() GroupByParametersGenerator is not called")
			}
		})
	}
}

// Тестирование получение названия
func Test_groupByGeneratorProcessor_Name(t *testing.T) {
	type fields struct {
		groupByGenerator groupByParametersGenerator.GroupByParametersGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "Тестирование получение названия",
			fields: fields{},
			want:   constants.GroupBySchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupByGeneratorProcessor{
				groupByGenerator: tt.fields.groupByGenerator,
			}
			if got := g.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}
