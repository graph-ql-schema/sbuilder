package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
)

// Генератор запросов листинга сущностей
type queryGenerator struct {
	logger         *logrus.Entry
	processors     []queryGeneratorProcessorInterface
	aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
}

// Получение суффикса для названия запроса
func (q queryGenerator) Suffix() constants.QueryType {
	return constants.ListQuery
}

// Генерация запроса на получение сущности
func (q queryGenerator) Generate(ctx context.Context, object *graphql.Object, resolver TResolver) *graphql.Field {
	q.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(fmt.Sprintf(`Generation list query for entity '%v'`, object.Name()))

	arguments := graphql.FieldConfigArgument{}
	for _, processor := range q.processors {
		config := processor.Generate(ctx, object, q.aliasGenerator.Generate(object.Name()))
		if config != nil {
			arguments[processor.Name()] = config
		}
	}

	return &graphql.Field{
		Type:        graphql.NewList(object),
		Name:        fmt.Sprintf(`Листинг сущностей '%v'`, object.Name()),
		Description: fmt.Sprintf(`Листинг сущностей '%v'`, object.Name()),
		Args:        arguments,
		Resolve:     resolver,
	}
}

// Фабрика генератора запроса листинга сущностей
func QueryGeneratorFactory() QueryGeneratorInterface {
	return &queryGenerator{
		logger: helpers.NewLogger(`queryGenerator`),
		processors: []queryGeneratorProcessorInterface{
			newWhereOrHavingQueryGeneratorProcessor(constants.WhereSchemaKey),
			newLimitQueryGeneratorProcessor(),
			newOffsetQueryGeneratorProcessor(),
			newOrderGeneratorProcessor(),
		},
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
	}
}

// Фабрика генератора запроса листинга сущностей с кастомизацией
func QueryGeneratorFactoryWithCustomizations(customizations []customizationService.CustomizationInterface) QueryGeneratorInterface {
	return &queryGenerator{
		logger: helpers.NewLogger(`queryGenerator`),
		processors: []queryGeneratorProcessorInterface{
			newWhereOrHavingQueryGeneratorProcessorWithCustomizations(constants.WhereSchemaKey, customizations),
			newLimitQueryGeneratorProcessor(),
			newOffsetQueryGeneratorProcessor(),
			newOrderGeneratorProcessor(),
		},
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
	}
}
