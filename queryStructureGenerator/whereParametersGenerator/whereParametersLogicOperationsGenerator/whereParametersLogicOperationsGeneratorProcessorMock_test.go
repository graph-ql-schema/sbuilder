package whereParametersLogicOperationsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type whereParametersLogicOperationsGeneratorProcessorMock struct {
	OperationCodeIsCalled bool
	GenerateIsCalled      bool
	Generator             interfaces.WhereParametersGeneratorInterface
}

// Установка генератора параметров для процессора
func (w *whereParametersLogicOperationsGeneratorProcessorMock) SetWhereParametersOperatorGenerator(g interfaces.WhereParametersGeneratorInterface) {
	w.Generator = g
}

// Код операции, для вставки в схему GraphQL
func (w *whereParametersLogicOperationsGeneratorProcessorMock) OperationCode() string {
	w.OperationCodeIsCalled = true

	return "test"
}

// Генерация параметров
func (w *whereParametersLogicOperationsGeneratorProcessorMock) Generate(context.Context, *graphql.Object, uint8, string) *graphql.InputObjectFieldConfig {
	w.GenerateIsCalled = true

	return &graphql.InputObjectFieldConfig{
		Description: `Оператор test для объединения условий`,
		Type:        graphql.NewNonNull(graphql.Int),
	}
}
