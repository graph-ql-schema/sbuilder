package types

import "bitbucket.org/graph-ql-schema/sbuilder/v2/constants"

// Тип, описывающий резолвер запросов
type RequestResolver = func(params Parameters) (interface{}, error)

// Параметры генерируемых запросов
type EntityQueries = map[constants.QueryType]RequestResolver
