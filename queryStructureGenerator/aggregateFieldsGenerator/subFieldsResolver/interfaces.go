package subFieldsResolver

import "github.com/graphql-go/graphql"

// Резолвер поля для аггрегирующей сущности
type SubFieldsResolverInterface interface {
	// Резолвинг полей для аггрегирующей сущности
	Resolve(p graphql.ResolveParams) (interface{}, error)
}

// Тип, описывающий фабрику резолвера
type TSubFieldsResolverFactory = func(fieldCode string, fieldType graphql.Type) func(p graphql.ResolveParams) (interface{}, error)
