package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"
)

// Парсер параметров фильтрации
type whereOrHavingParser struct {
	processors []whereOrHavingParserProcessorInterface
}

// Parse выполняет парсинг параметров фильтрации
func (w *whereOrHavingParser) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (operation Operation, err error) {
	for _, processor := range w.processors {
		if processor.IsAvailable(ctx, arguments) {
			result, err := processor.Parse(ctx, arguments)

			return result, err
		}
	}

	return nil, fmt.Errorf(`no available processor to parse parameters`)
}

// Установка текущего парсера в процессоры парсера.
// Парсер используется для рекурсивного обхода всего дерева параметров
func (w *whereOrHavingParser) setParserToProcessors() {
	for _, processor := range w.processors {
		processor.SetParser(w)
	}
}

// NewWhereOrHavingParser генерирует парсер WHERE параметров
func NewWhereOrHavingParser() WhereOrHavingParserInterface {
	parser := &whereOrHavingParser{
		processors: []whereOrHavingParserProcessorInterface{
			newRootProcessor(constants.WhereSchemaKey),
			newRootProcessor(constants.HavingSchemaKey),
			newAndProcessor(),
			newOrProcessor(),
			newAndRootProcessor(),
			newOrRootProcessor(),
			newNotProcessor(),
			newBetweenOperationProcessor(),
			newInOperationProcessor(),
			newSimpleOperationProcessor(),
		},
	}

	parser.setParserToProcessors()

	return parser
}

// NewWhereOrHavingParserWithCustomizations генерирует парсер WHERE параметров с учетом
// кастомизации. По сути использует кастомизаторы для генерации кастомных процессоров операторов
//
// TODO: Сделать выброс ошибки генерации, если один из кастомизаторов возвращает не корректный тип.
//   На текущий момент данный процессор скипается.
func NewWhereOrHavingParserWithCustomizations(customizations []customizationService.CustomizationInterface) WhereOrHavingParserInterface {
	parser := &whereOrHavingParser{
		processors: []whereOrHavingParserProcessorInterface{
			newRootProcessor(constants.WhereSchemaKey),
			newRootProcessor(constants.HavingSchemaKey),
			newAndProcessor(),
			newOrProcessor(),
			newAndRootProcessor(),
			newOrRootProcessor(),
			newNotProcessor(),
			newBetweenOperationProcessor(),
			newInOperationProcessor(),
		},
	}

	// Добавление кастомных процессоров
	for _, customization := range customizations {
		if customization.GetType() == constants.CustomizationTypeOperationProcessor {
			if processor, ok := customization.Customize().(whereOrHavingParserProcessorInterface); ok {
				parser.processors = append(parser.processors, processor)
			}
		}
	}

	parser.processors = append(parser.processors, newSimpleOperationProcessor())

	parser.setParserToProcessors()

	return parser
}
