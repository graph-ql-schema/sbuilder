package interfaces

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Генератор параметров Where для запросов GraphQL
type WhereParametersGeneratorInterface interface {
	// Генерация параметров
	BuildParameters(
		ctx context.Context,
		object *graphql.Object,
		namePrefix string,
		level uint8,
	) *graphql.InputObjectConfigFieldMap
}

// Фабрика генератора параметров Where для запросов GraphQL
type TWhereParametersGeneratorFactory = func() WhereParametersGeneratorInterface
