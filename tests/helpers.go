package main

import (
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/orderParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

var operatorsToSql = map[string]string{
	constants.AvgOperationSchemaKey:      "avg",
	constants.MinOperationSchemaKey:      "min",
	constants.MaxOperationSchemaKey:      "max",
	constants.SumOperationSchemaKey:      "sum",
	constants.VariantsOperationSchemaKey: "json_agg",
}

// Генерация sql строки запроса для аггрегируемых полей
func buildSqlStringForAggregateField(
	code string,
	operation string,
	fields []requestedFieldsParser.GraphQlRequestedFieldInterface,
	fieldsMap map[string]string,
) string {
	data := []string{}
	for _, column := range fields {
		columnName := column.GetFieldName()
		if mappedColumn, ok := fieldsMap[column.GetFieldName()]; ok {
			columnName = mappedColumn
		}

		data = append(data, fmt.Sprintf(`'%v', %v(%v)`, column.GetFieldName(), operation, columnName))
	}

	return fmt.Sprintf(
		`json_build_object(%v) as %v`,
		strings.Join(data, `, `),
		code,
	)
}

// Генерация SQL подстроки запрошенных полей для запроса аггрегации
func buildRequestedFieldsForAggregate(
	fields []requestedFieldsParser.GraphQlRequestedFieldInterface,
	fieldsMap map[string]string,
) (string, error) {
	requestedFieldsSql := []string{}
	for _, field := range fields {
		operationsSQL, operationSqlOk := operatorsToSql[field.GetFieldName()]

		switch true {
		case field.GetFieldName() == constants.CountOperationSchemaKey:
			requestedFieldsSql = append(requestedFieldsSql, "count(*) as count")
			continue
		case field.HasSubFields() && operationSqlOk:
			requestedFieldsSql = append(
				requestedFieldsSql,
				buildSqlStringForAggregateField(
					field.GetFieldName(),
					operationsSQL,
					field.GetSubFields(),
					fieldsMap,
				),
			)
			continue
		default:
			return "", fmt.Errorf("unavailable operator found: %v", field.GetFieldName())
		}
	}

	return strings.Join(requestedFieldsSql, `,`), nil
}

// Генерирует строку сортировки для запроса
func generateOrderBySql(orders []orderParser.Order, fieldsMap map[string]string) string {
	if 0 == len(orders) {
		return ""
	}

	sorts := []string{}
	sort.Slice(orders, func(i, j int) bool { return orders[i].Priority < orders[j].Priority })
	for _, order := range orders {
		fieldName := order.By
		if mappedField, ok := fieldsMap[fieldName]; ok {
			fieldName = mappedField
		}

		sorts = append(sorts, fmt.Sprintf(`%v %v`, fieldName, order.Direction))
	}

	return strings.Join(sorts, `,`)
}
