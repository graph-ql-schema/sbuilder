package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"github.com/graphql-go/graphql"
)

// Генератор аггрегирующей сущности для модели
type aggregateFieldsGenerator struct {
	processors []aggregateFieldsGeneratorProcessorInterface
	logger     *logrus.Entry
}

// Генерация сущности
func (a aggregateFieldsGenerator) Generate(ctx context.Context, object *graphql.Object) *graphql.Object {
	a.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(`Started generation of aggregation entity for object`)

	name := strings.ToLower(strings.ReplaceAll(object.Name(), ` `, `_`))
	fields := graphql.Fields{}

	for _, processor := range a.processors {
		fieldConfig, code := processor.GenerateField(ctx, object)
		if nil == fieldConfig {
			continue
		}

		fields[code] = fieldConfig
	}

	return graphql.NewObject(
		graphql.ObjectConfig{
			Name:        fmt.Sprintf(`%v_aggregation`, name),
			Fields:      fields,
			Description: fmt.Sprintf("Сущность для запросов аггрегации для объекта: '%v'", object.Name()),
		},
	)
}

// Фабрика генератора аггрегирующей сущности для модели
func AggregateFieldsGenerator() AggregateFieldsGeneratorInterface {
	return &aggregateFieldsGenerator{
		processors: []aggregateFieldsGeneratorProcessorInterface{
			newCountAggregateFieldsGeneratorProcessor(),
			newVariantsAggregateFieldsGeneratorProcessor(),
			newConfigurableAggregateFieldsGeneratorProcessor(
				constants.SumOperationSchemaKey,
				func(object *graphql.Object) string {
					return fmt.Sprintf(`Получение суммы значений для полей сущности '%v'`, object.Name())
				},
				func(valueCode string, object *graphql.Object) string {
					return fmt.Sprintf(`Получение суммы значений для поля %v`, valueCode)
				},
				[]graphql.Type{
					graphql.Int,
					graphql.Float,
				},
			),
			newConfigurableAggregateFieldsGeneratorProcessor(
				constants.AvgOperationSchemaKey,
				func(object *graphql.Object) string {
					return fmt.Sprintf(`Получение средних значений для полей сущности '%v'`, object.Name())
				},
				func(valueCode string, object *graphql.Object) string {
					return fmt.Sprintf(`Получение среднего значения для поля %v`, valueCode)
				},
				[]graphql.Type{
					graphql.Int,
					graphql.Float,
				},
			),
			newConfigurableAggregateFieldsGeneratorProcessor(
				constants.MaxOperationSchemaKey,
				func(object *graphql.Object) string {
					return fmt.Sprintf(`Получение максимальных значений для полей сущности '%v'`, object.Name())
				},
				func(valueCode string, object *graphql.Object) string {
					return fmt.Sprintf(`Получение максимального значения для поля %v`, valueCode)
				},
				[]graphql.Type{
					graphql.Int,
					graphql.Float,
					graphql.DateTime,
				},
			),
			newConfigurableAggregateFieldsGeneratorProcessor(
				constants.MinOperationSchemaKey,
				func(object *graphql.Object) string {
					return fmt.Sprintf(`Получение минимальных значений для полей сущности '%v'`, object.Name())
				},
				func(valueCode string, object *graphql.Object) string {
					return fmt.Sprintf(`Получение минимального значения для поля %v`, valueCode)
				},
				[]graphql.Type{
					graphql.Int,
					graphql.Float,
					graphql.DateTime,
				},
			),
		},
		logger: helpers.NewLogger(`aggregateFieldsGenerator`),
	}
}
