package constants

// Типы кастомизаций
const (
	CustomizationTypeWhereParametersOperatorGenerator = 1
	CustomizationTypeOperationProcessor = 2
	CustomizationTypeValueConverter = 3
)
