package types

import (
	"context"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
	"github.com/graphql-go/graphql"
)

// Параметры запроса, передаваемые в контроллер
type Parameters struct {
	Arguments     argumentsParser.ParsedArguments
	Context       context.Context
	Fields        requestedFieldsParser.GraphQlRequestedFields
	GraphQlObject *graphql.Object
	GraphqlParams graphql.ResolveParams
}
