package whereOrHavingParser

import (
	"fmt"
	"strings"

	"github.com/graphql-go/graphql"
)

// Операция логического AND и OR
type andOrOperation struct {
	operationType string
	operations    []Operation
	field         string
}

// Получение типа текущей операции
func (a andOrOperation) Type() string {
	return a.operationType
}

// Получение значения для текущей операции
func (a andOrOperation) Value() interface{} {
	return a.operations
}

// Получение кода поля для операции
func (a andOrOperation) Field() string {
	return a.field
}

// Конвертация в SQL
func (a andOrOperation) ToSQL(object *graphql.Object, fieldsMap map[string]string) (string, error) {
	sql := []string{}
	for _, operation := range a.operations {
		operationRes, err := operation.ToSQL(object, fieldsMap)
		if nil != err {
			return "", fmt.Errorf(`'%v' error: %v`, a.operationType, err.Error())
		}

		sql = append(sql, fmt.Sprintf(`%v`, operationRes))
	}

	result := strings.Join(sql, fmt.Sprintf(` %v `, a.operationType))
	if len(sql) > 1 {
		result = fmt.Sprintf(`(%v)`, result)
	}

	return result, nil
}

// Фабрика операции
func NewAndOrOperation(
	operationType string,
	operations []Operation,
	field string,
) Operation {
	return &andOrOperation{
		operationType: operationType,
		operations:    operations,
		field:         field,
	}
}
