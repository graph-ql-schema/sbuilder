package main

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/wsResolveGenerator"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"time"
)

var schemaBuilder = sbuilder.NewGraphQlSchemaBuilder()
var wsDispatcher sbuilder.WsDispatcherInterface

func main() {
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(new(prefixed.TextFormatter))

	schemaBuilder.RegisterEntity(sqlTestObj, sbuilder.EntityQueries{
		sbuilder.ListQuery:      testEntityListResolver,
		sbuilder.AggregateQuery: testEntityAggregateResolver,
		sbuilder.InsertMutation: testEntityInsertResolver,
		sbuilder.UpdateMutation: testEntityUpdateResolver,
		sbuilder.DeleteMutation: testEntityDeleteResolver,
	})

	schemaBuilder.RegisterEntityChangeSubscription(sqlTestObj, func(params wsResolveGenerator.SubscriptionMiddlewareParams) (entityData interface{}, err error) {
		return params.EntityData, nil
	})

	server, err := schemaBuilder.BuildServer()
	if nil != err {
		logrus.Fatal(err)
	}

	wsServer, err := schemaBuilder.BuildWsServer(func(token string) (i interface{}, e error) {
		return nil, nil
	})

	d, err := schemaBuilder.BuildWsDispatcher()
	if nil != err {
		log.Fatal(err)
	}

	wsDispatcher = d

	go func() {
		log.Print(wsServer.Run())
	}()

	go func() {
		time.Sleep(10 * time.Second)

		wsServer.GracefulShutdown()
	}()

	log.Fatal(server.Run())
}
