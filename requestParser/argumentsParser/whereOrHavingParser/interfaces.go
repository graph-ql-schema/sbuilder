package whereOrHavingParser

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Операция для применения фильтра
type Operation interface {
	// Получение типа текущей операции
	Type() string

	// Получение значения для текущей операции
	Value() interface{}

	// Получение кода поля для операции
	Field() string

	// Конвертация в SQL
	ToSQL(object *graphql.Object, fieldsMap map[string]string) (string, error)
}

// Парсер параметров фильтрации
type WhereOrHavingParserInterface interface {
	// Парсинг параметров фильтрации
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (operation Operation, err error)
}

// Процессор парсинга параметров фильтрации
type whereOrHavingParserProcessorInterface interface {
	// Установка парсера в процессор
	SetParser(parser WhereOrHavingParserInterface)

	// Проверка доступности процессора
	IsAvailable(ctx context.Context, arguments map[string]interface{}) bool

	// Парсинг параметров фильтрации процессором
	Parse(
		ctx context.Context,
		arguments map[string]interface{},
	) (operation Operation, err error)
}

// Публичный интерфейс парсинга параметров фильтрации для кастомизации
type WhereOrHavingParserProcessorInterface interface {
	whereOrHavingParserProcessorInterface
}
