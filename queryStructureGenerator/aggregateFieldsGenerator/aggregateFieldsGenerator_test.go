package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"github.com/graphql-go/graphql"
)

// Тестирование генерации аггрегирующей сущности
func Test_aggregateFieldsGenerator_Generate(t *testing.T) {
	type fields struct {
		processors []aggregateFieldsGeneratorProcessorInterface
		logger     *logrus.Entry
	}
	type args struct {
		object *graphql.Object
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.Object
	}{
		{
			name: "Тестирование генерации без доступных полей.",
			fields: fields{
				processors: []aggregateFieldsGeneratorProcessorInterface{
					&aggregateFieldsGeneratorProcessorMock{
						ReturnField: nil,
						ReturnCode:  "",
					},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
			},
			want: graphql.NewObject(
				graphql.ObjectConfig{
					Name:        `role_aggregation`,
					Fields:      graphql.Fields{},
					Description: "Сущность для запросов аггрегации для объекта: 'Role'",
				},
			),
		},
		{
			name: "Тестирование генерации с доступными полями.",
			fields: fields{
				processors: []aggregateFieldsGeneratorProcessorInterface{
					&aggregateFieldsGeneratorProcessorMock{
						ReturnField: &graphql.Field{
							Type: graphql.NewNonNull(graphql.NewObject(
								graphql.ObjectConfig{
									Name: `role_aggregate_test_field_response_type`,
									Fields: graphql.Fields{
										"date": &graphql.Field{
											Type:        graphql.NewList(graphql.DateTime),
											Name:        `role_aggregate_test_field_value_date`,
											Description: `test field desc`,
										},
									},
									Description: `test desc`,
								},
							)),
							Name:        `role_aggregate_test_field_type`,
							Description: `test desc`,
						},
						ReturnCode: "test",
					},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
			},
			want: graphql.NewObject(
				graphql.ObjectConfig{
					Name: `role_aggregation`,
					Fields: graphql.Fields{
						"test": &graphql.Field{
							Type: graphql.NewNonNull(graphql.NewObject(
								graphql.ObjectConfig{
									Name: `role_aggregate_test_field_response_type`,
									Fields: graphql.Fields{
										"date": &graphql.Field{
											Type:        graphql.NewList(graphql.DateTime),
											Name:        `role_aggregate_test_field_value_date`,
											Description: `test field desc`,
										},
									},
									Description: `test desc`,
								},
							)),
							Name:        `role_aggregate_test_field_type`,
							Description: `test desc`,
						},
					},
					Description: "Сущность для запросов аггрегации для объекта: 'Role'",
				},
			),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := aggregateFieldsGenerator{
				processors: tt.fields.processors,
				logger:     tt.fields.logger,
			}
			if got := a.Generate(context.Background(), tt.args.object); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
