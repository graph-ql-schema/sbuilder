package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/nullable"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Процессор генерации объекта SET для обновления сущности.
type setObjectGeneratorProcessor struct {
	logger         *logrus.Entry
	typeGetter     gql_root_type_getter.GraphQlRootTypeGetterInterface
	aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
}

// Получение названия генерируемого поля
func (s setObjectGeneratorProcessor) Name() string {
	return constants.SetSchemaKey
}

// Генерация
func (s setObjectGeneratorProcessor) Generate(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
) *graphql.ArgumentConfig {
	name := fmt.Sprintf(`%v_set`, namePrefix)
	updateObject := s.createUpdateObject(object, name)
	if nil == updateObject {
		return nil
	}

	s.logger.WithFields(logrus.Fields{
		"code":   100,
		"object": object.Name(),
	}).Debug(`Generation SET parameter for mutation`)

	return &graphql.ArgumentConfig{
		Type:        updateObject,
		Description: "Сущность для обновления",
	}
}

// Генерация объекта для вставки
func (s setObjectGeneratorProcessor) createUpdateObject(object *graphql.Object, namePrefix string) *graphql.InputObject {
	fieldsConfig := s.getFieldsConfigMapByObject(object, namePrefix)
	if 0 == len(fieldsConfig) {
		return nil
	}

	return graphql.NewInputObject(
		graphql.InputObjectConfig{
			Name:        fmt.Sprintf(`%v_type`, namePrefix),
			Fields:      fieldsConfig,
			Description: fmt.Sprintf(`Тип объекта для обновления значения для сущности: '%v'`, object.Name()),
		},
	)
}

// Генерация коллекции значений для вставки
func (s setObjectGeneratorProcessor) getFieldsConfigMapByObject(object *graphql.Object, namePrefix string) graphql.InputObjectConfigFieldMap {
	configMap := graphql.InputObjectConfigFieldMap{}
	for code, field := range object.Fields() {
		if nil != field.Resolve {
			continue
		}

		fieldType := s.typeGetter.GetRootType(field.Type)
		if _, ok := fieldType.(*graphql.Object); ok {
			continue
		}

		if scalar, ok := fieldType.(*graphql.Scalar); ok && s.typeGetter.IsNullable(field.Type) {
			fieldType = nullable.NewNullable(scalar)
		}

		if s.typeGetter.IsList(field.Type) {
			fieldType = graphql.NewList(fieldType)
		}

		configMap[code] = &graphql.InputObjectFieldConfig{
			Type: fieldType,
			Description: fmt.Sprintf(
				`Значение для обновления для сущности '%v' поля '%v'`,
				object.Name(),
				s.aliasGenerator.Generate(field.Name),
			),
		}
	}

	return configMap
}

// Фабрика процессора
func newSetObjectGeneratorProcessor() queryGeneratorProcessorInterface {
	return &setObjectGeneratorProcessor{
		logger:         helpers.NewLogger(`setObjectGeneratorProcessor`),
		typeGetter:     gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		aliasGenerator: aliasGenerationService.AliasGenerationService(),
	}
}
