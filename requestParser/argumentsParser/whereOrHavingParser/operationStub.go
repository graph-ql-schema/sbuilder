package whereOrHavingParser

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type OperationStub struct {
	operationType string
	value         interface{}
	field         string
	sql           string
	sqlErr        bool
}

// Получение типа текущей операции
func (o OperationStub) Type() string {
	return o.operationType
}

// Получение значения для текущей операции
func (o OperationStub) Value() interface{} {
	return o.value
}

// Получение кода поля для операции
func (o OperationStub) Field() string {
	return o.field
}

// Конвертация в SQL
func (o OperationStub) ToSQL(*graphql.Object, map[string]string) (string, error) {
	if o.sqlErr {
		return "", fmt.Errorf(`test`)
	}

	return o.sql, nil
}
