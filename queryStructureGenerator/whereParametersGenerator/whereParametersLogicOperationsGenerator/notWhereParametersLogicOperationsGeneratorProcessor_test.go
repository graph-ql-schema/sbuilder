package whereParametersLogicOperationsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/whereParametersOperatorGenerator"
	"github.com/graphql-go/graphql"
)

// Тестирование получения кода операции
func Test_notWhereParametersLogicOperationsGeneratorProcessor_OperationCode(t *testing.T) {
	type fields struct {
		generator whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface
		logger    *logrus.Entry
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения кода операции",
			fields: fields{
				generator: nil,
				logger:    nil,
			},
			want: constants.NotSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notWhereParametersLogicOperationsGeneratorProcessor{
				generator: tt.fields.generator,
				logger:    tt.fields.logger,
			}
			if got := n.OperationCode(); got != tt.want {
				t.Errorf("OperationCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование корректности кода операции в результате
func Test_notWhereParametersLogicOperationsGeneratorProcessor_Generate_OperationName(t *testing.T) {
	type fields struct {
		generator whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface
		logger    *logrus.Entry
	}
	type args struct {
		object     *graphql.Object
		level      uint8
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование корректности кода операции в результате. Вариант 1.",
			fields: fields{
				generator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "test",
			},
			want: "test_not",
		},
		{
			name: "Тестирование корректности кода операции в результате. Вариант 2.",
			fields: fields{
				generator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "",
			},
			want: "not",
		},
		{
			name: "Тестирование корректности кода операции в результате. Вариант 3.",
			fields: fields{
				generator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "test_test",
			},
			want: "test_test_not",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notWhereParametersLogicOperationsGeneratorProcessor{
				generator: tt.fields.generator,
				logger:    tt.fields.logger,
			}
			if got := n.Generate(context.Background(), tt.args.object, tt.args.level, tt.args.namePrefix); !reflect.DeepEqual(got.Type.Name(), tt.want) {
				t.Errorf("Generate() Type name = %v, want %v", got.Type.Name(), tt.want)
			}
		})
	}
}

// Тестирование корректности ответа при различных вариантах параметров, когда результата быть не должно.
func Test_notWhereParametersLogicOperationsGeneratorProcessor_Generate_NilResultTest(t *testing.T) {
	type fields struct {
		generator whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface
		logger    *logrus.Entry
	}
	type args struct {
		object     *graphql.Object
		level      uint8
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование передачи пустого объекта",
			fields: fields{
				generator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled:     false,
					ReturnResult: &graphql.InputObjectConfigFieldMap{},
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object:     &graphql.Object{},
				level:      1,
				namePrefix: "test",
			},
		},
		{
			name: "Тестирование возвращения пустого объекта генератором",
			fields: fields{
				generator: &for_tests.WhereParametersOperatorGeneratorMock{
					IsCalled:     false,
					ReturnResult: nil,
				},
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "user",
						Fields: graphql.Fields{
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "User ID",
							},
						},
						Description: "User entity",
					},
				),
				level:      1,
				namePrefix: "test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notWhereParametersLogicOperationsGeneratorProcessor{
				generator: tt.fields.generator,
				logger:    tt.fields.logger,
			}
			if got := n.Generate(context.Background(), tt.args.object, tt.args.level, tt.args.namePrefix); got != nil {
				t.Errorf("Generate() = %v, want %v", got, nil)
			}
		})
	}
}
