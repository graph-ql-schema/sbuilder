package whereOrHavingParser

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	for_tests2 "bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование получения типа операции
func Test_inOperation_Type(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue []interface{}
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения типа операции",
			fields: fields{
				operationType:  "test",
				operationValue: nil,
				field:          "",
				valueConverter: nil,
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			if got := i.Type(); got != tt.want {
				t.Errorf("Type() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения значения
func Test_inOperation_Value(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue []interface{}
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   interface{}
	}{
		{
			name: "Тестирование получения значения",
			fields: fields{
				operationType: "",
				operationValue: []interface{}{
					1,
				},
				field:          "",
				valueConverter: nil,
			},
			want: []interface{}{
				1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			if got := i.Value(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тетсирование получения кода поля
func Test_inOperation_Field(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue []interface{}
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тетсирование получения кода поля",
			fields: fields{
				operationType:  "",
				operationValue: nil,
				field:          "test",
				valueConverter: nil,
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			if got := i.Field(); got != tt.want {
				t.Errorf("Field() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации SQL
func Test_inOperation_ToSQL(t *testing.T) {
	type fields struct {
		operationType  string
		operationValue []interface{}
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	type args struct {
		object    *graphql.Object
		fieldsMap map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование с возвращением ошибки от конвертера значения",
			fields: fields{
				operationType: "test",
				operationValue: []interface{}{
					10,
				},
				field: "test",
				valueConverter: &for_tests2.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: true,
					ToSQLValueRes: "",
				},
			},
			args: args{
				object:    nil,
				fieldsMap: nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование без ошибки от конвертера значения",
			fields: fields{
				operationType: "test",
				operationValue: []interface{}{
					10,
				},
				field: "test",
				valueConverter: &for_tests2.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: false,
					ToSQLValueRes: "10",
				},
			},
			args: args{
				object:    nil,
				fieldsMap: nil,
			},
			want:    "test in (10)",
			wantErr: false,
		},
		{
			name: "Тестирование без ошибки от конвертера значения",
			fields: fields{
				operationType: "test",
				operationValue: []interface{}{
					10,
					10,
				},
				field: "test",
				valueConverter: &for_tests2.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: false,
					ToSQLValueRes: "10",
				},
			},
			args: args{
				object:    nil,
				fieldsMap: nil,
			},
			want:    "test in (10,10)",
			wantErr: false,
		},
		{
			name: "Тестирование с подменой кода поля",
			fields: fields{
				operationType: "test",
				operationValue: []interface{}{
					10,
				},
				field: "test",
				valueConverter: &for_tests2.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: false,
					ToSQLValueRes: "10",
				},
			},
			args: args{
				object: nil,
				fieldsMap: map[string]string{
					"test": "mapped",
				},
			},
			want:    "mapped in (10)",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inOperation{
				operationType:  tt.fields.operationType,
				operationValue: tt.fields.operationValue,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			got, err := i.ToSQL(tt.args.object, tt.args.fieldsMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToSQL() got = %v, want %v", got, tt.want)
			}
		})
	}
}
