package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aliasGenerationService"
	"github.com/graphql-go/graphql"
)

// Тестирование генерации
func Test_setObjectGeneratorProcessor_Generate(t *testing.T) {
	type fields struct {
		logger         *logrus.Entry
		typeGetter     gql_root_type_getter.GraphQlRootTypeGetterInterface
		aliasGenerator aliasGenerationService.AliasGenerationServiceInterface
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.ArgumentConfig
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				logger:         helpers.NewLogger(`test`),
				typeGetter:     &for_tests.GraphQlRootTypeGetterMock{},
				aliasGenerator: &for_tests.AliasGenerationServiceMock{},
			},
			args: args{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"field_1": &graphql.Field{
								Type: graphql.Int,
								Name: "field_1",
							},
							"id": &graphql.Field{
								Type: graphql.ID,
								Name: "id",
							},
							"ids": &graphql.Field{
								Type: graphql.NewList(graphql.ID),
								Name: "ids",
							},
						},
						Description: "Role entity",
					},
				),
				namePrefix: "test",
			},
			want: &graphql.ArgumentConfig{
				Type: graphql.NewInputObject(
					graphql.InputObjectConfig{
						Name: `test_set_type`,
						Fields: graphql.InputObjectConfigFieldMap{
							"field_1": &graphql.InputObjectFieldConfig{
								Type:        graphql.Int,
								Description: `Значение для обновления для сущности 'Role' поля 'field_1'`,
							},
							"id": &graphql.InputObjectFieldConfig{
								Type:        graphql.ID,
								Description: `Значение для обновления для сущности 'Role' поля 'id'`,
							},
							"ids": &graphql.InputObjectFieldConfig{
								Type:        graphql.NewList(graphql.ID),
								Description: `Значение для обновления для сущности 'Role' поля 'ids'`,
							},
						},
						Description: `Тип объекта для обновления значения для сущности: 'Role'`,
					},
				),
				Description: "Сущность для обновления",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := setObjectGeneratorProcessor{
				logger:         tt.fields.logger,
				typeGetter:     tt.fields.typeGetter,
				aliasGenerator: tt.fields.aliasGenerator,
			}
			if got := s.Generate(context.Background(), tt.args.object, tt.args.namePrefix); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
