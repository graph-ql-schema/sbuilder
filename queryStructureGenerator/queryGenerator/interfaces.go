package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"context"
	"github.com/graphql-go/graphql"
)

// Резолвер сущности
type TResolver = func(graphql.ResolveParams) (interface{}, error)

// Генератор запроса для сущности
type QueryGeneratorInterface interface {
	// Получение суффикса для названия мутации/запроса
	Suffix() constants.QueryType

	// Генерация запроса для сущности
	Generate(
		ctx context.Context,
		object *graphql.Object,
		resolver TResolver,
	) *graphql.Field
}

// Процессор генерации запроса
type queryGeneratorProcessorInterface interface {
	// Получение названия генерируемого поля
	Name() string

	// Генерация
	Generate(ctx context.Context, object *graphql.Object, namePrefix string) *graphql.ArgumentConfig
}

// Фабрика генератора запроса на получение сущности
type TQueryGeneratorFactory = func() QueryGeneratorInterface
