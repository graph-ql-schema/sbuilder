package for_tests

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка: Генератор параметров Where для запросов GraphQL
type WhereParametersGeneratorMock struct {
	IsCalled     bool
	ReturnResult *graphql.InputObjectConfigFieldMap
}

// Фабрика генератора параметров Where для запросов GraphQL
func (w *WhereParametersGeneratorMock) BuildParameters(
	context.Context,
	*graphql.Object,
	string,
	uint8,
) *graphql.InputObjectConfigFieldMap {
	w.IsCalled = true

	return w.ReturnResult
}
