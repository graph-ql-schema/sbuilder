package whereParametersGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/whereParametersLogicOperationsGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/whereParametersOperatorGenerator"
	"github.com/graphql-go/graphql"
)

// Генератор параметров Where для запросов GraphQL
type whereParametersGenerator struct {
	logicOperationsGenerator whereParametersLogicOperationsGenerator.WhereParametersLogicOperationsGeneratorInterface
	operatorGenerator        whereParametersOperatorGenerator.WhereParametersOperatorGeneratorInterface
	logger                   *logrus.Entry
}

// Генерация параметров
func (w whereParametersGenerator) BuildParameters(
	ctx context.Context,
	object *graphql.Object,
	namePrefix string,
	level uint8,
) *graphql.InputObjectConfigFieldMap {
	w.logger.WithFields(logrus.Fields{
		"code":       100,
		"object":     object.Name(),
		"namePrefix": namePrefix,
		"level":      level,
	}).Debug(fmt.Sprintf("Started generation parameters for prefix '%v'", namePrefix))

	fieldsConfig := w.operatorGenerator.GenerateOperations(ctx, object, namePrefix, level)
	operatorsConfig := w.logicOperationsGenerator.GenerateParametersForObject(ctx, object, level, namePrefix)

	result := graphql.InputObjectConfigFieldMap{}

	if nil != fieldsConfig {
		for key, data := range *fieldsConfig {
			result[key] = data
		}
	}

	if nil != operatorsConfig {
		for key, data := range *operatorsConfig {
			result[key] = data
		}
	}

	if 0 == len(result) {
		return nil
	}

	return &result
}

// Фабрика сервиса
func WhereParametersGeneratorFactory() interfaces.WhereParametersGeneratorInterface {
	fieldTypeGetterService := gql_root_type_getter.GraphQlRootTypeGetterFactory()

	operatorGenerator := whereParametersOperatorGenerator.WhereParametersOperatorGenerator(
		whereParamsGenerationMaxLevel,
		fieldTypeGetterService,
	)

	logicOperationsGenerator := whereParametersLogicOperationsGenerator.WhereParametersLogicOperationsGenerator(
		whereParamsGenerationMaxLevel,
		operatorGenerator,
	)

	parametersGenerator := &whereParametersGenerator{
		logicOperationsGenerator: logicOperationsGenerator,
		operatorGenerator:        operatorGenerator,
		logger:                   helpers.NewLogger(`whereParametersGenerator`),
	}

	logicOperationsGenerator.SetWhereParametersOperatorGenerator(parametersGenerator)

	return parametersGenerator
}

// Фабрика сервиса с кастомизацией
func WhereParametersGeneratorFactoryWithCustomizations(
	customizations []customizationService.CustomizationInterface,
) interfaces.WhereParametersGeneratorInterface {
	fieldTypeGetterService := gql_root_type_getter.GraphQlRootTypeGetterFactory()

	operatorGenerator := whereParametersOperatorGenerator.WhereParametersOperatorGeneratorWithCustomizations(
		whereParamsGenerationMaxLevel,
		fieldTypeGetterService,
		customizations,
	)

	logicOperationsGenerator := whereParametersLogicOperationsGenerator.WhereParametersLogicOperationsGenerator(
		whereParamsGenerationMaxLevel,
		operatorGenerator,
	)

	parametersGenerator := &whereParametersGenerator{
		logicOperationsGenerator: logicOperationsGenerator,
		operatorGenerator:        operatorGenerator,
		logger:                   helpers.NewLogger(`whereParametersGenerator`),
	}

	logicOperationsGenerator.SetWhereParametersOperatorGenerator(parametersGenerator)

	return parametersGenerator
}
