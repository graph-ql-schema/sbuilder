package groupByParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования
type GroupByParserMock struct {
	Result GroupBy
	IsErr  bool
}

// Парсинг полей группировки
func (g GroupByParserMock) Parse(
	context.Context,
	map[string]interface{},
) (GroupBy, error) {
	if g.IsErr {
		return nil, fmt.Errorf(`test`)
	}

	return g.Result, nil
}
