package customizationService

// Интерфейс кастомизатора
type CustomizationInterface interface {
	// Получение типа кастомизации
	GetType() int64

	// Применение кастомизатора
	Customize() interface{}
}
