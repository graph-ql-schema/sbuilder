package whereParametersLogicOperationsGenerator

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/whereParametersGenerator/interfaces"
	"github.com/graphql-go/graphql"
)

// Тестирование установки генератора
func Test_whereParametersLogicOperationsGenerator_SetWhereParametersOperatorGenerator(t *testing.T) {
	type fields struct {
		processors []whereParametersLogicOperationsGeneratorProcessorInterface
		generator  interfaces.WhereParametersGeneratorInterface
		maxLevel   uint8
	}
	type args struct {
		generator interfaces.WhereParametersGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование установки генератора во все процессоры",
			fields: fields{
				processors: []whereParametersLogicOperationsGeneratorProcessorInterface{
					&whereParametersLogicOperationsGeneratorProcessorMock{},
					&whereParametersLogicOperationsGeneratorProcessorMock{},
					&whereParametersLogicOperationsGeneratorProcessorMock{},
				},
				generator: nil,
				maxLevel:  100,
			},
			args: args{
				generator: &for_tests.WhereParametersGeneratorMock{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &whereParametersLogicOperationsGenerator{
				processors: tt.fields.processors,
				generator:  tt.fields.generator,
				maxLevel:   tt.fields.maxLevel,
			}

			w.SetWhereParametersOperatorGenerator(tt.args.generator)

			if !reflect.DeepEqual(w.generator, tt.args.generator) {
				t.Errorf("SetWhereParametersOperatorGenerator() Generator is not set in main struct")
			}

			for i, processor := range tt.fields.processors {
				parsedProc := processor.(*whereParametersLogicOperationsGeneratorProcessorMock)
				if !reflect.DeepEqual(parsedProc.Generator, tt.args.generator) {
					t.Errorf("SetWhereParametersOperatorGenerator() Generator is not set in %v processor", i)
				}
			}
		})
	}
}

// Тестирование вызова процессоров из метода генерации
func Test_whereParametersLogicOperationsGenerator_GenerateParametersForObject(t *testing.T) {
	type fields struct {
		processors []whereParametersLogicOperationsGeneratorProcessorInterface
		generator  interfaces.WhereParametersGeneratorInterface
		maxLevel   uint8
	}
	type args struct {
		object     *graphql.Object
		level      uint8
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *graphql.InputObjectConfigFieldMap
	}{
		{
			name: "Тестирование вызова процессоров из метода генерации",
			fields: fields{
				processors: []whereParametersLogicOperationsGeneratorProcessorInterface{
					&whereParametersLogicOperationsGeneratorProcessorMock{},
					&whereParametersLogicOperationsGeneratorProcessorMock{},
					&whereParametersLogicOperationsGeneratorProcessorMock{},
				},
				generator: nil,
				maxLevel:  100,
			},
			args: args{
				object:     &graphql.Object{},
				level:      0,
				namePrefix: "test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := whereParametersLogicOperationsGenerator{
				processors: tt.fields.processors,
				generator:  tt.fields.generator,
				maxLevel:   tt.fields.maxLevel,
			}

			w.GenerateParametersForObject(context.Background(), tt.args.object, tt.args.level, tt.args.namePrefix)
			for i, processor := range tt.fields.processors {
				proc := processor.(*whereParametersLogicOperationsGeneratorProcessorMock)
				if proc.OperationCodeIsCalled == false {
					t.Errorf("GenerateParametersForObject() OperationCode() is not called for %v processor", i)
				}

				if proc.GenerateIsCalled == false {
					t.Errorf("GenerateParametersForObject() Generate() is not called for %v processor", i)
				}
			}
		})
	}
}
