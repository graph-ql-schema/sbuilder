package sbuilder

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/wsResolveGenerator"
	"context"
	"github.com/functionalfoundry/graphqlws"
	"github.com/graphql-go/graphql"
)

// Конфигурация GraphQL сервера
type GraphQlServerConfig struct {
	Host   string
	Port   uint16
	Uri    string
	WsPort uint16
	WsUri  string
}

// Сервер GraphQL API
type GraphQlServerInterface interface {
	// Запуск сервера
	Run() error

	// Остановка сервера
	GracefulShutdown()

	// Возвращает статус состояния запуска сервиса
	IsStarted() bool

	// Возвращает статус сервиса: живой или нет
	IsAlive() bool
}

// Тип, описывающий фабрику сервера
type tServerFactory = func(
	configuration GraphQlServerConfig,
	schema graphql.Schema,
	middleware ...Middleware,
) GraphQlServerInterface

// Тип, описывающий фабрику WS сервера
type tWsServerFactory = func(
	configuration GraphQlServerConfig,
	sManager graphqlws.SubscriptionManager,
	authCallback tAuthCallback,
) GraphQlServerInterface

// Тип, описывающий фабрику WS диспетчера
type tWsDispatcherFactory = func(
	subscriptionManager graphqlws.SubscriptionManager,
	schema graphql.Schema,
) WsDispatcherInterface

// Callback функция проверки авторизации для WS
type tAuthCallback = func(token string) (interface{}, error)

// Диспетчер уведомлений для WS сервера
type WsDispatcherInterface interface {
	// Отправка события всем подписчикам
	// При отправке данных будет отправлена соответствующая нотификация
	// всем зарегистрированным пользователям на сервере WS
	DispatchEvent(
		ctx context.Context,
		object *graphql.Object,
		eventType types.TEventType,
		entityId string,
		entityData interface{},
	) error

	// Получение текущего менеджера подписок на события
	GetManager() graphqlws.SubscriptionManager
}

// Интерфейс билдера GraphQL сервера
type GraphQlSchemaBuilderInterface interface {
	// Регистрация сущности для обработки сервером.
	// Метод генерирует валидное GraphQL API для данной сущности при помощи переданных параметров
	// запросов (queries), где в качестве ключа передается тип запроса для генерации, а в качестве
	// значения - резолвер сущности.
	RegisterEntity(object *graphql.Object, queries EntityQueries)

	// Регистрация сущности как метод подписки на события изменения сущности
	// Метод генерирует валидную подписку для отслеживания событий изменения данных
	// сущности. По сути позволяет в дальнейшем получать все виды нотификаций об
	// изменениях для переданной сущности.
	RegisterEntityChangeSubscription(object *graphql.Object, middleware wsResolveGenerator.TSubscriptionMiddleware)

	// Регистрация базового запроса без какой либо обработки со стороны сервиса
	// Метод предназначен для регистрации кастома GraphQL API, не входящего в библиотеку
	RegisterQuery(key string, field *graphql.Field)

	// Регистрация базовой мутации без какой либо обработки со стороны сервиса
	// Метод предназначен для регистрации кастома GraphQL API, не входящего в библиотеку
	RegisterMutation(key string, field *graphql.Field)

	// Регистрация базовой подписки без какой либо обработки со стороны сервиса
	// Метод предназначен для регистрации кастома GraphQL API, не входящего в библиотеку
	RegisterSubscription(key string, field *graphql.Field)

	// Установка кастомных параметров конфигурации сервера
	// По умолчанию используются:
	// 	- Host: 0.0.0.0
	//  - Port: 8080
	//  - Uri: /graphql
	SetServerConfig(config GraphQlServerConfig)

	// Создание экземпляра сервера для обработки GraphQL запросов.
	BuildServer(middleware ...Middleware) (GraphQlServerInterface, error)

	// Создание экземпляра сервера для обработки WS запросов к GraphQL
	BuildWsServer(authCallback tAuthCallback) (GraphQlServerInterface, error)

	// Создание сервиса доставки уведомлений до конечного пользователя по WS
	BuildWsDispatcher() (WsDispatcherInterface, error)
}

// Middleware описывает интерфейс посредника для сервера GraphQL
type Middleware interface {
	// Run обработка результатов
	Run(next func(params graphql.Params) *graphql.Result) func(params graphql.Params) *graphql.Result
}
