package whereParametersOperatorGenerator

import (
	"context"
	"github.com/graphql-go/graphql"
)

// Подставка для тестирования генератора
type whereParametersOperatorGeneratorProcessorMock struct {
	operation          string
	isAvailable        bool
	isGenerationCalled bool
	generationResult   *graphql.InputObjectFieldConfig
}

// Проверка доступности процессора
func (w whereParametersOperatorGeneratorProcessorMock) IsAvailable(context.Context, *graphql.FieldDefinition, uint8) bool {
	return w.isAvailable
}

// Код оператора, для вставки в схему GraphQL
func (w whereParametersOperatorGeneratorProcessorMock) OperatorCode() string {
	return w.operation
}

// Генерация параметров
func (w *whereParametersOperatorGeneratorProcessorMock) Generate(context.Context, *graphql.FieldDefinition, uint8, string) *graphql.InputObjectFieldConfig {
	w.isGenerationCalled = true

	return w.generationResult
}
