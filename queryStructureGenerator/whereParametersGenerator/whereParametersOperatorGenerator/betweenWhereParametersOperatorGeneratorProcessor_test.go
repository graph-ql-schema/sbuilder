package whereParametersOperatorGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование проверки доступности процессора
func Test_betweenWhereParametersOperatorGeneratorProcessor_IsAvailable(t *testing.T) {
	type fields struct {
		availableTypes  []graphql.Type
		maxLevel        uint8
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		field *graphql.FieldDefinition
		level uint8
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на валидном типе с валидным уровнем",
			fields: fields{
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.Int,
				},
				level: 1,
			},
			want: true,
		},
		{
			name: "Тестирование на валидном типе с не валидным уровнем",
			fields: fields{
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.Int,
				},
				level: 101,
			},
			want: false,
		},
		{
			name: "Тестирование на не валидном типе с валидным уровнем",
			fields: fields{
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.Boolean,
				},
				level: 1,
			},
			want: false,
		},
		{
			name: "Тестирование на не валидном типе с не валидным уровнем",
			fields: fields{
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.Boolean,
				},
				level: 101,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenWhereParametersOperatorGeneratorProcessor{
				availableTypes:  tt.fields.availableTypes,
				maxLevel:        tt.fields.maxLevel,
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}
			if got := b.IsAvailable(context.Background(), tt.args.field, tt.args.level); got != tt.want {
				t.Errorf("IsAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование полученного кода операции
func Test_betweenWhereParametersOperatorGeneratorProcessor_OperatorCode(t *testing.T) {
	type fields struct {
		availableTypes  []graphql.Type
		maxLevel        uint8
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование полученного кода операции",
			fields: fields{
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			want: constants.BetweenSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenWhereParametersOperatorGeneratorProcessor{
				availableTypes:  tt.fields.availableTypes,
				maxLevel:        tt.fields.maxLevel,
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}
			if got := b.OperatorCode(); got != tt.want {
				t.Errorf("OperatorCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генератора
func Test_betweenWhereParametersOperatorGeneratorProcessor_Generate(t *testing.T) {
	type fields struct {
		availableTypes  []graphql.Type
		maxLevel        uint8
		logger          *logrus.Entry
		fieldTypeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		field      *graphql.FieldDefinition
		level      uint8
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   graphql.Type
	}{
		{
			name: "Тестирование сгенерированного типа на базовом типе: Int",
			fields: fields{
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.Int,
					Name: "Test",
				},
				level:      1,
				namePrefix: "test",
			},
			want: graphql.Int,
		},
		{
			name: "Тестирование сгенерированного типа на базовом типе: NotNull Int",
			fields: fields{
				availableTypes: []graphql.Type{
					graphql.Int,
				},
				maxLevel:        100,
				logger:          helpers.NewLogger(`test`),
				fieldTypeGetter: &for_tests.GraphQlRootTypeGetterMock{},
			},
			args: args{
				field: &graphql.FieldDefinition{
					Type: graphql.NewNonNull(graphql.Int),
					Name: "Test",
				},
				level:      1,
				namePrefix: "test",
			},
			want: graphql.NewNonNull(graphql.Int),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenWhereParametersOperatorGeneratorProcessor{
				availableTypes:  tt.fields.availableTypes,
				maxLevel:        tt.fields.maxLevel,
				logger:          tt.fields.logger,
				fieldTypeGetter: tt.fields.fieldTypeGetter,
			}

			got := b.Generate(context.Background(), tt.args.field, tt.args.level, tt.args.namePrefix)
			if gType, ok := got.Type.(*graphql.InputObject); ok {
				fields := gType.Fields()
				for _, code := range []string{"_from", "_to"} {
					if val, ok := fields[code]; ok {
						if !reflect.DeepEqual(val.Type, tt.want) {
							t.Errorf("Generate() returns object type: %v, subfield: %v with type: %v, want: %v", gType, code, val.Type, tt.want)
						}
					} else {
						t.Errorf("Generate() returns object type: %v, without subfield: %v", gType, code)
					}
				}
			} else {
				t.Errorf("Generate() returns not object type: %v", gType)
			}
		})
	}
}
