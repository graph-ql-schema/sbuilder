package resolverGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"github.com/graphql-go/graphql"
)

// Генератор резолверов
type resolverGenerator struct {
	argumentsService argumentsParser.ArgumentsParserInterface
	fieldsService    requestedFieldsParser.RequestedFieldsParserInterface
}

// Генерация резолвера
func (r resolverGenerator) Generate(
	ctx context.Context,
	object *graphql.Object,
	resolver types.RequestResolver,
) TResolver {
	return func(params graphql.ResolveParams) (i interface{}, err error) {
		args, err := r.argumentsService.Parse(ctx, params.Args)
		if nil != err {
			return nil, fmt.Errorf(`resolve: %v`, err.Error())
		}

		fields, err := r.fieldsService.ParseRequest(ctx, params)
		if nil != err {
			return nil, fmt.Errorf(`resolve: %v`, err.Error())
		}

		return resolver(types.Parameters{
			Arguments:     *args,
			Context:       params.Context,
			Fields:        fields,
			GraphQlObject: object,
			GraphqlParams: params,
		})
	}
}

// Фабрика генератора резолверов
func NewResolverGenerator() ResolverGeneratorInterface {
	return &resolverGenerator{
		argumentsService: argumentsParser.NewArgumentsParser(),
		fieldsService:    requestedFieldsParser.NewRequestedFieldsParser(),
	}
}

// Фабрика генератора резолверов с кастомизацией
func NewResolverGeneratorWithCustomizations(customizations []customizationService.CustomizationInterface) ResolverGeneratorInterface {
	return &resolverGenerator{
		argumentsService: argumentsParser.NewArgumentsParserWithCustomizations(customizations),
		fieldsService:    requestedFieldsParser.NewRequestedFieldsParser(),
	}
}
