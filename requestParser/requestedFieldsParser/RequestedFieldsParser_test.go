package requestedFieldsParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

func Test_requestedFieldsParser_ParseRequest(t *testing.T) {
	type fields struct {
		logger        *logrus.Entry
		fieldsFactory TFieldFactory
	}
	type args struct {
		params graphql.ResolveParams
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    GraphQlRequestedFields
		wantErr bool
	}{
		{
			name: "Поля не определены",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: nil,
					},
				},
			},
			want:    GraphQlRequestedFields{},
			wantErr: false,
		},
		{
			name: "Поля не переданны",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: []*ast.Field{},
					},
				},
			},
			want:    GraphQlRequestedFields{},
			wantErr: false,
		},
		{
			name: "Передано одно поле",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: []*ast.Field{
							{
								Name: &ast.Name{
									Value: "query",
								},
								SelectionSet: &ast.SelectionSet{
									Selections: []ast.Selection{
										&ast.Field{
											Name: &ast.Name{
												Value: "test",
											},
											SelectionSet: nil,
										},
									},
								},
							},
						},
					},
				},
			},
			want: GraphQlRequestedFields{
				&graphQlRequestedFieldStub{
					fieldName: "test",
					subFields: GraphQlRequestedFields{},
				},
			},
			wantErr: false,
		},
		{
			name: "Передано одно поле с дочерним полем",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: []*ast.Field{
							{
								Name: &ast.Name{
									Value: "query",
								},
								SelectionSet: &ast.SelectionSet{
									Selections: []ast.Selection{
										&ast.Field{
											Name: &ast.Name{
												Value: "test",
											},
											SelectionSet: &ast.SelectionSet{
												Selections: []ast.Selection{
													&ast.Field{
														Name: &ast.Name{
															Value: "data",
														},
														SelectionSet: nil,
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			want: GraphQlRequestedFields{
				&graphQlRequestedFieldStub{
					fieldName: "test",
					subFields: GraphQlRequestedFields{
						&graphQlRequestedFieldStub{
							fieldName: "data",
							subFields: GraphQlRequestedFields{},
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Передано два поля с дочерним полем",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: []*ast.Field{
							{
								Name: &ast.Name{
									Value: "query",
								},
								SelectionSet: &ast.SelectionSet{
									Selections: []ast.Selection{
										&ast.Field{
											Name: &ast.Name{
												Value: "test",
											},
											SelectionSet: &ast.SelectionSet{
												Selections: []ast.Selection{
													&ast.Field{
														Name: &ast.Name{
															Value: "data",
														},
														SelectionSet: nil,
													},
												},
											},
										},
										&ast.Field{
											Name: &ast.Name{
												Value: "test-2",
											},
											SelectionSet: &ast.SelectionSet{
												Selections: []ast.Selection{
													&ast.Field{
														Name: &ast.Name{
															Value: "data-2",
														},
														SelectionSet: nil,
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			want: GraphQlRequestedFields{
				&graphQlRequestedFieldStub{
					fieldName: "test",
					subFields: GraphQlRequestedFields{
						&graphQlRequestedFieldStub{
							fieldName: "data",
							subFields: GraphQlRequestedFields{},
						},
					},
				},
				&graphQlRequestedFieldStub{
					fieldName: "test-2",
					subFields: GraphQlRequestedFields{
						&graphQlRequestedFieldStub{
							fieldName: "data-2",
							subFields: GraphQlRequestedFields{},
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Передан фрагмент без данных",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: []*ast.Field{
							{
								Name: &ast.Name{
									Value: "query",
								},
								SelectionSet: &ast.SelectionSet{
									Selections: []ast.Selection{
										&ast.FragmentSpread{
											Name: &ast.Name{
												Value: "test",
											},
										},
									},
								},
							},
						},
						Fragments: map[string]ast.Definition{},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Передан фрагмент с дочерним полем",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: []*ast.Field{
							{
								Name: &ast.Name{
									Value: "query",
								},
								SelectionSet: &ast.SelectionSet{
									Selections: []ast.Selection{
										&ast.FragmentSpread{
											Name: &ast.Name{
												Value: "test",
											},
										},
									},
								},
							},
						},
						Fragments: map[string]ast.Definition{
							"test": &ast.FragmentDefinition{
								SelectionSet: &ast.SelectionSet{
									Selections: []ast.Selection{
										&ast.Field{
											Name: &ast.Name{
												Value: "data",
											},
											SelectionSet: nil,
										},
									},
								},
							},
						},
					},
				},
			},
			want: GraphQlRequestedFields{
				&graphQlRequestedFieldStub{
					fieldName: "test",
					subFields: GraphQlRequestedFields{
						&graphQlRequestedFieldStub{
							fieldName: "data",
							subFields: GraphQlRequestedFields{},
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Передан фрагмент",
			fields: fields{
				logger:        helpers.NewLogger(`test`),
				fieldsFactory: newGraphQlRequestedFieldStub,
			},
			args: args{
				params: graphql.ResolveParams{
					Info: graphql.ResolveInfo{
						FieldASTs: []*ast.Field{
							{
								Name: &ast.Name{
									Value: "query",
								},
								SelectionSet: &ast.SelectionSet{
									Selections: []ast.Selection{
										&ast.FragmentSpread{
											Name: &ast.Name{
												Value: "test",
											},
										},
									},
								},
							},
						},
						Fragments: map[string]ast.Definition{
							"test": &ast.FragmentDefinition{
								SelectionSet: nil,
							},
						},
					},
				},
			},
			want: GraphQlRequestedFields{
				&graphQlRequestedFieldStub{
					fieldName: "test",
					subFields: GraphQlRequestedFields{},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := requestedFieldsParser{
				logger:        tt.fields.logger,
				fieldsFactory: tt.fields.fieldsFactory,
			}
			got, err := r.ParseRequest(context.Background(), tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseRequest() got = %v, want %v", got, tt.want)
			}
		})
	}
}
