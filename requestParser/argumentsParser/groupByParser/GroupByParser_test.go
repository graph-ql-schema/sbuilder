package groupByParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

// Тестирование парсинга параметров группировки
func Test_groupByParser_Parse(t *testing.T) {
	type fields struct {
		logger *logrus.Entry
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    GroupBy
		wantErr bool
	}{
		{
			name: "Тестирование на отсутствующей коллекции",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на пустой коллекции",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на коллекции с не корректным ключем",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"group": []string{
						"test",
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на коллекции с корректным ключем и не корректным значением",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					constants.GroupBySchemaKey: []int{
						1,
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на коллекции с корректным ключем и не корректным типом значения",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					constants.GroupBySchemaKey: 1,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на коллекции корректной коллекции",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					constants.GroupBySchemaKey: []string{
						"test",
					},
				},
			},
			want: GroupBy{
				"test",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupByParser{
				logger: tt.fields.logger,
			}
			got, err := g.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() got = %v, want %v", got, tt.want)
			}
		})
	}
}
