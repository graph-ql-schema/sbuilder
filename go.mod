module bitbucket.org/graph-ql-schema/sbuilder/v2

go 1.14

require (
	bitbucket.org/graph-ql-schema/gql-root-type-getter v1.3.2
	bitbucket.org/graph-ql-schema/gql-sql-converter v1.2.6
	bitbucket.org/graph-ql-schema/nullable v1.0.5
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/functionalfoundry/graphqlws v0.0.0-20200611113535-7bc58903ce7b
	github.com/google/uuid v1.1.2 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/graphql-go/graphql v0.7.9
	github.com/jmoiron/sqlx v1.2.0
	github.com/klauspost/compress v1.11.1 // indirect
	github.com/lib/pq v1.5.2
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.4 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/valyala/fasthttp v1.16.0
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f
	golang.org/x/sys v0.0.0-20201004230629-f6757f270073 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
