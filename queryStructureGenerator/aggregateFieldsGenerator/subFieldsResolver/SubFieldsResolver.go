package subFieldsResolver

import (
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/graphql-go/graphql"
)

// Резолвер поля для аггрегирующей сущности
type subFieldsResolver struct {
	fieldCode string
	fieldType graphql.Type
}

// Резолвинг полей для аггрегирующей сущности
func (s subFieldsResolver) Resolve(p graphql.ResolveParams) (interface{}, error) {
	if p.Source == nil {
		return nil, nil
	}

	if reflect.TypeOf(p.Source).Kind() != reflect.Map {
		data, err := json.Marshal(p.Source)
		if nil != err {
			return nil, nil
		}

		var resultMap map[string]interface{}
		err = json.Unmarshal(data, &resultMap)

		if nil != err {
			return nil, nil
		}

		p.Source = resultMap
	}

	value := reflect.ValueOf(p.Source)
	for _, mapKey := range value.MapKeys() {
		key := fmt.Sprintf("%s", mapKey.Interface())
		if key != s.fieldCode {
			continue
		}

		value := value.MapIndex(mapKey).Interface()
		return s.uniqueData(value), nil
	}

	return nil, nil
}

// Уникализирует полученные данные
func (s subFieldsResolver) uniqueData(data interface{}) interface{} {
	if reflect.TypeOf(data).Kind() != reflect.Slice {
		return data
	}

	result := []interface{}{}
	valueOfSlice := reflect.ValueOf(data)
	for i := 0; i < valueOfSlice.Len(); i++ {
		val := valueOfSlice.Index(i).Interface()
		if s.fieldType == graphql.DateTime {
			// Если поле имеет тип DateTime и получено строковове значение, то парсим его
			if _, ok := val.(time.Time); !ok {
				var err error
				val, err = time.Parse(time.RFC3339, fmt.Sprintf(`%v`, val))

				// Пропускаем не распарсенные значения
				if nil != err {
					continue
				}
			}
		}

		if s.inArray(val, result) {
			continue
		}

		result = append(result, val)
	}

	return result
}

// Проверка наличия переданного значения в массиве. Возвращает TRUE, если элемент уже в массиве.
//
// Если передан массив дат, то каждый элемент приводится к строке для корректного сравнения, аналогично
// и искомый элемент.
func (s subFieldsResolver) inArray(item interface{}, data []interface{}) bool {
	if itemTime, ok := item.(time.Time); ok {
		item = itemTime.Format(time.RFC3339)
	}

	for _, existsVal := range data {
		if existsValTime, ok := existsVal.(time.Time); ok {
			existsVal = existsValTime.Format(time.RFC3339)
		}

		if existsVal == item {
			return true
		}
	}

	return false
}

// Фабрика резолвера
func newSubFieldsResolver(fieldCode string, fieldType graphql.Type) SubFieldsResolverInterface {
	return &subFieldsResolver{
		fieldCode: fieldCode,
		fieldType: fieldType,
	}
}

// Фабрика резолвера
func SubFieldsResolverFactory(fieldCode string, fieldType graphql.Type) func(p graphql.ResolveParams) (interface{}, error) {
	return newSubFieldsResolver(fieldCode, fieldType).Resolve
}
