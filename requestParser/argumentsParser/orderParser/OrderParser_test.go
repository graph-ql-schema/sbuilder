package orderParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"
)

func Test_orderParser_Parse(t *testing.T) {
	type fields struct {
		logger *logrus.Entry
	}
	type args struct {
		arguments map[string]interface{}
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantOrder []Order
		wantErr   bool
	}{
		{
			name: "Тестирование без передачи параметров сортировки",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{},
			},
			wantOrder: nil,
			wantErr:   false,
		},
		{
			name: "Тестирование с передачей сортировки в не корректном формате",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"order": "title",
				},
			},
			wantOrder: nil,
			wantErr:   true,
		},
		{
			name: "Тестирование с передачей сортировки без полного набора полей",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"order": []interface{}{
						map[string]interface{}{
							"by":        "title",
							"direction": "asc",
						},
					},
				},
			},
			wantOrder: []Order{},
			wantErr:   false,
		},
		{
			name: "Тестирование с передачей сортировки c полным набором полей и не валидным типом",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"order": []interface{}{
						map[string]interface{}{
							"by":        "title",
							"direction": "asc",
							"priority":  "test",
						},
					},
				},
			},
			wantOrder: []Order{},
			wantErr:   false,
		},
		{
			name: "Тестирование с передачей сортировки c полным набором полей и не валидным значением",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"order": []interface{}{
						map[string]interface{}{
							"by":        "title",
							"direction": "asc",
							"priority":  -100,
						},
					},
				},
			},
			wantOrder: []Order{},
			wantErr:   false,
		},
		{
			name: "Тестирование с передачей сортировки c полным набором полей",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"order": []interface{}{
						map[string]interface{}{
							"by":        "title",
							"direction": "asc",
							"priority":  100,
						},
					},
				},
			},
			wantOrder: []Order{
				{
					Priority:  100,
					By:        "title",
					Direction: "asc",
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование с передачей 2 сортировок",
			fields: fields{
				logger: helpers.NewLogger(`test`),
			},
			args: args{
				arguments: map[string]interface{}{
					"order": []interface{}{
						map[string]interface{}{
							"by":        "title",
							"direction": "asc",
							"priority":  100,
						},
						map[string]interface{}{
							"by":        "name",
							"direction": "desc",
							"priority":  10,
						},
					},
				},
			},
			wantOrder: []Order{
				{
					Priority:  100,
					By:        "title",
					Direction: "asc",
				},
				{
					Priority:  10,
					By:        "name",
					Direction: "desc",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := orderParser{
				logger: tt.fields.logger,
			}
			gotOrder, err := o.Parse(context.Background(), tt.args.arguments)
			if (err != nil) != tt.wantErr {
				t.Errorf("Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOrder, tt.wantOrder) {
				t.Errorf("Parse() gotOrder = %v, want %v", gotOrder, tt.wantOrder)
			}
		})
	}
}
