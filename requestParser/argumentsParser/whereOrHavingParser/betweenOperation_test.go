package whereOrHavingParser

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"github.com/graphql-go/graphql"
)

// Тестирование получение типа
func Test_betweenOperation_Type(t *testing.T) {
	type fields struct {
		value          BetweenValue
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тестирование получения типа операции",
			fields: fields{
				field:          "",
				valueConverter: nil,
				value: BetweenValue{
					From: 1,
					To:   2,
				},
			},
			want: constants.BetweenSchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenOperation{
				value:          tt.fields.value,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			if got := b.Type(); got != tt.want {
				t.Errorf("Type() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения значения
func Test_betweenOperation_Value(t *testing.T) {
	type fields struct {
		value          BetweenValue
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   interface{}
	}{
		{
			name: "Тестирование получения значения",
			fields: fields{
				field:          "",
				valueConverter: nil,
				value: BetweenValue{
					From: 1,
					To:   2,
				},
			},
			want: BetweenValue{
				From: 1,
				To:   2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenOperation{
				value:          tt.fields.value,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			if got := b.Value(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Value() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тетсирование получения кода поля
func Test_betweenOperation_Field(t *testing.T) {
	type fields struct {
		value          BetweenValue
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Тетсирование получения кода поля",
			fields: fields{
				field:          "test",
				valueConverter: nil,
				value: BetweenValue{
					From: 1,
					To:   2,
				},
			},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenOperation{
				value:          tt.fields.value,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			if got := b.Field(); got != tt.want {
				t.Errorf("Field() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации SQL
func Test_betweenOperation_ToSQL(t *testing.T) {
	type fields struct {
		value          BetweenValue
		field          string
		valueConverter gql_sql_converter.GraphQlSqlConverterInterface
	}
	type args struct {
		object    *graphql.Object
		fieldsMap map[string]string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование с возвращением ошибки от конвертера значения",
			fields: fields{
				field: "test",
				valueConverter: &for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: true,
					ToSQLValueRes: "",
				},
				value: BetweenValue{
					From: 1,
					To:   2,
				},
			},
			args: args{
				object:    nil,
				fieldsMap: nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование без ошибки от конвертера значения",
			fields: fields{
				field: "test",
				valueConverter: &for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: false,
					ToSQLValueRes: "10",
				},
				value: BetweenValue{
					From: 1,
					To:   2,
				},
			},
			args: args{
				object:    nil,
				fieldsMap: nil,
			},
			want:    "test between 10 and 10",
			wantErr: false,
		},
		{
			name: "Тестирование с подменой кода поля",
			fields: fields{
				field: "test",
				valueConverter: &for_tests.GraphQlSqlConverterMock{
					ToBaseTypeErr: false,
					ToBaseTypeRes: nil,
					ToSQLValueErr: false,
					ToSQLValueRes: "10",
				},
				value: BetweenValue{
					From: 1,
					To:   2,
				},
			},
			args: args{
				object: nil,
				fieldsMap: map[string]string{
					"test": "mapped",
				},
			},
			want:    "mapped between 10 and 10",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := betweenOperation{
				value:          tt.fields.value,
				field:          tt.fields.field,
				valueConverter: tt.fields.valueConverter,
			}
			got, err := b.ToSQL(tt.args.object, tt.args.fieldsMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ToSQL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ToSQL() got = %v, want %v", got, tt.want)
			}
		})
	}
}
