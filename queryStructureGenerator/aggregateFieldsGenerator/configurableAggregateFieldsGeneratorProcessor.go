package aggregateFieldsGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/aggregateFieldsGenerator/subFieldsResolver"
	"github.com/graphql-go/graphql"
)

type tOperatorValueDescriptionGenerator = func(valueCode string, object *graphql.Object) string
type tDescriptionGenerator = func(object *graphql.Object) string

// Настраиваемый процессор для генерации полей аггрегирующей сущности
type configurableAggregateFieldsGeneratorProcessor struct {
	logger                    *logrus.Entry
	fieldTypeGetter           gql_root_type_getter.GraphQlRootTypeGetterInterface
	operatorType              string
	operatorDescription       tDescriptionGenerator
	valueDescriptionGenerator tOperatorValueDescriptionGenerator
	availableTypes            []graphql.Type
	fieldsResolverFactory     subFieldsResolver.TSubFieldsResolverFactory
}

// Генерация поля для сущности
func (c configurableAggregateFieldsGeneratorProcessor) GenerateField(ctx context.Context, object *graphql.Object) (*graphql.Field, string) {
	fields := c.getAvailableFields(object)
	if 0 == len(fields) {
		return nil, ""
	}

	c.logger.WithFields(logrus.Fields{
		"code":   100,
		"field":  c.operatorType,
		"object": object.Name(),
	}).Debug(fmt.Sprintf(`Generation aggregation field '%v'`, c.operatorType))

	namePrefix := fmt.Sprintf(
		`%v_aggregate_%v_field`,
		strings.ReplaceAll(strings.ToLower(object.Name()), ` `, `_`),
		strings.ToLower(c.operatorType),
	)

	fieldsConf := graphql.Fields{}
	for code, field := range fields {
		fType := c.fieldTypeGetter.GetRootType(field.Type)
		if fType == graphql.Int {
			fType = graphql.Float
		}

		fieldsConf[code] = &graphql.Field{
			Type:        fType,
			Name:        fmt.Sprintf(`%v_value_%v`, namePrefix, code),
			Description: c.valueDescriptionGenerator(code, object),
			Resolve:     c.fieldsResolverFactory(code, fType),
		}
	}

	return &graphql.Field{
		Type: graphql.NewNonNull(graphql.NewObject(
			graphql.ObjectConfig{
				Name:        fmt.Sprintf(`%v_response_type`, namePrefix),
				Fields:      fieldsConf,
				Description: c.operatorDescription(object),
			},
		)),
		Name:        fmt.Sprintf(`%v_type`, namePrefix),
		Description: c.operatorDescription(object),
	}, c.operatorType
}

// Получение доступных для генерации полей
func (c configurableAggregateFieldsGeneratorProcessor) getAvailableFields(object *graphql.Object) map[string]*graphql.FieldDefinition {
	result := map[string]*graphql.FieldDefinition{}
	for code, field := range object.Fields() {
		if nil != field.Resolve {
			continue
		}

		fType := c.fieldTypeGetter.GetRootType(field.Type)
		if false == c.checkTypeIsAvailable(fType) {
			continue
		}

		result[code] = field
	}

	return result
}

// Проверка типа на доступность для генерации
func (c configurableAggregateFieldsGeneratorProcessor) checkTypeIsAvailable(t graphql.Type) bool {
	for _, aType := range c.availableTypes {
		if aType == t {
			return true
		}
	}

	return false
}

// Фабрика процессора генерации
func newConfigurableAggregateFieldsGeneratorProcessor(
	operatorType string,
	operatorDescription tDescriptionGenerator,
	valueDescriptionGenerator tOperatorValueDescriptionGenerator,
	availableTypes []graphql.Type,
) aggregateFieldsGeneratorProcessorInterface {
	return &configurableAggregateFieldsGeneratorProcessor{
		logger:                    helpers.NewLogger(fmt.Sprintf(`configurableAggregateFieldsGeneratorProcessor(Type: %v)`, operatorType)),
		fieldTypeGetter:           gql_root_type_getter.GraphQlRootTypeGetterFactory(),
		operatorType:              operatorType,
		operatorDescription:       operatorDescription,
		valueDescriptionGenerator: valueDescriptionGenerator,
		availableTypes:            availableTypes,
		fieldsResolverFactory:     subFieldsResolver.SubFieldsResolverFactory,
	}
}
