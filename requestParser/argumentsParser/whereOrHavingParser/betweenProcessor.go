package whereOrHavingParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
)

type betweenDataType = map[string]interface{}

// Процессор BETWEEN сравнения
type betweenProcessor struct {
	logger  *logrus.Entry
	factory TNewBetweenOperation
}

// Установка парсера в процессор
func (b betweenProcessor) SetParser(WhereOrHavingParserInterface) {}

// Проверка доступности процессора
func (b betweenProcessor) IsAvailable(ctx context.Context, arguments map[string]interface{}) bool {
	for _, data := range arguments {
		if parsedData, ok := data.(betweenDataType); ok {
			if _, ok := parsedData[constants.BetweenSchemaKey]; !ok {
				return false
			}

			for _, itemsData := range parsedData {
				if _, ok = itemsData.(betweenDataType); ok {
					return true
				}
			}
		}
	}

	return false
}

// Парсинг параметров фильтрации процессором
func (b betweenProcessor) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (operation Operation, err error) {
	b.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Started filter between parameters parsing")

	var items betweenDataType
	var operationCode string
	var field string

	for code, argument := range arguments {
		if parsedArgument, ok := argument.(betweenDataType); ok {
			for operation, itemsData := range parsedArgument {
				if parsedItemsData, ok := itemsData.(betweenDataType); ok {
					items = parsedItemsData
					field = code
					operationCode = operation

					break
				}
			}
		}
	}

	if nil == items {
		err = fmt.Errorf(`no available structures passed to between operation processor`)
		b.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	if operationCode != constants.BetweenSchemaKey {
		err = fmt.Errorf(`passed operation code is incorrect (between operation processor)`)
		b.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	var operationValue BetweenValue

	from, ok := items[`_from`]
	if !ok {
		err = fmt.Errorf(`'from' parameter is not passed to between operation processor`)
		b.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	to, ok := items[`_to`]
	if !ok {
		err = fmt.Errorf(`'to' parameter is not passed to between operation processor`)
		b.logger.WithFields(logrus.Fields{
			"code":      400,
			"arguments": arguments,
		}).Warning(err.Error())

		return nil, err
	}

	operationValue = BetweenValue{
		From: from,
		To:   to,
	}

	val := b.factory(
		operationValue,
		field,
	)

	b.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
		"value":     val,
	}).Debug("Parsed filter between value")

	return val, nil
}

// Фабрика процессора
func newBetweenOperationProcessor() whereOrHavingParserProcessorInterface {
	return &betweenProcessor{
		logger:  helpers.NewLogger(`betweenProcessor`),
		factory: NewBetweenOperation,
	}
}
