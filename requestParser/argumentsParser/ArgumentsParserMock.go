package argumentsParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования
type ArgumentsParserMock struct {
	Result *ParsedArguments
	IsErr  bool
}

// Парсинг аргументов запроса
func (a ArgumentsParserMock) Parse(
	context.Context,
	map[string]interface{},
) (*ParsedArguments, error) {
	if a.IsErr {
		return nil, fmt.Errorf(`test`)
	}

	return a.Result, nil
}
