package queryGenerator

import (
	"context"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/for-tests"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/queryStructureGenerator/orderParametersGenerator"
	"github.com/graphql-go/graphql"
)

// Тестирование получение названия
func Test_orderGeneratorProcessor_Name(t *testing.T) {
	type fields struct {
		orderGenerator orderParametersGenerator.OrderParametersGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "Тестирование получение названия",
			fields: fields{},
			want:   constants.OrderBySchemaKey,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := orderGeneratorProcessor{
				orderGenerator: tt.fields.orderGenerator,
			}
			if got := o.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_orderGeneratorProcessor_Generate(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "Role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.Int,
					Name: "field_1",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		orderGenerator orderParametersGenerator.OrderParametersGeneratorInterface
	}
	type args struct {
		object     *graphql.Object
		namePrefix string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				orderGenerator: &for_tests.OrderParametersGeneratorMock{},
			},
			args: args{
				object:     objectConfig,
				namePrefix: "role",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := orderGeneratorProcessor{
				orderGenerator: tt.fields.orderGenerator,
			}

			_ = o.Generate(context.Background(), tt.args.object, tt.args.namePrefix)
			orderGenerator := tt.fields.orderGenerator.(*for_tests.OrderParametersGeneratorMock)
			if !orderGenerator.IsCalled {
				t.Errorf("Generate() OrderParametersGenerator is not called")
			}
		})
	}
}
