package objectsParser

import (
	"context"
	"fmt"
)

// Подставка для тестирования
type ObjectsParserMock struct {
	Result Objects
	IsErr  bool
}

// Парсинг значений для вставки
func (o ObjectsParserMock) Parse(
	context.Context,
	map[string]interface{},
) (Objects, error) {
	if o.IsErr {
		return nil, fmt.Errorf(`test`)
	}

	return o.Result, nil
}
