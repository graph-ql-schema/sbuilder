package for_tests

import "github.com/graphql-go/graphql"

// Подставка для тестов
type PaginationParametersGeneratorMock struct {
	GenerateLimitIsCalled  bool
	GenerateOffsetIsCalled bool
}

// Генерация параметров "Limit"
func (p *PaginationParametersGeneratorMock) GenerateLimit() *graphql.ArgumentConfig {
	p.GenerateLimitIsCalled = true

	return &graphql.ArgumentConfig{}
}

// Генерация параметров "Offset"
func (p *PaginationParametersGeneratorMock) GenerateOffset() *graphql.ArgumentConfig {
	p.GenerateOffsetIsCalled = true

	return &graphql.ArgumentConfig{}
}
