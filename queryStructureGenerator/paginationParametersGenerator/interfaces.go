package paginationParametersGenerator

import "github.com/graphql-go/graphql"

// Генератор параметров пагинации
type PaginationParametersGeneratorInterface interface {
	// Генерация параметров "Limit"
	GenerateLimit() *graphql.ArgumentConfig

	// Генерация параметров "Offset"
	GenerateOffset() *graphql.ArgumentConfig
}

// Тип фабрики генератора пагенации
type TPaginationParametersGeneratorFactory = func() PaginationParametersGeneratorInterface
