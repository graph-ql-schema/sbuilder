package requestedFieldsParser

// Подставка для тестирования парсинга полей
type graphQlRequestedFieldStub struct {
	fieldName string
	subFields GraphQlRequestedFields
}

// Получение названия поля
func (g graphQlRequestedFieldStub) GetFieldName() string {
	return g.fieldName
}

// Есть ли у поля дочерние элементы
func (g graphQlRequestedFieldStub) HasSubFields() bool {
	return 0 != len(g.subFields)
}

// Получение дочерних элементов поля
func (g graphQlRequestedFieldStub) GetSubFields() []GraphQlRequestedFieldInterface {
	return g.subFields
}

// Фабрика подставок
func newGraphQlRequestedFieldStub(
	fieldName string,
	subFields GraphQlRequestedFields,
) GraphQlRequestedFieldInterface {
	return &graphQlRequestedFieldStub{
		fieldName: fieldName,
		subFields: subFields,
	}
}
