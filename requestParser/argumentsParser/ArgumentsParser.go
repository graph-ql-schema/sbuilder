package argumentsParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/groupByParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/orderParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/paginationParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/setParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/services/customizationService"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
)

// Парсер аргументов запроса
type argumentsParser struct {
	logger               *logrus.Entry
	objectsService       objectsParser.ObjectsParserInterface
	setService           setParser.SetParserInterface
	groupByService       groupByParser.GroupByParserInterface
	orderService         orderParser.OrderParserInterface
	paginationService    paginationParser.PaginationParserInterface
	whereOrHavingService whereOrHavingParser.WhereOrHavingParserInterface
}

// Парсинг аргументов запроса
func (a argumentsParser) Parse(
	ctx context.Context,
	arguments map[string]interface{},
) (*ParsedArguments, error) {
	a.logger.WithFields(logrus.Fields{
		"code":      100,
		"arguments": arguments,
	}).Debug("Request arguments parsing started")

	result := &ParsedArguments{}

	// Парсим объекты для вставки
	if _, ok := arguments[constants.ObjectsSchemaKey]; ok {
		val, err := a.objectsService.Parse(ctx, map[string]interface{}{
			constants.ObjectsSchemaKey: arguments[constants.ObjectsSchemaKey],
		})

		if nil != err {
			return nil, fmt.Errorf(`objects parsing: %v`, err.Error())
		}

		result.Objects = val
	}

	// Парсим значения для обновления
	if _, ok := arguments[constants.SetSchemaKey]; ok {
		val, err := a.setService.Parse(ctx, map[string]interface{}{
			constants.SetSchemaKey: arguments[constants.SetSchemaKey],
		})

		if nil != err {
			return nil, fmt.Errorf(`update values parsing: %v`, err.Error())
		}

		result.Set = val
	}

	// Парсим значения группировки
	if _, ok := arguments[constants.GroupBySchemaKey]; ok {
		val, err := a.groupByService.Parse(ctx, map[string]interface{}{
			constants.GroupBySchemaKey: arguments[constants.GroupBySchemaKey],
		})

		if nil != err {
			return nil, fmt.Errorf(`GroupBy parsing: %v`, err.Error())
		}

		result.GroupBy = val
	}

	// Парсим значения сортировки
	if _, ok := arguments[constants.OrderBySchemaKey]; ok {
		val, err := a.orderService.Parse(ctx, map[string]interface{}{
			constants.OrderBySchemaKey: arguments[constants.OrderBySchemaKey],
		})

		if nil != err {
			return nil, fmt.Errorf(`OrderBy parsing: %v`, err.Error())
		}

		result.OrderBy = val
	}

	// Парсим значения пагенации
	_, limitOk := arguments[constants.LimitSchemaKey]
	_, offsetOk := arguments[constants.OffsetSchemaKey]
	if offsetOk || limitOk {
		val, err := a.paginationService.Parse(ctx, arguments)
		if nil != err {
			return nil, fmt.Errorf(`pagination parsing: %v`, err.Error())
		}

		result.Pagination = val
	}

	// Парсим значения фильтрации Where
	if _, ok := arguments[constants.WhereSchemaKey]; ok {
		val, err := a.whereOrHavingService.Parse(ctx, map[string]interface{}{
			constants.WhereSchemaKey: arguments[constants.WhereSchemaKey],
		})

		if nil != err {
			return nil, fmt.Errorf(`where filter parsing: %v`, err.Error())
		}

		result.Where = val
	}

	// Парсим значения фильтрации Having
	if _, ok := arguments[constants.HavingSchemaKey]; ok {
		val, err := a.whereOrHavingService.Parse(ctx, map[string]interface{}{
			constants.HavingSchemaKey: arguments[constants.HavingSchemaKey],
		})

		if nil != err {
			return nil, fmt.Errorf(`having filter parsing: %v`, err.Error())
		}

		result.Having = val
	}

	return result, nil
}

// Фабрика парсера аргументов
func NewArgumentsParser() ArgumentsParserInterface {
	return &argumentsParser{
		logger:               helpers.NewLogger(`argumentsParser`),
		objectsService:       objectsParser.NewObjectsParser(),
		setService:           setParser.NewSetParser(),
		groupByService:       groupByParser.NewGroupByParser(),
		orderService:         orderParser.NewOrderParser(),
		paginationService:    paginationParser.NewPaginationParser(),
		whereOrHavingService: whereOrHavingParser.NewWhereOrHavingParser(),
	}
}

// Фабрика парсера аргументов
func NewArgumentsParserWithCustomizations(customizations []customizationService.CustomizationInterface) ArgumentsParserInterface {
	return &argumentsParser{
		logger:               helpers.NewLogger(`argumentsParser`),
		objectsService:       objectsParser.NewObjectsParser(),
		setService:           setParser.NewSetParser(),
		groupByService:       groupByParser.NewGroupByParser(),
		orderService:         orderParser.NewOrderParser(),
		paginationService:    paginationParser.NewPaginationParser(),
		whereOrHavingService: whereOrHavingParser.NewWhereOrHavingParserWithCustomizations(customizations),
	}
}
