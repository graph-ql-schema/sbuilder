package requestedFieldsParser

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

// Парсер полей запроса
type requestedFieldsParser struct {
	logger        *logrus.Entry
	fieldsFactory TFieldFactory
}

// Парсинг запроса
func (r requestedFieldsParser) ParseRequest(
	ctx context.Context,
	params graphql.ResolveParams,
) (GraphQlRequestedFields, error) {
	r.logger.WithFields(logrus.Fields{
		"code":   100,
		"params": params,
	}).Debug("Started request field parsing")

	fieldASTs := params.Info.FieldASTs
	if 0 == len(fieldASTs) {
		r.logger.WithFields(logrus.Fields{
			"code":   400,
			"params": params,
		}).Warning("Fields to parse is not found in request")

		return GraphQlRequestedFields{}, nil
	}

	result, err := r.getFieldsFromSelection(ctx, params, fieldASTs[0].SelectionSet.Selections)
	if nil != err {
		return nil, err
	}

	r.logger.WithFields(logrus.Fields{
		"code":   100,
		"params": params,
		"result": result,
	}).Debug("Parsed request fields")

	return result, nil
}

// Получение полей из переданного запроса
func (r requestedFieldsParser) getFieldsFromSelection(
	ctx context.Context,
	params graphql.ResolveParams,
	selections []ast.Selection,
) (GraphQlRequestedFields, error) {
	var err error
	fields := GraphQlRequestedFields{}

	for _, selection := range selections {
		switch selection := selection.(type) {
		case *ast.Field:
			subFields := GraphQlRequestedFields{}
			if selection.SelectionSet != nil {
				subFields, err = r.getFieldsFromSelection(ctx, params, selection.SelectionSet.Selections)
			}

			if nil != err {
				return nil, err
			}

			r.logger.WithFields(logrus.Fields{
				"code": 100,
			}).Debug(fmt.Sprintf("Parsed parameters for '%v' field", selection.Name.Value))

			fields = append(fields, r.fieldsFactory(selection.Name.Value, subFields))
			break
		case *ast.FragmentSpread:
			fieldName := selection.Name.Value
			frag, ok := params.Info.Fragments[fieldName]
			if !ok {
				err = fmt.Errorf("no fragment found for field with name '%v'", selection.Name.Value)
				r.logger.WithFields(logrus.Fields{
					"code": 400,
				}).Warning(err.Error())

				return nil, err
			}

			subFields := GraphQlRequestedFields{}
			if nil != frag.GetSelectionSet() {
				subFields, err = r.getFieldsFromSelection(ctx, params, frag.GetSelectionSet().Selections)
				if nil != err {
					return nil, err
				}
			}

			r.logger.WithFields(logrus.Fields{
				"code": 100,
			}).Debug(fmt.Sprintf("Parsed parameters for '%v' field", selection.Name.Value))

			fields = append(fields, r.fieldsFactory(selection.Name.Value, subFields))
			break
		default:
			err = fmt.Errorf("found unexpected selection field type")
			r.logger.WithFields(logrus.Fields{
				"code": 400,
			}).Warning(err.Error())

			return nil, err
		}
	}

	return fields, nil
}

// Фабрика парсера полей
func NewRequestedFieldsParser() RequestedFieldsParserInterface {
	return &requestedFieldsParser{
		logger:        helpers.NewLogger(`requestedFieldsParser`),
		fieldsFactory: NewGraphQlRequestedField,
	}
}
